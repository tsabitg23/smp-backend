exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('project_jobs', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('task_id').references('tasks.id').onDelete('restrict');
            table.datetime('request_date');
            table.string('name');
            table.decimal('total_cost',14,2);
            table.text('description');
            // table.string('status');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('project_jobs');
};