import {UserPermissions} from '../models/user_permissions';
import {generateData} from '../helpers/paging';

export class UserPermission {
	static async get(ctx){
		const {body,query={}} = ctx.request;
		
		ctx.body = await generateData(
				UserPermissions.query().eager('[permission]').whereNull('deleted_at'),
				ctx,
				{user_id : ctx.state.user.user_id}
			);
	}

	public static async getMobile(ctx) {
		const {body,query={}} = ctx.request;
		
		const data = await generateData(
				UserPermissions.query().eager('[permission]').whereNull('deleted_at'),
				ctx,
				{user_id : ctx.state.user.user_id}
		);

		const menus = [];
        data.data.map(it=>{
			const menu = it.permission.name.split('_')[0];
			const dataIndex = menus.findIndex(prop=>prop.key == menu);
			if(!it.read){
				return it;
			}
			else if(dataIndex < 0 && it.read){
                menus.push({
					key : menu,
					submenu : [{
						key : it.permission.key
					}]
				})
			}
			else{
				menus[dataIndex].submenu.push({
					key : it.permission.key
				})
			}
			return it;
		})
		ctx.body = menus;
	}
}