import BaseModel from "./base_model";
import {Model} from "objection";

export class Rewards extends BaseModel {
    code : string;
    reward : string;
    is_claimed : string;


    static get tableName() {
        return 'rewards';
    }
}