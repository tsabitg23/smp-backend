exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('roles', (table) => {
            table.uuid('id').primary();
            table.string('name').notNullable();
            table.string('key').notNullable();
            table.uuid('parent_id');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('roles');
};