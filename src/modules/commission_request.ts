import {generateData} from '../helpers/paging';
import { CommissionRequest } from '../models/commission_request';
import * as uuid from 'uuid';
import {transaction} from 'objection';
import {omit, get as getLodash} from 'lodash';
import { Commission } from '../models/commission';
import { Units } from '../models/units';

export class CommissionRequestModule{
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				CommissionRequest.query().eager('[agent.agency, unit]').whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}



	static async getDetail(ctx){

		ctx.body = await CommissionRequest.query().eager('[agent.agency, unit]').findById(ctx.params.id);	
	}

	static async post(ctx){
		const req = omit(ctx.request.body,['agency_id']);
		ctx.body = await CommissionRequest.query().insert({
			...req,
			code : (uuid.v4()).split("-")[0],
			project_id : ctx.state.user.selected_project,
			marketing_approval_status : null,
			finance_approval_status : null,
			manager_approval_status : null
		})
	}

	static async put(ctx){
		await transaction(CommissionRequest.knex(), async (trx)=>{
			await CommissionRequest.query(trx).where('id',ctx.params.id).update(ctx.request.body);
			const dataRequest = await CommissionRequest.query(trx).findOne('id', ctx.params.id);
			if(dataRequest.finance_approval_status && dataRequest.manager_approval_status && dataRequest.marketing_approval_status && !dataRequest.counted_status){
				const unitData = await Units.query(trx).findOne('id', dataRequest.unit_id);
				const dataCommission = await Commission.query(trx).findOne('agent_id', dataRequest.agent_id);
				let unit_commission_pending = (getLodash(dataCommission,'unit_commission_pending','') || '');
				if(unit_commission_pending){
					unit_commission_pending = unit_commission_pending + `,${unitData.kavling_number}`
				} else {
					unit_commission_pending = unitData.kavling_number;
				}
				const total_commission_pending = (+getLodash(dataCommission,'total_commission_pending',0) || 0) + (+dataRequest.amount);
				if(dataCommission){
					await Commission.query(trx).where('id', dataCommission.id).update({
						unit_commission_pending,
						total_commission_pending
					})
				} else {
					await Commission.query(trx).insert({
						project_id: dataRequest.project_id,
						agent_id : dataRequest.agent_id,
						unit_commission_approved : '',
						unit_commission_pending,
						total_commission_approved : 0,
						total_commission_pending
					})
				}
				await CommissionRequest.query(trx).where('id',ctx.params.id).update({
					counted_status : true
				});
			}
		})
		

		ctx.body = {
			message: 'success'
		}
    }
    
    static async approvalChange(ctx){
        
    }

	static async del(ctx){
		const dataDB = await CommissionRequest.query().findOne('id', ctx.params.id);
		if(dataDB.counted_status){
			throw new Error('Permintaan sudah disetujui, data tidak bisa dihapus')
			return;
		}
		ctx.body = await CommissionRequest.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}

}