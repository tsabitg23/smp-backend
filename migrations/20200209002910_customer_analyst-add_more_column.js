
exports.up = function(knex, Promise) {
    return knex.schema.table("customer_analyst", table => {
        table.string('installment');
        table.string('next_dp_payment');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("customer_analyst", table => {
        table.dropColumn('installment');
        table.dropColumn('next_dp_payment');
    })
};
