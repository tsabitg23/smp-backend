import * as fs from 'fs-extra-promise';
import * as path from "path";

export function generateHtml(templateName, replacer) {
    let templateSpitted = templateName.split('/');
    templateSpitted[templateSpitted.length - 1] = templateSpitted[templateSpitted.length - 1] + '.html';
    let filepath = path.join(__dirname, '..', 'templates', ...templateSpitted);
    return fs.readFileAsync(filepath, 'utf8').then(content => {
        let keys = Object.keys(replacer);
        return keys.reduce((template, key) => {
            return template.replace(new RegExp(`\\$\\{${key}\\}`, 'g'), replacer[key]);
        }, content);
    })
}