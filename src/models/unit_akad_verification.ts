import BaseModel from "./base_model";
import {Model} from "objection";
import {UnitAkad} from './unit_akad';

export class UnitAkadVerification extends BaseModel {
	unit_akad_id : string;
	name : string;
	ktp_number : string;
	gender : string;
	city : string;
    occupation : string;
	test_result : string;

	unit_akad ?: UnitAkad;
    static get tableName() {
        return 'unit_akad_verification';
    }

    static get relationMappings() {
        return {
        	unit_akad : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : UnitAkad,
        		join : {
        			from : `${this.tableName}.unit_akad_id`,
        			to : `${UnitAkad.tableName}.id`
        		}
        	}
        }
    }
}