exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('accounting_period', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('user_id').references('users.id').onDelete('restrict');
            table.decimal('start_balance',14,2);
            table.string('account_number');
            table.string('month');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('accounting_period');
};