import BaseModel from "./base_model";
import {Model} from "objection";
import {Agents} from './agents';
import { Units } from "./units";
import { CommissionRequest } from "./commission_request";

export class CommissionReport extends BaseModel {
	project_id : string;
	commission_request_id : string;
	desc : string;
	marketing_approval_status : boolean;
    marketing_desc : string;
    finance_approval_status : boolean;
    finance_desc : string;
    counted_status : boolean;
    commission_request ?: CommissionRequest;
    
    static get tableName() {
        return 'commission_report';
    }

    static get relationMappings() {
        return {
            commission_request : {
                relation : Model.BelongsToOneRelation,
                modelClass : CommissionRequest,
                join : {
                    from : `${this.tableName}.commission_request_id`,
                    to : `${CommissionRequest.tableName}.id`
                }
            },
        }
    }

}