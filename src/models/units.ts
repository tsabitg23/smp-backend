import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {UnitAkad} from './unit_akad';
import {UnitProgress} from './unit_progress';

export class Units extends BaseModel {
	project_id : string;
	kavling_number : string;
	land_area : string;
	building_area : string;
    status : string;
    akad_by_agent_id ?: string;

    project ?: Projects;
    akad ?: UnitAkad[];
    progress ?: UnitProgress;
    commission_status : boolean;

    static get tableName() {
        return 'units';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
            akad : {
                relation : Model.HasManyRelation,
                modelClass : UnitAkad,
                join : {
                    from : `${this.tableName}.id`,
                    to : `${UnitAkad.tableName}.unit_id`
                }
            },
            progress : {
                relation : Model.HasManyRelation,
                modelClass : UnitProgress,
                join : {
                    from : `${this.tableName}.id`,
                    to : `${UnitProgress.tableName}.unit_id`
                }
            }
        }
    }
}
