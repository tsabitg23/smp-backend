import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';

export class ProjectPlannings extends BaseModel {
	project_id : string;
	name : string;
	start_date : string;
	approx_finish_date : string;
	percentage : string;
	status : string;
	description : string;
	project ?: Projects;
    user_id : string;

    static get tableName() {
        return 'project_plannings';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	}
        }
    }
}