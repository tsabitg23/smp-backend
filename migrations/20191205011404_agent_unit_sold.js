
exports.up = function(knex, Promise) {
    return knex.schema.table("agents", table => {
        table.integer('unit_sold').defaultTo(0);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("agents", table => {
        table.dropColumn('unit_sold');
    })
};
