
exports.up = function(knex, Promise) {
    return knex.schema.alterTable("employee_scores", table => {
        table.jsonb('score_list').alter();
    })
};

exports.down = function(knex, Promise) {
	return knex.schema.alterTable("employee_scores", table => {
        table.jsonb('score_list').alter();
    })  
};
