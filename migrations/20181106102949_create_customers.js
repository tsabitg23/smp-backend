exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('customers', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.string('name');
            table.string('email');
            table.string('gender');
            table.date('dob');
            table.string('id_card_type');
            table.string('id_card_number');
            table.string('marital_status');
            table.string('religion');
            table.string('last_degree');
            table.text('home_address');
            table.string('home_status');
            table.text('phones_data');
            table.text('company_data');
            table.text('contactable_data');
            table.text('financial_data');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('customers');
};