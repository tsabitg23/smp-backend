exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('project_plannings', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            // table.uuid('task_id').references('tasks.id');
            table.string('name');
            table.datetime('start_date');
            table.datetime('approx_finish_date');
            table.decimal('percentage',3,2);
            table.string('status');
            table.text('description');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('project_plannings');
};