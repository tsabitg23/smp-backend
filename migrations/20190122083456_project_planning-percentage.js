
exports.up = function(knex, Promise) {
    return knex.schema.alterTable("project_plannings", table => {
        table.integer('percentage').alter();
    })
};

exports.down = function(knex, Promise) {
	return knex.schema.alterTable("project_plannings", table => {
        table.decimal('percentage',14,2).alter();
    })  
};
