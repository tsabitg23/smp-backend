
exports.up = function(knex, Promise) {
    return knex.schema.table("project_timelines", table => {
        table.datetime('end_date');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("project_timelines", table => {
        table.dropColumn('end_date');
    })
};
