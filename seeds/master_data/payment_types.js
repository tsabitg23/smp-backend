const constant = require('../../config/const').constant;

module.exports = [
    {
        id : constant.PAYMENT_TYPE.CASH,
        name : 'Cash'
    },
    {
        id : constant.PAYMENT_TYPE.CASH_BERTAHAP,
        name : 'Cash Bertahap'
    },
    {
        id : constant.PAYMENT_TYPE.CREDIT,
        name : 'Kredit'
    },
];