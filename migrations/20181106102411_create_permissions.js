exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('permissions', (table) => {
            table.uuid('id').primary();
            table.string('name');
            table.string('key');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('permissions');
};