import BaseModel from "./base_model";
import {Model} from "objection";
import {Agencies} from "./agencies";
import {Customers} from "./customers";
import {Units} from "./units";
import {CustomerAnalystAccount} from "./customer_analyst_account";
import {Employees} from "./employees";

export class HrdReports extends BaseModel {
    project_id : string;
    employee_id : string;
    period_start: string;
    period_end: string;
    total_customer_booking: string;
    total_customer_akad: string;
    total_customer_interviewed: string;
    total_customer_interviewed_passed: string;
    total_customer_interviewed_failed: string;
    manager_approval_status: boolean;
    manager_desc: string;


    employee ?: Customers;


    static get tableName() {
        return 'hrd_reports';
    }

    static get relationMappings() {
        return {
            employee : {
                relation : Model.BelongsToOneRelation,
                modelClass : Employees,
                join : {
                    from : `${this.tableName}.employee_id`,
                    to : `${Employees.tableName}.id`
                }
            }
        }
    }

}
