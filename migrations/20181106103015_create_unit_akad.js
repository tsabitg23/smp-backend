exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('unit_akad', (table) => {
            table.uuid('id').primary();
            table.uuid('unit_id').references('units.id').onDelete('restrict');
            table.uuid('customer_id').references('customers.id').nullable();
            table.uuid('agent_id').references('agents.id').nullable();
            table.datetime('booking_date');
            table.datetime('verification_date');
            table.datetime('akad_date');
            table.string('status');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('unit_akad');
};