import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {Agents} from './agents';

export class Agencies extends BaseModel {
	project_id : string;
	name : string;
	address : string;
	phone : string;

    project ?: Projects;
	agents ?: Agents;
	

    static get tableName() {
        return 'agencies';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
            agents : {
                relation : Model.HasManyRelation,
                modelClass : Agents,
                join : {
                    from : `${this.tableName}.id`,
                    to : `${Agents.tableName}.agency_id`
                }
            }
        }
    }
}