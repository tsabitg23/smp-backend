import BaseModel from "./base_model";
import {Model} from "objection";
import {Entities} from './entities';
import {Roles} from './roles';
import {UserPermissions} from './user_permissions'
import {UserProjects} from './user_projects'

export class Users extends BaseModel {
	entity_id : string;
	role_id : string;
	email : string;
	password : string;
	salt : string;

	entity ?: Entities;
	role ?: Roles;
    permissions ?:UserPermissions[];
    projects ?: UserProjects[];

    static get tableName() {
        return 'users';
    }

    static get relationMappings() {
        return {
        	entity : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Entities,
        		join : {
        			from : `${this.tableName}.entity_id`,
        			to : `${Entities.tableName}.id`
        		}
        	},
        	role : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Roles,
        		join : {
        			from : `${this.tableName}.role_id`,
        			to : `${Roles.tableName}.id`
        		}
        	},
            permissions : {
                relation : Model.HasManyRelation,
                modelClass : UserPermissions,
                join : {
                    from : `${this.tableName}.id`,
                    to : `${UserPermissions.tableName}.user_id`
                }
            },
            projects : {
                relation : Model.HasManyRelation,
                modelClass : UserProjects,
                join : {
                    from : `${this.tableName}.id`,
                    to : `${UserProjects.tableName}.user_id`
                }    
            }
        }
    }
}