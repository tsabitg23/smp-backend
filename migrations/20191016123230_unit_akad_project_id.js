
exports.up = function(knex, Promise) {
    return knex.schema.table("unit_akad", table => {
        table.uuid('project_id').references('projects.id').onDelete('restrict');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("unit_akad", table => {
        table.dropColumn('project_id');
    })
};
