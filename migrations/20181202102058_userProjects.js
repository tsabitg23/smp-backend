exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('user_projects', (table) => {
            table.uuid('id').primary();
            table.uuid('user_id').references('users.id').onDelete('restrict');
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('user_projects');
};