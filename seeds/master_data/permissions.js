const constant = require('../../config/const').constant;

let data = Object.keys(constant.PERMISSIONS).map(it=>{
	let key = it.split("_");
	if(key.length > 1){
		key.splice(0,1);
		key = key.join("_");
	} else {
		key = it;
	}

	return {
		id : constant.PERMISSIONS[it],
		name : it.toLowerCase(),
		key : key.toLowerCase()
	}
});
module.exports = data;