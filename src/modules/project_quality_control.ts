import {ProjectQualityControls} from '../models/project_quality_controls';
import {generateData} from '../helpers/paging';
import {constant} from '../../config/const';

export class ProjectQualityControl {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		const q = ProjectQualityControls.query().whereNull('deleted_at')

		if(ctx.request.query.division === 'null'){
			ctx.request.query.division = null;
		}

		ctx.body = await generateData(
				q,
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){

		ctx.body = await ProjectQualityControls.query().findById(ctx.params.id);
	}

	static async post(ctx){
		ctx.body = await ProjectQualityControls.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
	}


	static async put(ctx){
		ctx.body = await ProjectQualityControls.query().where('id',ctx.params.id).update(ctx.request.body)
	}


	static async del(ctx){
		ctx.body = await ProjectQualityControls.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}