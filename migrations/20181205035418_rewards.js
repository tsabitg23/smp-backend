exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('rewards', (table) => {
            table.uuid('id').primary();
            table.string('code');
            table.string('reward');
            table.boolean('is_claimed').defaultTo(false);
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('rewards');
};