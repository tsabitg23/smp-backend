import BaseModel from "./base_model";
import {Model} from "objection";
import {Users} from './users';
import {Tasks} from './tasks';

export class TaskUsers extends BaseModel {
	user_id : string;
	task_id : string;
	status : string;

	user ?: Users;
	task ?: Tasks;

    static get tableName() {
        return 'task_users';
    }

    static get relationMappings() {
        return {
        	user : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.user_id`,
        			to : `${Users.tableName}.id`
        		}
        	},
        	task : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Tasks,
        		join : {
        			from : `${this.tableName}.task_id`,
        			to : `${Tasks.tableName}.id`
        		}
        	}
        }
    }
}