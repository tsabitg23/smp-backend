import {Model} from "objection";
import * as Knex from 'knex';
import * as uuid from 'uuid';
import { Request } from "koa";

const knex = Knex(require('../../knexfile')[process.env.NODE_ENV || 'development']);

Model.knex(knex);

export default class BaseModel extends Model {
    id: string;
    created_at: string;
    updated_at: string;
    deleted_at?: string;

    $beforeInsert() {
        this.id = this.id || uuid.v4();
        this.created_at = new Date().toISOString();
    }

    $beforeUpdate() {
        this.updated_at = new Date().toISOString();
    }
}