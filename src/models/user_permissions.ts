import BaseModel from "./base_model";
import {Model} from "objection";
import {Users} from './users';
import {Permissions} from './permissions';

export class UserPermissions extends BaseModel {
	user_id : string;
	permission_id : string;
	create : boolean;
	update : boolean;
	delete : boolean;
	read : boolean;

	user ?: Users;
	permission ?: Permissions;

    static get tableName() {
        return 'user_permissions';
    }

    static get relationMappings() {
        return {
        	user : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.user_id`,
        			to : `${Users.tableName}.id`
        		}
        	},
        	permission : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Permissions,
        		join : {
        			from : `${this.tableName}.permission_id`,
        			to : `${Permissions.tableName}.id`
        		}
        	}
        }
    }
}