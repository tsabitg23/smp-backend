import BaseModel from "./base_model";
import {Model} from "objection";
import {Units} from './units';
import {Customers} from './customers';
import {PaymentTypes} from './payment_types';
export class CustomerUnits extends BaseModel {
	unit_id : string;
	customer_id : string;
	payment_type_id : string;
	downpayment : number;
	installment_per_moth: number;
	installment_times : number;

	unit ?: Units;
	customer ?: Customers;
	payment_type ?: PaymentTypes;

    static get tableName() {
        return 'customer_units';
    }

    static get relationMappings() {
        return {
        	unit : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Units,
        		join : {
        			from : `${this.tableName}.unit_id`,
        			to : `${Units.tableName}.id`
        		}
        	},
        	customer : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Customers,
        		join : {
        			from : `${this.tableName}.customer_id`,
        			to : `${Customers.tableName}.id`
        		}
        	},
        	payment_type : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : PaymentTypes,
        		join : {
        			from : `${this.tableName}.payment_type_id`,
        			to : `${PaymentTypes.tableName}.id`
        		}
        	}
        }
    }
}