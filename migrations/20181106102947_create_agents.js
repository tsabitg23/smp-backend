exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('agents', (table) => {
            table.uuid('id').primary();
            table.uuid('agency_id').references('agencies.id').onDelete('restrict');
            table.string('name');
            table.string('phone');
            table.string('email').unique();
            table.text('address');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('agents');
};