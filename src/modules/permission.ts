import {Permissions} from '../models/permissions';
import {generateData} from '../helpers/paging';

export class Permission {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				Permissions.query().whereNull('deleted_at'),
				ctx,
			);
	}

	static async getDetail(ctx){

		ctx.body = await Permissions.query().findById(ctx.params.id);	
	}

	static async post(ctx){
		ctx.body = await Permissions.query().insert(ctx.request.body);
	}

	static async put(ctx){
		ctx.body = await Permissions.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await Permissions.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}