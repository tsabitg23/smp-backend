import {Rewards} from '../models/rewards';
import * as path from 'path';
import * as send from 'koa-send';
import * as mime from 'mime-types';
import * as fs from 'fs-extra-promise';
const passThrough = require('stream').PassThrough;
const request = require('request');

export class Reward{
	static async claim(ctx){
		const {request : {body}} = ctx;
		let data = await Rewards.query().findOne({
			code : body.code
		})

		if(!data){
			ctx.status = 400;
			ctx.body = {
				message : 'Code unvalid'
			}
			return;
		}

		if(data.is_claimed){
			ctx.status = 400;
			ctx.body = {
				message : 'Reward already claimed'
			}
			return;	
		}

		let file = path.join('assets',data.reward);
		if (fs.existsSync(file)) {
	        ctx.set('Content-disposition','attachment; filename=' + data.reward);
	        ctx.set('Content-type',mime.lookup(file))
	        ctx.attachment(data.reward);
	        await Rewards.query().where('code',body.code).update({
	        	is_claimed : true
	        });
	        await send(ctx,file)
		}
		else {
			ctx.status = 400;
			ctx.body = {
				message : 'Reward not found'
			}
			return;	
		}

	}

	static async get(ctx){
		let file = path.join('assets',ctx.params.filename);
		
		await send(ctx,file)
	}
}