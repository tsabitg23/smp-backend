
exports.up = function(knex, Promise) {
    return knex.schema.alterTable("customer_units", table => {
        table.decimal('installment_per_month',14,2).alter();
    })
};

exports.down = function(knex, Promise) {
	return knex.schema.alterTable("customer_units", table => {
        table.integer('installment_per_month').alter();
    })  
};
