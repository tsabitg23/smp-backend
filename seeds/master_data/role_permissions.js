const constant = require('../../config/const').constant;

module.exports = [
	{
		id : 'bc909395-b29a-4cec-bfb8-3e9b3da1cd00',
		role_id : constant.ROLES.ADMIN ,
		permission_id : constant.PERMISSIONS.USERS,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : 'cfecd434-a259-42e9-b7f6-1348f06c5f12',
		role_id : constant.ROLES.ADMIN ,
		permission_id : constant.PERMISSIONS.PROJECTS,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '248ca266-e1f8-40b8-9c12-dbb510643bda',
		role_id : constant.ROLES.MARKETING ,
		permission_id : constant.PERMISSIONS.UNITS,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '7ddb7aae-de14-4f00-8453-0879d6fa43f2',
		role_id : constant.ROLES.MARKETING ,
		permission_id : constant.PERMISSIONS.GUEST_BOOK,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : 'a4875dea-8d68-49cb-931d-08c2cb6cc745',
		role_id : constant.ROLES.MARKETING ,
		permission_id : constant.PERMISSIONS.MARKETING_PLANS,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : 'acf56076-7016-475f-8ba4-405040f20a27',
		role_id : constant.ROLES.MARKETING ,
		permission_id : constant.PERMISSIONS.AGENTS,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '498e643b-68bf-431e-8120-953d3d4ac276',
		role_id : constant.ROLES.MARKETING ,
		permission_id : constant.PERMISSIONS.AGENCIES,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '34d4a95c-f2cd-4afa-b15e-27226130186a',
		role_id : constant.ROLES.HRD ,
		permission_id : constant.PERMISSIONS.CUSTOMER_VERIFICATION,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '48d1f491-8901-4916-8308-00bb8dc6e9c0',
		role_id : constant.ROLES.HRD ,
		permission_id : constant.PERMISSIONS.CUSTOMERS,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '062e049e-1465-4048-9b98-9fbb535999bc',
		role_id : constant.ROLES.HRD ,
		permission_id : constant.PERMISSIONS.EMPLOYEES,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : 'c8bf4e4e-b5fd-4675-814e-3f9432999a83',
		role_id : constant.ROLES.SUPERVISOR ,
		permission_id : constant.PERMISSIONS.TIMELINE_PROJECT,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '30abdd1c-7d11-4aa2-9982-46318f97477f',
		role_id : constant.ROLES.SUPERVISOR ,
		permission_id : constant.PERMISSIONS.UNIT_PROGRESS,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : 'b4bc844d-95fb-4858-a135-3ab81d073ede',
		role_id : constant.ROLES.SUPERVISOR ,
		permission_id : constant.PERMISSIONS.PROJECT_JOBS,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '6543d8fe-3560-444b-abca-fac82f4b3ca8',
		role_id : constant.ROLES.SUPERVISOR ,
		permission_id : constant.PERMISSIONS.PROJECT_PLANNING,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '981915f7-0f8c-4ce0-93c8-9f3d1b60d24a',
		role_id : constant.ROLES.SUPERVISOR ,
		permission_id : constant.PERMISSIONS.PROJECT_PAYMENT_REQUEST,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '5958b482-27e3-499f-b0f2-7c20f832147f',
		role_id : constant.ROLES.SUPERVISOR ,
		permission_id : constant.PERMISSIONS.PROJECT_REPORT,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : 'd9a27929-ab5d-4a2e-a5a3-ac73762cc760',
		role_id : constant.ROLES.SUPERVISOR ,
		permission_id : constant.PERMISSIONS.QUALITY_CONTROL,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '2e77ed30-3b1e-45a3-88e9-a8a14983a1bb',
		role_id : constant.ROLES.MR ,
		permission_id : constant.PERMISSIONS.EMPLOYEE_SCORE,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : 'bfeeca56-ac9f-4550-b65b-6e7177b49211',
		role_id : constant.ROLES.MR ,
		permission_id : constant.PERMISSIONS.MINUTES_OF_MEETINGS,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '52ad1551-89a5-4b40-99bd-67d6f1da630f',
		role_id : constant.ROLES.MR ,
		permission_id : constant.PERMISSIONS.PROJECT_SUPERVISION,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '21aa310d-6cdb-4b59-be33-16218522be44',
		role_id : constant.ROLES.MR ,
		permission_id : constant.PERMISSIONS.QUALITY_CONTROL,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : 'e572588c-a2a1-42d4-aebe-b69dc398374e',
		role_id : constant.ROLES.MR ,
		permission_id : constant.PERMISSIONS.APPROVE_PROJECT_PAYMENT_REQUEST,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '42eeda6d-4de1-4d77-88a3-af7a5cea48e1',
		role_id : constant.ROLES.MR ,
		permission_id : constant.PERMISSIONS.APPROVE_PROJECT_JOBS,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : 'a2b768cc-accf-4bee-9be1-a5d335342721',
		role_id : constant.ROLES.MR ,
		permission_id : constant.PERMISSIONS.RECOMMEND_PROJECT_REPORT,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : 'e1dd5c74-2c8a-4b02-b6ea-3fe0f2c422cd',
		role_id : constant.ROLES.ACCOUNTING ,
		permission_id : constant.PERMISSIONS.ACCOUNTING_CODES,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : 'c8828d25-6ba5-48ce-b2f8-1534a53df07a',
		role_id : constant.ROLES.ACCOUNTING ,
		permission_id : constant.PERMISSIONS.TRANSACTION_INCOME,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '55249aba-0299-4e09-b929-1292e0d72b6e',
		role_id : constant.ROLES.ACCOUNTING ,
		permission_id : constant.PERMISSIONS.TRANSACTION_EXPENSE,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '87e0321b-4b29-465f-86c1-5da252879bf6',
		role_id : constant.ROLES.ACCOUNTING ,
		permission_id : constant.PERMISSIONS.CASH_IN_PROJECTION,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '957e1121-6327-4c38-a8fd-c2c029d62a59',
		role_id : constant.ROLES.ACCOUNTING ,
		permission_id : constant.PERMISSIONS.CASH_IN_REAL,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '1ab5ae37-f2e1-4bc0-bffc-817c41c56a51',
		role_id : constant.ROLES.ACCOUNTING ,
		permission_id : constant.PERMISSIONS.CASH_OUT_PROJECTION,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '700dc207-57c9-4587-9cb7-a1e7b42ec0e3',
		role_id : constant.ROLES.ACCOUNTING ,
		permission_id : constant.PERMISSIONS.CASH_OUT_REAL,
		create : true,
		read : true,
		update : true,
		delete : true
	},
	{
		id : '73c3f9f6-ec16-40a6-bfcd-40c06c4a46e2',
		role_id : constant.ROLES.ACCOUNTING ,
		permission_id : constant.PERMISSIONS.READ_CASHFLOW,
		create : false,
		read : true,
		update : false,
		delete : false
	}
];