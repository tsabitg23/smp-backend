exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('project_timelines', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('user_id').references('users.id').onDelete('restrict');
            table.string('name');
            table.datetime('activity_name');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('project_timelines');
};