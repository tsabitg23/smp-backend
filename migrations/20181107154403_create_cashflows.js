exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('cashflows', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('transaction_type_id').references('transaction_types.id').onDelete('restrict');
            table.uuid('user_creator_id').references('users.id').onDelete('restrict');
            table.string('type');
            table.string('month');
            table.decimal('amount',14,2);
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('cashflows');
};