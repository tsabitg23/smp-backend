const insertIfNotExist = require('../src/helpers/seed_insert').insertIfNotExist;
const entities = require('./sample_data/entities');
const users = require('./sample_data/users');
const user_permissions = require('./sample_data/user_permissions');
const user_profile = require('./sample_data/user_profile');

exports.seed = async function(knex, Promise) {
  await insertIfNotExist('entities',entities,knex,true);
  await insertIfNotExist('users',users,knex,true);
  await insertIfNotExist('user_profile',user_profile,knex,true);
  await insertIfNotExist('user_permissions',user_permissions,knex,true);
};