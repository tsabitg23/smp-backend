exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('employee_scores', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('employee_id').references('employees.id').onDelete('restrict');
            table.uuid('user_id').references('users.id');
            table.date('periode_start');
            table.date('periode_end');
            table.string('phone');
            table.string('total_score');
            table.text('score_list');
            table.text('employee_note');
            table.text('hrd_note');
            table.text('boss_note');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('employee_scores');
};