import BaseModel from "./base_model";
import {Model} from "objection";

export class VerificationQuestions extends BaseModel {
	name : string;
	type : string;
	category : string;
	is_favorable : boolean;
	order : number;
 
    static get tableName() {
        return 'verification_questions';
    }
}