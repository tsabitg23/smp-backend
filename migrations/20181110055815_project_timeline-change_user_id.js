
exports.up = function(knex, Promise) {
    return knex.schema.table("project_timelines", table => {
    	table.dropForeign('user_id');
        table.dropColumn('user_id');
        table.uuid('employee_id').references('employees.id').onDelete('restrict');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("project_timelines", table => {
        table.uuid('user_id').references('users.id').onDelete('restrict');
        table.dropColumn('employee_id');
    })
};
