import {Events} from '../models/events';
import {CustomerUnits} from '../models/customer_units';
import {generateData} from '../helpers/paging';
import {transaction} from 'objection';
import {omit} from 'lodash';
import * as uuid from 'uuid';
import { EventFunds } from '../models/event_funds';
import { EventCrew } from '../models/event_crew';
import { Commission } from '../models/commission';

export class CommissionController {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				Commission.query().eager('[agent.agency]').whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}
}