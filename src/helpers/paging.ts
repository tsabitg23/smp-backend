import { Request } from "koa";
import { Model, QueryBuilder } from "objection";
import {omit} from 'lodash';
import {appConfig} from '../../config/app';
import * as moment from "moment";

export const generateData = async (model:QueryBuilder<any>, request:Request,additional_query:any={}) => {
    const {query:{page=1, per_page=30}} = request;

    let q = model;
    let request_query = omit(request.query,appConfig.ignored_column);
    
    q.where(Object.assign(additional_query,request_query))

    if(request.query.search && request.query.search_by){
        q.where(request.query.search_by,'ilike',`%${request.query.search}%`);
    }

    if(request.query.start_date && request.query.end_date){
        const startKey = request.query.start_key || 'date';
        const endKey = request.query.end_key || 'date';
            q.where(startKey, '>=', moment(request.query.start_date).startOf('days').format('YYYY-MM-DD'))
            .where(endKey, '<=', moment(request.query.end_date).endOf('days').format('YYYY-MM-DD'));
    }
    if(!request.query.show_all){
        q.page(page-1, per_page);
    }

    if(!request.query.order_by){
        q.orderBy('created_at','DESC');
    }else{
        let order_by = request.query.order_by.split('_');
        q.orderBy(order_by[0],order_by[1].toUpperCase());
    }
    
    const data:any = await q;

    return {
        data: (request.query.show_all) ? data : data.results,
        max: (request.query.show_all) ? data.length : data.total,
        size: (request.query.show_all) ? data.length : data.results.length
    };
}

export const filterRange = async (model:QueryBuilder<any>, request:Request)=>{
    let q = model;
    let request_query = omit(request.query,appConfig.ignored_column);
    q.where(request_query);
    if(request.query.start_date && request.query.end_date){
        const startKey = request.query.start_key || 'date';
        const endKey = request.query.end_key || 'date';
            q.where(startKey, '>=', moment(request.query.start_date).startOf('days').format('YYYY-MM-DD'))
            .where(endKey, '<=', moment(request.query.end_date).endOf('days').format('YYYY-MM-DD'));
    }

    return await q;
}