import * as nodemailer from 'nodemailer';
import {appConfig} from "../../config/app";
import {SendMailOptions} from "nodemailer";

let transporter = nodemailer.createTransport(appConfig.email);

export function sendMail(mailOptions: SendMailOptions) {
    mailOptions = Object.assign({},mailOptions ,{
        from: `${appConfig.email.name} <${appConfig.email.auth.user}>`
    });
    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error, 'error');
                reject(error);
                return;
            }
            resolve(info);
            return;
        });
    })
}
