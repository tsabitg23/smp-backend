exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('accounts', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('parent_id').nullable();
            table.string('code');
            table.string('name');
            table.string('position');
            table.string('category');
            // table.string('status');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('accounts');
};
