import BaseModel from "./base_model";
import {Model} from "objection";

export class TransactionTypes extends BaseModel {
	name : string;
	type : string;
	is_header : boolean;
	parent_id : boolean;
	children : any;
	
    static get tableName() {
        return 'transaction_types';
    }
}