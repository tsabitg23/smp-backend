exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('assessment_aspects', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id');
            table.string('name');
            table.boolean('is_header');
            table.uuid('parent_id');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('assessment_aspects');
};