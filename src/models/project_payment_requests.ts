import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {Tasks} from './tasks';

export class ProjectPaymentRequests extends BaseModel {
	project_id : string;
	task_id : string;
	name : string;
	max_cost : number;
	description : string;
	project ?:Projects;
	task ?: Tasks;

    static get tableName() {
        return 'project_payment_requests';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
        	task : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Tasks,
        		join : {
        			from : `${this.tableName}.task_id`,
        			to : `${Tasks.tableName}.id`
        		}
        	}
        }
    }
}