import {generateData} from '../helpers/paging';
import {BankAccounts} from "../models/bank_accounts";
import {Accounts} from "../models/accounts";

export class AccountModule{
    static async get(ctx){
        const {body,query={}} = ctx.request;

        ctx.body = await generateData(
            Accounts.query().whereNull('deleted_at').where((builder)=>{
                builder.whereNull('project_id').orWhere({project_id : ctx.state.user.selected_project})
            }),
            ctx,
            {}
        );
    }

    static async getHeader(ctx){
        ctx.body = await Accounts.query().whereNull('parent_id');
    }

    static async getByParent(ctx){
        ctx.body = await Accounts.query().where('parent_id', ctx.request.query.parent_id);
    }

    static async getDetail(ctx){

        const data = await Accounts.query().findById(ctx.params.id);
        let loop = 2;
        let currentData = JSON.parse(JSON.stringify(data));
        while(loop>0){
            if(currentData.parent_id){
                const foundData = await Accounts.query().findById(currentData.parent_id)
                data[`account_header${loop}`] = foundData.id;
                currentData = JSON.parse(JSON.stringify(foundData));
                loop--;
            } else {
                break;
            }
        }

        ctx.body = data;
    }

    static async post(ctx){
        ctx.body = await Accounts.query()
            .insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
    }

    static async put(ctx){
        ctx.body = await Accounts.query().where('id',ctx.params.id).update(ctx.request.body)
    }

    static async del(ctx){
        ctx.body = await Accounts.query().where('id',ctx.params.id).update({
            deleted_at : new Date().toISOString()
        })
    }


}
