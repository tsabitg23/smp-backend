import BaseModel from "./base_model";
import {Model} from "objection";

export class PaymentTypes extends BaseModel {
	name : string;
	
    static get tableName() {
        return 'payment_types';
    }
}