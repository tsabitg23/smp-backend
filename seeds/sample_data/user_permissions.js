const constant = require('../../config/const').constant;

module.exports = [
    {
        id : '9943fc59-3774-4065-9555-116f21957d0b',
        user_id : constant.USER.ADMIN,
        permission_id : constant.PERMISSIONS.ADMIN_USERS,
        create : true,
        read : true,
        update : true,
        delete : true    
    },
    {
        id : 'dd07a37f-40d8-4005-9cb3-192c86c43d12',
        user_id : constant.USER.ADMIN,
        permission_id : constant.PERMISSIONS.ADMIN_PROJECTS,
        create : true,
        read : true,
        update : true,
        delete : true    
    },
];