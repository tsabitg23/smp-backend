import {Agencies} from '../models/agencies';
import {Agents} from '../models/agents';
import {generateData} from '../helpers/paging';

export class Agency {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				Agencies.query().whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){

		ctx.body = await Agencies.query().eager('[agents]').findById(ctx.params.id);	
	}

	static async post(ctx){
		ctx.body = await Agencies.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
	}

	static async put(ctx){
		ctx.body = await Agencies.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		await Agencies.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})

		await Agents.query().where('agency_id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})

		ctx.body = {
			message : "success"
		}
	}
}