import * as crypto from 'crypto';
import {Users} from '../models/users';
import {UserFirebaseTokens} from '../models/user_firebase_tokens';
import {appConfig} from '../../config/app';
import {constant} from '../../config/const';
import {generateHtml} from '../helpers/template';
import {sendMail} from '../helpers/email';
import {verifyCaptcha} from '../services/recaptcha';

import * as jwt from 'jsonwebtoken';

export class Authentication {
	static async login(ctx){
		const {body,query={}} = ctx.request;

        if(!body.url.includes('localhost') && !body.captcha){
            ctx.status = 400;
            ctx.body = {
                message: 'Please verify by clicking the captcha'
            };
            return;
        }


		if(!body.email || !body.password){
			ctx.status = 400;
            ctx.body = {
                message: 'Email or password cant be empty'
            };
            return;
		}
		const user:any = await Users.query().eager('[role,entity.projects(notDeleted),projects(notDeleted)]',{
			notDeleted : (builder)=>{
				builder.whereNull('deleted_at')
			}
		}).findOne({
			email : body.email,
			deleted_at : null
		})

		if(!user){
			// throw new Error('User not found');
			ctx.status = 400;
            ctx.body = {
                message: 'User not found'
            };
            return;
		}

		const password = crypto.pbkdf2Sync(body.password.toString(), user.salt, 50, 100, 'sha512').toString('hex');
		if(user.password !== password){
			ctx.status = 400;
            ctx.body = {
                message: 'Password Wrong'
            };
            return;
		}

        else if(!body.url.includes('localhost') && body.captcha){
            let verifCaptcha = await verifyCaptcha(body.captcha).catch(err=>{
                console.log(err,'this is error captcha');
            })
            if(!verifCaptcha.success){
                ctx.status = 400;
                ctx.body = {
                    message: 'Captcha not valid'
                };
                return;    
            }
        }

		if(body.token){
			//delete same token from table and re-input
			await UserFirebaseTokens.query().del().where('token',body.token);
			await UserFirebaseTokens.query().insert({
				user_id : user.id,
				token  : body.token
			})
		}

		// let permissions = user.permissions.map(it=>{
		// 	it.name = it.permission.name;
		// 	it.key = it.permission.key;
		// 	delete it.permission
		// 	return it;
		// })

		let tokenData:any = {
            user_id: user.id,
            email : user.email,
            role : user.role.name,
            entity_id : user.entity_id,
            role_id : user.role.id,
            fb_token : body.token
            // permission : permissions
        };

        if(user.role.id === constant.ROLES.ADMIN){
            if(user.entity.projects.length > 0){
                tokenData.selected_project = user.entity.projects[0].id
            }
            else{
                tokenData.selected_project = null
            }
        }
        else{
            if(user.projects.length > 0){
            	tokenData.selected_project = user.projects[0].project_id
            }
            else{
            	tokenData.selected_project = null
            }
        }

		const token = jwt.sign(tokenData, appConfig.secret);

		ctx.body = {
			message : 'success',
			token : token
		}
	}

    static async loginMobile(ctx){
        const {body,query={}} = ctx.request;

        if(!body.email || !body.password){
            ctx.status = 400;
            ctx.body = {
                message: 'Email or password cant be empty'
            };
            return;
        }
        const user:any = await Users.query().eager('[role,entity.projects(notDeleted),projects(notDeleted)]',{
            notDeleted : (builder)=>{
                builder.whereNull('deleted_at')
            }
        }).findOne({
            email : body.email,
            deleted_at : null
        });

        if(!user){
            // throw new Error('User not found');
            ctx.status = 400;
            ctx.body = {
                message: 'User not found'
            };
            return;
        }

        const password = crypto.pbkdf2Sync(body.password.toString(), user.salt, 50, 100, 'sha512').toString('hex');
        if(user.password !== password){
            ctx.status = 400;
            ctx.body = {
                message: 'Password Wrong'
            };
            return;
        }

        if(body.token){
            //delete same token from table and re-input
            await UserFirebaseTokens.query().del().where('token',body.token);
            await UserFirebaseTokens.query().insert({
                user_id : user.id,
                token  : body.token
            })
        }

        let tokenData:any = {
            user_id: user.id,
            email : user.email,
            role : user.role.name,
            entity_id : user.entity_id,
            role_id : user.role.id,
            fb_token : body.token
            // permission : permissions
        };

        if(user.entity.projects.length > 0){
            tokenData.selected_project = user.entity.projects[0].id
        }
        else{
            tokenData.selected_project = null
        }

        const token = jwt.sign(tokenData, appConfig.secret);

        ctx.body = {
            message : 'success',
            token : token
        }
    }

	static async logout(ctx){
        const {state : {user : {user_id,fb_token=null}},request : {body}} = ctx;

        ctx.body = await UserFirebaseTokens.query().del().where({
            user_id : user_id,
            token : fb_token
        });
    }

    static async changePasswordAdmin(ctx){
        const {state : {user},request : {body}} = ctx;

        let userData = await Users.query().findOne({
            id : body.user_id
        });

        if(body.new_password !== body.confirm_new_password){
            ctx.status = 400;
            ctx.body = {
                message: 'New password and confirm did not match'
            };
            return;           
        }

        let new_password = crypto.pbkdf2Sync(body.new_password.toString(), userData.salt, 50, 100, 'sha512').toString('hex');

        await Users.query().update({
            password : new_password 
        }).where('id',userData.id);

        ctx.body = {
            message : 'success'
        }
    }

    static async changePassword(ctx){
    	const {state : {user},request : {body}} = ctx;

    	let userData = await Users.query().findOne({
        	id : user.user_id
        });

        const password = crypto.pbkdf2Sync(body.current_password.toString(), userData.salt, 50, 100, 'sha512').toString('hex');

        if(userData.password !== password){
			ctx.status = 400;
            ctx.body = {
                message: 'Current password is incorrect'
            };
            return;       	
        }

        if(body.new_password !== body.confirm_new_password){
			ctx.status = 400;
            ctx.body = {
                message: 'New password and confirm did not match'
            };
            return;       	
        }

        let new_password = crypto.pbkdf2Sync(body.new_password.toString(), userData.salt, 50, 100, 'sha512').toString('hex');

        await Users.query().update({
        	password : new_password 
        }).where('id',user.user_id);

        ctx.body = {
        	message : 'success'
        }
    }

    static async resetPassword(ctx){
        const {state : {user},request : {body}} = ctx;

        let userData = await Users.query().findOne({
            id : body.id
        });

        if(!userData){
            ctx.status = 400;
            ctx.body = {
                message: 'User not found'
            };
            return;           
        }

        if(body.new_password !== body.confirm_new_password){
            ctx.status = 400;
            ctx.body = {
                message: 'New password and confirm did not match'
            };
            return;           
        }

        const password = crypto.pbkdf2Sync(body.new_password.toString(), userData.salt, 50, 100, 'sha512').toString('hex');

        if(userData.password == password){
            ctx.status = 400;
            ctx.body = {
                message: 'New password cannot be same as old password'
            };
            return;           
        }

        let new_password = crypto.pbkdf2Sync(body.new_password.toString(), userData.salt, 50, 100, 'sha512').toString('hex');


        await Users.query().update({
            password : new_password 
        }).where('id',body.id);

        ctx.body = {
            message : 'success'
        }
    }

    static async forgotPassword(ctx){
    	const {request : {body}} = ctx;
    	let user = await Users.query().findOne({
    		email : body.email
    	})

    	if(!user){
    		ctx.status = 400;
    		ctx.body = {
    			message : 'User not found'
    		}
    		return;
    	}

        let token = jwt.sign({
            id: user.id,
            email: user.email
        }, appConfig.secret);

        let html = await generateHtml('forgotpassword', {
            target: appConfig.appUrl + `reset_password?token=${token}`
        });

        await sendMail({
            subject: "Forgot Password SMP",
            text: "Click link below to change your password",
            html: html,
            to: user.email
        });

    	ctx.body = {
    		message : 'success'
    	}
    }
}