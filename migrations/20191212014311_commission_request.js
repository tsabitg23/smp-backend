exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('commission_request', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('agent_id').references('agents.id').onDelete('restrict');
            table.uuid('unit_id').references('units.id').onDelete('restrict');
            table.string('code');
            table.decimal('amount',14,2);
            table.string('desc');
            table.boolean('marketing_approval_status');
            table.string('marketing_desc');
            table.boolean('finance_approval_status');
            table.string('finance_desc');
            table.boolean('manager_approval_status');
            table.string('manager_desc');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('commission_request');
};