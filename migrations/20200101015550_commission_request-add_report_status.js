
exports.up = function(knex, Promise) {
    return knex.schema.table("commission_request", table => {
        table.boolean('report_status').defaultTo(false);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("commission_request", table => {
        table.dropColumn('report_status');
    })
};
