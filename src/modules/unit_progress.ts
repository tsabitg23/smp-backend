import {UnitProgress} from '../models/unit_progress';
import {generateData} from '../helpers/paging';

export class UnitProgressModule {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				UnitProgress.query().eager('[unit]').whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){

		ctx.body = await UnitProgress.query().eager('[unit]').findById(ctx.params.id);
	}

	static async post(ctx){
		ctx.body = await UnitProgress.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
	}


	static async put(ctx){
		ctx.body = await UnitProgress.query().where('id',ctx.params.id).update(ctx.request.body)
	}


	static async del(ctx){
		ctx.body = await UnitProgress.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}