import {Notifications} from '../models/notifications';
import {NotificationUsers} from '../models/notification_users';
import {transaction} from 'objection';
import {UserFirebaseTokens} from '../models/user_firebase_tokens'
import * as uuid from 'uuid';
import {firebaseAdmin} from "./firebase";
import {notifData} from '../../config/notif_data';
import {has} from 'lodash';

export class NotifHelper {
	static createData(type){
		if(has(notifData,type)){
			return notifData[type] 	
		}
		else{
			return {
				title : 'New Notification',
				desc : 'Please check notification'
			}
		}
		
	}

	static async send(type,data,userIds,notifId=false,project_id=null){
		let firebaseTokens = await UserFirebaseTokens.query().whereIn('user_id',userIds);
		let notifData = this.createData(type);
		firebaseTokens.map(firebase=>{
			let firebaseData = JSON.parse(JSON.stringify({
                new_notif: 'true',
                message_id: notifId ? notifId : uuid.v4(),
                additional_data: data,
                title : notifData.title,
                description : notifData.desc,
                type : type,
                project_id,
                notification_type : 'notification'
            }));

            firebaseAdmin().messaging().send({
                data: firebaseData,
                notification : {
                    title : notifData.title,
                    body : notifData.desc
                },
                token: firebase.token
            }).then(it => {
            	//it mean success
                console.log('successsss');
            }).catch(err => {
                console.log(err, 'err notif');
                UserFirebaseTokens.query().del().where('id',firebase.id).then();
            });
		});
	}
}