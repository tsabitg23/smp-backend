import {PaymentTypes} from '../models/payment_types';
import {generateData} from '../helpers/paging';

export class PaymentType {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				PaymentTypes.query().whereNull('deleted_at'),
				ctx,
			);
	}

	static async getDetail(ctx){

		ctx.body = await PaymentTypes.query().findById(ctx.params.id);	
	}

	static async post(ctx){
		ctx.body = await PaymentTypes.query().insert(ctx.request.body);
	}

	static async put(ctx){
		ctx.body = await PaymentTypes.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await PaymentTypes.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}