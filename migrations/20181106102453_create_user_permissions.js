exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('user_permissions', (table) => {
            table.uuid('id').primary();
            table.uuid('user_id').references('users.id').onDelete('restrict');
            table.uuid('permission_id').references('permissions.id').onDelete('restrict');
            table.boolean('create');
            table.boolean('read');
            table.boolean('update');
            table.boolean('delete');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('user_permissions');
};