import BaseModel from "./base_model";

export class EventFunds extends BaseModel {
	name : string;
	event_id : string;
	amount : number;
	desc : string;
	
    static get tableName() {
        return 'event_funds';
    }

}