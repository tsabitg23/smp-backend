import BaseModel from "./base_model";
import {Model} from "objection";

export class UserFirebaseTokens extends BaseModel {
	token : string;
	user_id : string;
	
    static get tableName() {
        return 'user_firebase_tokens';
    }
}