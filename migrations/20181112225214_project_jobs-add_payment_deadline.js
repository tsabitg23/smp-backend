
exports.up = function(knex, Promise) {
    return knex.schema.table("project_jobs", table => {
        table.date('payment_deadline');
        table.dropColumn('request_date');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("project_jobs", table => {
        table.dropColumn('payment_deadline');
        table.date('request_date');
    })
};
