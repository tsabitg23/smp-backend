
exports.up = function(knex, Promise) {
    return knex.schema.table("employees", table => {
    	table.dropForeign('role_id');
        table.dropColumn('role_id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("employees", table => {
        table.uuid('role_id').references('roles.id').onDelete('restrict');
    })
};
