import {checkToken} from './middleware/jwt';
import {Authentication} from './modules/authentication';
import {PaymentType} from './modules/payment_type';
import {Permission} from './modules/permission';
import {AssessmentAspect} from './modules/assessment_aspect';
import {Role} from './modules/role';
import {TransactionType} from './modules/transaction_type';
import {Project} from './modules/project';
import {Agency} from './modules/agencies';
import {Agent} from './modules/agent';
import {Profile} from './modules/profile';
import {AccountingCode} from './modules/accounting_code';
import {GuestBooks} from './modules/guest_book';
import {Employee} from './modules/employee';
import {MarketingPlan} from './modules/marketing_plan';
import {User} from './modules/user'
import {Unit} from './modules/unit'
import {UnitAkad} from './modules/unit_akad'
import {UserPermission} from './modules/user_permission';
import {Notification} from './modules/notif';
import {Upload} from './modules/upload';
import {Reward} from './modules/reward';
import {Dashboard} from './modules/dashboard';
import {Customer} from './modules/customer';
import {UnitProgressModule} from './modules/unit_progress';
import {ProjectTimeline} from './modules/project_timeline';
import {ProjectPlanning} from './modules/project_planning';
import {ProjectJob} from './modules/project_job';
import {ProjectPaymentRequest} from './modules/project_payment_request';
import {ProjectQualityControl} from './modules/project_quality_control';
import {ProjectReport} from './modules/project_report';
import {MinuteOfMeeting} from './modules/minute_of_meeting';
import {ProjectSupervisionModule} from './modules/project_supervision'
import {EmployeeScore} from './modules/employee_score';
import { EventsController } from './modules/events';
import { CommissionController } from './modules/commission';
import { CommissionRequestModule } from './modules/commission_request';
import { CommissionReportModule } from './modules/commission_report';
import {CustomerAnalystModule} from "./modules/customer_analyst";
import {HrdReportModule} from "./modules/hrd_reports";
import {BankAccountModule} from "./modules/bank_accounts";
import {AccountModule} from "./modules/accounts";

export const routes = [
    {
        method : 'get',
        route : "/v1/payment_type/",
        execute : PaymentType.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/payment_type/:id",
        execute : PaymentType.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/payment_type/",
        execute : PaymentType.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/payment_type/:id",
        execute : PaymentType.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/payment_type/:id",
        execute : PaymentType.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/transaction_type/",
        execute : TransactionType.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/transaction_type/:id",
        execute : TransactionType.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/transaction_type/",
        execute : TransactionType.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/transaction_type/:id",
        execute : TransactionType.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/transaction_type/:id",
        execute : TransactionType.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/permission/",
        execute : Permission.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/permission/:id",
        execute : Permission.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/permission/",
        execute : Permission.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/permission/:id",
        execute : Permission.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/permission/:id",
        execute : Permission.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/assessment_aspect/",
        execute : AssessmentAspect.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/assessment_aspect/:id",
        execute : AssessmentAspect.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/assessment_aspect/",
        execute : AssessmentAspect.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/assessment_aspect/:id",
        execute : AssessmentAspect.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/assessment_aspect/:id",
        execute : AssessmentAspect.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/role/",
        execute : Role.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/role/:id",
        execute : Role.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/role/",
        execute : Role.post,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/role/:id/permission",
        execute : Role.postPermission,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/role/:id",
        execute : Role.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/role/:id/permission/:permission_id",
        execute : Role.delPermission,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/role/:id",
        execute : Role.del,
        middleware : [checkToken]
    },


    {
        method : 'post',
        route : "/v1/authentication/login",
        execute : Authentication.login,
        middleware : []
    },
    {
        method : 'post',
        route : "/v1/authentication/login_mobile",
        execute : Authentication.loginMobile,
        middleware : []
    },
    {
        method : 'post',
        route : "/v1/authentication/logout",
        execute : Authentication.logout,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/authentication/change_password",
        execute : Authentication.changePassword,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/authentication/change_password_admin",
        execute : Authentication.changePasswordAdmin,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/authentication/forgot_password",
        execute : Authentication.forgotPassword,
        middleware : []
    },
    {
        method : 'post',
        route : "/v1/authentication/reset_password",
        execute : Authentication.resetPassword,
        middleware : []
    },


    {
        method : 'get',
        route : "/v1/project/",
        execute : Project.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/project/:id",
        execute : Project.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/project/",
        execute : Project.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/project/:id",
        execute : Project.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/project/:id",
        execute : Project.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/agency/",
        execute : Agency.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/agency/:id",
        execute : Agency.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/agency/",
        execute : Agency.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/agency/:id",
        execute : Agency.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/agency/:id",
        execute : Agency.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/profile/",
        execute : Profile.get,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/profile/",
        execute : Profile.put,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/agent/",
        execute : Agent.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/agent_chart/",
        execute : Agent.getChart,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/agent_chart_two/",
        execute : Agent.getChartTwo,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/agent/:id",
        execute : Agent.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/agent/",
        execute : Agent.post,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : '/v1/agent_import',
        execute : Agent.importData,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/agent/:id",
        execute : Agent.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/agent/:id",
        execute : Agent.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/accounting_code/",
        execute : AccountingCode.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/accounting_code/:id",
        execute : AccountingCode.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/accounting_code/",
        execute : AccountingCode.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/accounting_code/:id",
        execute : AccountingCode.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/accounting_code/:id",
        execute : AccountingCode.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/guest_book/",
        execute : GuestBooks.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/guest_book_chart/",
        execute : GuestBooks.getChart,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/guest_book_export/",
        execute : GuestBooks.exportData,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/guest_book/:id",
        execute : GuestBooks.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/guest_book/",
        execute : GuestBooks.post,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : '/v1/guest_book_import',
        execute : GuestBooks.importData,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/guest_book/:id",
        execute : GuestBooks.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/guest_book/:id",
        execute : GuestBooks.del,
        middleware : [checkToken]
    },

    {
        method : 'get',
        route : "/v1/customer_akad/",
        execute : Customer.getCustomerAkad,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/customer/",
        execute : Customer.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/customer/:id",
        execute : Customer.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/customer/",
        execute : Customer.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/customer/:id",
        execute : Customer.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/customer/:id",
        execute : Customer.del,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/customer/:customer_id/unit",
        execute : Customer.postUnit,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/customer/:customer_id/unit/:id",
        execute : Customer.putUnit,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/customer/:customer_id/unit/:id",
        execute : Customer.delUnit,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/employee/",
        execute : Employee.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/employee_export/",
        execute : Employee.exportData,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : '/v1/employee_import',
        execute : Employee.importData,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/employee/:id",
        execute : Employee.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/employee/",
        execute : Employee.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/employee/:id",
        execute : Employee.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/employee/:id",
        execute : Employee.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/marketing_plan/",
        execute : MarketingPlan.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/marketing_plan/:id",
        execute : MarketingPlan.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/marketing_plan/",
        execute : MarketingPlan.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/marketing_plan/:id",
        execute : MarketingPlan.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/marketing_plan/:id",
        execute : MarketingPlan.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/user/",
        execute : User.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/user/:id",
        execute : User.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/user/",
        execute : User.post,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/user/:id/permission",
        execute : User.postPermission,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/user/:id",
        execute : User.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/user/:id/permission/:permission_id",
        execute : User.delPermission,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/user/:id",
        execute : User.del,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/user_permission/",
        execute : UserPermission.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/user_permission_mobile/",
        execute : UserPermission.getMobile,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/unit/",
        execute : Unit.get,
        middleware : [checkToken]
    },

    {
        method : 'get',
        route : "/v1/unit_chart/",
        execute : Unit.getChart,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/unit_export/",
        execute : Unit.exportData,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/unit_akad_by_period/",
        execute : Unit.getUnitBookedByPeriod,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/unit/:id",
        execute : Unit.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/unit/",
        execute : Unit.post,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/unit/:id/akad",
        execute : Unit.postAkad,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/unit/:id/progress",
        execute : Unit.postProgress,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/unit/:id",
        execute : Unit.put,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/unit/:id/akad/:akad_id",
        execute : Unit.putAkad,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/unit/:id/progress/:progress_id",
        execute : Unit.putProgress,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/unit/:id",
        execute : Unit.del,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/unit/:id/akad/:akad_id",
        execute : Unit.delAkad,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/unit/:id/progress/:progress_id",
        execute : Unit.delProgress,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/unit_akad/",
        execute : UnitAkad.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/unit_akad/:id",
        execute : UnitAkad.getDetail,
        middleware : []
    },
    {
        method : 'get',
        route : "/v1/unit_akad_new",
        execute : UnitAkad.getNewData,
        middleware : []
    },
    {
        method : 'put',
        route : "/v1/unit_akad/:id",
        execute : UnitAkad.updateVerification,
        middleware : []
    },
    {
        method : 'put',
        route : "/v1/unit_akad_data/:id",
        execute : UnitAkad.updateNormal,
        middleware : []
    },
    {
        method : 'post',
        route : "/v1/unit_akad/:id/send_verification",
        execute : UnitAkad.updateEmail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/unit_akad_new_save",
        execute : UnitAkad.saveNewVerification,
        middleware : [checkToken]
    },

    {
        method : 'get',
        route : "/v1/unit_progress/",
        execute : UnitProgressModule.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/unit_progress/:id",
        execute : UnitProgressModule.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/unit_progress/",
        execute : UnitProgressModule.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/unit_progress/:id",
        execute : UnitProgressModule.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/unit_progress/:id",
        execute : UnitProgressModule.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/project_timeline/",
        execute : ProjectTimeline.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/project_timeline/:id",
        execute : ProjectTimeline.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/project_timeline/",
        execute : ProjectTimeline.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/project_timeline/:id",
        execute : ProjectTimeline.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/project_timeline/:id",
        execute : ProjectTimeline.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/project_planning/",
        execute : ProjectPlanning.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/project_planning/:id",
        execute : ProjectPlanning.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/project_planning/",
        execute : ProjectPlanning.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/project_planning/:id",
        execute : ProjectPlanning.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/project_planning/:id",
        execute : ProjectPlanning.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/project_job/",
        execute : ProjectJob.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/project_job/:id",
        execute : ProjectJob.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/project_job/",
        execute : ProjectJob.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/project_job/:id",
        execute : ProjectJob.put,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/project_job/:id/task",
        execute : ProjectJob.updateTask,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/project_job/:id",
        execute : ProjectJob.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/project_payment_request/",
        execute : ProjectPaymentRequest.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/project_payment_request/:id",
        execute : ProjectPaymentRequest.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/project_payment_request/",
        execute : ProjectPaymentRequest.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/project_payment_request/:id",
        execute : ProjectPaymentRequest.put,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/project_payment_request/:id/task",
        execute : ProjectPaymentRequest.updateTask,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/project_payment_request/:id",
        execute : ProjectPaymentRequest.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/project_quality_control/",
        execute : ProjectQualityControl.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/project_quality_control/:id",
        execute : ProjectQualityControl.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/project_quality_control/",
        execute : ProjectQualityControl.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/project_quality_control/:id",
        execute : ProjectQualityControl.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/project_quality_control/:id",
        execute : ProjectQualityControl.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/project_report/",
        execute : ProjectReport.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/project_report/:id",
        execute : ProjectReport.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/project_report/",
        execute : ProjectReport.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/project_report/:id",
        execute : ProjectReport.put,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/project_report/:id/reject",
        execute : ProjectReport.reject,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/project_report/:id",
        execute : ProjectReport.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/project_supervision/",
        execute : ProjectSupervisionModule.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/project_supervision/:id",
        execute : ProjectSupervisionModule.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/project_supervision/",
        execute : ProjectSupervisionModule.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/project_supervision/:id",
        execute : ProjectSupervisionModule.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/project_supervision/:id",
        execute : ProjectSupervisionModule.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/minute_of_meeting/",
        execute : MinuteOfMeeting.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/minute_of_meeting/:id",
        execute : MinuteOfMeeting.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/minute_of_meeting/",
        execute : MinuteOfMeeting.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/minute_of_meeting/:id",
        execute : MinuteOfMeeting.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/minute_of_meeting/:id",
        execute : MinuteOfMeeting.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/employee_score/",
        execute : EmployeeScore.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/employee_score/:id",
        execute : EmployeeScore.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/employee_score_export/",
        execute : EmployeeScore.exportData,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/employee_score/",
        execute : EmployeeScore.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/employee_score/:id",
        execute : EmployeeScore.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/employee_score/:id",
        execute : EmployeeScore.del,
        middleware : [checkToken]
    },


    {
        method : 'post',
        route : "/v1/notification/test",
        execute : Notification.test,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/notification/",
        execute : Notification.getAll,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/notification/read/:notification_id",
        execute : Notification.read,
        middleware : [checkToken]
    },


    {
        method : 'post',
        route : '/v1/upload',
        execute : Upload.upload,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : '/v1/download_unit_template',
        execute : Upload.download,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : '/v1/download_template/:filename',
        execute : Upload.download,
        middleware : []
    },


    {
        method : 'post',
        route : '/v1/reward/claim',
        execute : Reward.claim,
        middleware : []
    },
    {
        method : 'get',
        route : '/v1/reward/:filename',
        execute : Reward.get,
        middleware : []
    },


    {
        method : 'get',
        route : '/v1/dashboard',
        execute : Dashboard.get,
        middleware : [checkToken]
    },

    {
        method : 'get',
        route : "/v1/events/",
        execute : EventsController.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/events/:id",
        execute : EventsController.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/events/",
        execute : EventsController.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/events/:id",
        execute : EventsController.put,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/events/:id/approval",
        execute : EventsController.approvalChange,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/events/:id",
        execute : EventsController.del,
        middleware : [checkToken]
    },

    {
        method : 'get',
        route : "/v1/commission/",
        execute : CommissionController.get,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/commission_request/",
        execute : CommissionRequestModule.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/commission_request/:id",
        execute : CommissionRequestModule.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/commission_request/",
        execute : CommissionRequestModule.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/commission_request/:id",
        execute : CommissionRequestModule.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/commission_request/:id",
        execute : CommissionRequestModule.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/commission_report/",
        execute : CommissionReportModule.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/commission_report/:id",
        execute : CommissionReportModule.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/commission_report/",
        execute : CommissionReportModule.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/commission_report/:id",
        execute : CommissionReportModule.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/commission_report/:id",
        execute : CommissionReportModule.del,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/get_unit_akad_by_agent_id/:id",
        execute : Unit.getUnitAkadByAgentId,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/get_customer_unit/:customer_id",
        execute : Customer.getCustomerUnit,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/customer_analyst/",
        execute : CustomerAnalystModule.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/customer_analyst/:id",
        execute : CustomerAnalystModule.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/customer_analyst/",
        execute : CustomerAnalystModule.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/customer_analyst/:id",
        execute : CustomerAnalystModule.put,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/customer_analyst/:id/approval",
        execute : CustomerAnalystModule.approvalChange,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/customer_analyst/:id",
        execute : CustomerAnalystModule.del,
        middleware : [checkToken]
    },

    {
        method : 'get',
        route : "/v1/hrd_reports/",
        execute : HrdReportModule.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/hrd_reports_chart/",
        execute : HrdReportModule.getChart,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/hrd_reports/:id",
        execute : HrdReportModule.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/hrd_reports/",
        execute : HrdReportModule.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/hrd_reports/:id",
        execute : HrdReportModule.put,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/hrd_reports/:id/approval",
        execute : HrdReportModule.approvalChange,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/hrd_reports/:id",
        execute : HrdReportModule.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/bank_accounts/",
        execute : BankAccountModule.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/bank_accounts/:id",
        execute : BankAccountModule.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/bank_accounts/",
        execute : BankAccountModule.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/bank_accounts/:id",
        execute : BankAccountModule.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/bank_accounts/:id",
        execute : BankAccountModule.del,
        middleware : [checkToken]
    },


    {
        method : 'get',
        route : "/v1/accounts/",
        execute : AccountModule.get,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/accounts_header",
        execute : AccountModule.getHeader,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/accounts_child",
        execute : AccountModule.getByParent,
        middleware : [checkToken]
    },
    {
        method : 'get',
        route : "/v1/accounts/:id",
        execute : AccountModule.getDetail,
        middleware : [checkToken]
    },
    {
        method : 'post',
        route : "/v1/accounts/",
        execute : AccountModule.post,
        middleware : [checkToken]
    },
    {
        method : 'put',
        route : "/v1/accounts/:id",
        execute : AccountModule.put,
        middleware : [checkToken]
    },
    {
        method : 'del',
        route : "/v1/accounts/:id",
        execute : AccountModule.del,
        middleware : [checkToken]
    },
];
