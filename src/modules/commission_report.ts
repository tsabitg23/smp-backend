import {generateData} from '../helpers/paging';
import * as uuid from 'uuid';
import {transaction} from 'objection';
import {omit, get as getLodash} from 'lodash';
import { Commission } from '../models/commission';
import { Units } from '../models/units';
import { CommissionReport } from '../models/commission_report';
import { CommissionRequest } from '../models/commission_request';

export class CommissionReportModule{
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				CommissionReport.query().eager('[commission_request.[agent.agency, unit]]').whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}



	static async getDetail(ctx){

		ctx.body = await CommissionReport.query().eager('[commission_request.[agent.agency, unit]]').findById(ctx.params.id);	
	}

	static async post(ctx){
		const req = omit(ctx.request.body,['agency_id','agent_id','unit_id','amount', 'code']);
		await transaction(CommissionReport.knex(), async (trx) => {
			await CommissionReport.query(trx).insert({
				...req,
				project_id : ctx.state.user.selected_project,
				marketing_approval_status : null,
				marketing_desc : null,
				finance_approval_status : null,
				finance_desc : null,
			})

			await CommissionRequest.query(trx).update({
				report_status: true
			}).where('id', req.commission_request_id);
		})
		ctx.body = {
			message: 'success'
		}
	}
	
	static async put(ctx){
		
		await CommissionReport.query().where('id',ctx.params.id).update(ctx.request.body);
		const dataDB = await CommissionReport.query().eager('[commission_request.[agent.agency, unit]]').findOne('id', ctx.params.id);
		if(dataDB.commission_request.distribution_status){
			ctx.status= 400;
			ctx.body = {
				message: 'Komisi sudah didistribusikan sebelumnya'
			};
			return;
		}
		await transaction(CommissionReport.knex(), async (trx)=>{
			const kavlingNo = dataDB.commission_request.unit.kavling_number;
			if(dataDB.finance_approval_status && dataDB.marketing_approval_status && !dataDB.counted_status){
				const dataCommission = await Commission.query(trx).findOne('agent_id', dataDB.commission_request.agent_id);
				let unit_commission_approved = (getLodash(dataCommission,'unit_commission_approved','') || '');
				if(unit_commission_approved){
					unit_commission_approved = unit_commission_approved + `,${kavlingNo}`
				} else {
					unit_commission_approved = kavlingNo;
				}

				let unit_commission_pending:any = (getLodash(dataCommission,'unit_commission_pending','') || '').split(',');
				if(unit_commission_pending.length > 0){
					unit_commission_pending = unit_commission_pending.filter(prop=>prop !== kavlingNo).join(',');
				} else {
					unit_commission_pending = "";
				}
				
			const total_commission_pending = (+getLodash(dataCommission,'total_commission_pending',0) || 0) - (+dataDB.commission_request.amount);
			const total_commission_approved = (+getLodash(dataCommission,'total_commission_approved',0) || 0) + (+dataDB.commission_request.amount);

				if(dataCommission){
					await Commission.query(trx).where('id', dataCommission.id).update({
						unit_commission_approved,
						unit_commission_pending,
						total_commission_approved,
						total_commission_pending,
					})
				} else {
					await Commission.query(trx).insert({
						project_id: dataDB.project_id,
						agent_id : dataDB.commission_request.agent_id,
						unit_commission_approved,
						unit_commission_pending : '',
						total_commission_approved,
						total_commission_pending : 0
					})
				}
				await CommissionReport.query(trx).where('id',ctx.params.id).update({
					counted_status : true
				});

				await CommissionRequest.query(trx).where('id', dataDB.commission_request.id).update({
					distribution_status : true
				})

				await Units.query(trx).where('id', dataDB.commission_request.unit_id).update({
					commission_status : true
				})
            }
            return ctx.request.body
		}).catch(err=>{
			throw err;
		})
		

		ctx.body = {
			message: 'success'
		}
    }
    
	static async del(ctx){
		const dataDB = await CommissionReport.query().findOne('id', ctx.params.id);
		if(dataDB.counted_status){
			ctx.status = 400;
			ctx.body = {
				message : 'Komisi sudah cair, laporan tidak bisa dihapus'
			}
			return;
		}
		ctx.body = await CommissionReport.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}

}