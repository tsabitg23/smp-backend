const constant = require('../../config/const').constant;

module.exports = [
    {
        id : constant.USER.SUPERADMIN,
        entity_id : null,
        role_id : constant.ROLES.SUPERADMIN,
        email : 'superadmin@smp.com',
        password : '3248f392ff34cd006a2da32470767da830d2621d328b77f6b5fa10937bbd013e983cbf65676feebd7f712929513ffab382b1f14bac73c65f3aeb34b8451fc0461f652ae8e32b882a1c0aecf93577fa669f91fd37003f821bb6aa371643101bb3b331e351',
        salt : '6a26a77981f04356a71d2d70a13f39d8'
    },
    {
        id : constant.USER.ADMIN,
        entity_id : constant.ENTITY.MAIN,
        role_id : constant.ROLES.ADMIN,
        email : 'admin@smp.com',
        password : '3248f392ff34cd006a2da32470767da830d2621d328b77f6b5fa10937bbd013e983cbf65676feebd7f712929513ffab382b1f14bac73c65f3aeb34b8451fc0461f652ae8e32b882a1c0aecf93577fa669f91fd37003f821bb6aa371643101bb3b331e351',
        salt : '6a26a77981f04356a71d2d70a13f39d8'
    },
];