
exports.up = function(knex, Promise) {
    return knex.schema.table("commission_report", table => {
        table.boolean('counted_status').defaultTo(false);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("commission_report", table => {
        table.dropColumn('counted_status');
    })
};
