import {ProjectJobs} from '../models/project_jobs';
import {generateData} from '../helpers/paging';
import {TaskHelper} from '../helpers/task';
import {transaction} from 'objection';
import * as uuid from 'uuid';
import {constant} from '../../config/const';
import {Users} from '../models/users';
import {UserProjects} from '../models/user_projects';

export class ProjectJob {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				ProjectJobs.query().eager('[task]').whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){

		ctx.body = await ProjectJobs.query().eager('[task]').findById(ctx.params.id);
	}

	static async post(ctx){
		await transaction(ProjectJobs.knex(),async trx=>{
			let Id = uuid.v4();
			let userTarget:any = await UserProjects.query().select('user_id as id').innerJoinRelation('project')
			.innerJoinRelation('user').where({
				['project.id'] : ctx.state.user.selected_project,
				['user.deleted_at'] : null,
				['user.role_id'] : constant.ROLES.MR
			});
			let taskData = await TaskHelper.createTask('project_jobs',ctx.state.user.selected_project,{
					project_job_id : Id
				},
				ctx.state.user.user_id,
				userTarget.map(it=>it.id)
			);

			await ProjectJobs.query()
				.insert(
					Object.assign({
						project_id : ctx.state.user.selected_project,
						id:Id,
						task_id : taskData.task_id
					},
					ctx.request.body)
				);
				
			ctx.body = {
				message : 'Success'
			}
		})
	}

	static async updateTask(ctx){
		let data = await ProjectJobs.query().findOne('id',ctx.params.id);
		
		let userTarget:any = await UserProjects.query().select('user_id as id').innerJoinRelation('project')
			.innerJoinRelation('user').where({
				['project.id'] : ctx.state.user.selected_project,
				['user.deleted_at'] : null,
				['user.role_id'] : constant.ROLES.SUPERVISOR
			});
			
		ctx.body = await TaskHelper.update(
			data.task_id,
			ctx.request.body['task.status'],
			userTarget.map(it=>it.id),
			'project_jobs',
			{
				project_job_id : ctx.params.id
			},
			ctx.state.user.selected_project
		)
	}


	static async put(ctx){
		ctx.body = await ProjectJobs.query().where('id',ctx.params.id).update(ctx.request.body)
	}


	static async del(ctx){
		ctx.body = await ProjectJobs.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}