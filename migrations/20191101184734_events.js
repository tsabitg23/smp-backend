exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('events', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.string('name');
            table.date('date');
            table.string('place');
            table.text('desc');
            table.text('target');
            table.text('achievement');
            table.text('evaluation');
            table.string('status');
            table.integer('total');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('events');
};