module.exports = [
    {
        id : "f871e713-4b2e-419d-bd51-ca1098cea1be",
        project_id : null,
        parent_id : null,
        code : "10000",
        name : "AKTIVA",
        position : "debit",
        category : "neraca"
    },
    {
        id : "65a29d57-f159-43a4-8095-b071872092ad",
        project_id : null,
        parent_id : "f871e713-4b2e-419d-bd51-ca1098cea1be",
        code : "11000",
        name : "Aktiva Lancar",
        position : "debit",
        category : "neraca"
    },
    {
        id : "b5bc2a8a-d336-4d57-9949-ce3f081dbeab",
        project_id : null,
        parent_id : "65a29d57-f159-43a4-8095-b071872092ad",
        code : "11100",
        name : "Kas",
        position : "debit",
        category : "neraca"
    },
    {
        id : "ae30d4c6-b92a-4bfa-b493-1887453527a8",
        project_id : null,
        parent_id : "65a29d57-f159-43a4-8095-b071872092ad",
        code : "11200",
        name : "Bank",
        position : "debit",
        category : "neraca"
    },
    {
        id : "e3ac8316-d21c-40f0-8c24-b25ad00d0034",
        project_id : null,
        parent_id : "65a29d57-f159-43a4-8095-b071872092ad",
        code : "11300",
        name : "Piutang",
        position : "debit",
        category : "neraca"
    },
    {
        id : "a6b5a6d2-6840-4a71-bd99-15cebd55de4a",
        project_id : null,
        parent_id : "65a29d57-f159-43a4-8095-b071872092ad",
        code : "11400",
        name : "Perlengkapan",
        position : "debit",
        category : "neraca"
    },
    {
        id : "6013b1ca-0b7c-4109-8e1a-b27d5a54571d",
        project_id : null,
        parent_id : "65a29d57-f159-43a4-8095-b071872092ad",
        code : "11500",
        name : "Sewa dibayar dimuka",
        position : "debit",
        category : "neraca"
    },
    {
        id : "c828c2ae-d54d-429e-8cde-88f5afd60552",
        project_id : null,
        parent_id : "f871e713-4b2e-419d-bd51-ca1098cea1be",
        code : "12000",
        name : "Aktiva Tetap",
        position : "debit",
        category : "neraca"
    },
    {
        id : "7ae4d3f0-ae37-44b0-8fb4-377619acf647",
        project_id : null,
        parent_id : "c828c2ae-d54d-429e-8cde-88f5afd60552",
        code : "12100",
        name : "Tanah",
        position : "debit",
        category : "neraca"
    },
    {
        id : "f7947d70-d2f1-4b77-bcd3-a49717ea57c4",
        project_id : null,
        parent_id : "c828c2ae-d54d-429e-8cde-88f5afd60552",
        code : "12200",
        name : "Mobil",
        position : "debit",
        category : "neraca"
    },
    {
        id : "08d126dc-d9fc-470c-8edd-1298a656a8e8",
        project_id : null,
        parent_id : "c828c2ae-d54d-429e-8cde-88f5afd60552",
        code : "12300",
        name : "Gedung",
        position : "debit",
        category : "neraca"
    },
    {
        id : "34a3b6f0-3eb8-4899-a143-8f9fecc8cf6d",
        project_id : null,
        parent_id : "c828c2ae-d54d-429e-8cde-88f5afd60552",
        code : "12400",
        name : "Peralatan Kantor",
        position : "debit",
        category : "neraca"
    },
    {
        id : "84041ddc-99d0-440a-941f-831825a07ab6",
        project_id : null,
        parent_id : "c828c2ae-d54d-429e-8cde-88f5afd60552",
        code : "12500",
        name : "Penyusutan Peralatan Kantor",
        position : "debit",
        category : "neraca"
    },
    {
        id : "1e46a9c0-efec-4208-9e06-cee7557fce6b",
        project_id : null,
        parent_id : null,
        code : "20000",
        name : "KEWAJIBAN",
        position : "kredit",
        category : "neraca"
    },
    {
        id : "9d59aff2-ce0d-45a0-959d-c59d89316d82",
        project_id : null,
        parent_id : "1e46a9c0-efec-4208-9e06-cee7557fce6b",
        code : "21000",
        name : "Kewajiban Lancar",
        position : "kredit",
        category : "neraca"
    },
    {
        id : "61b84ada-62fa-44cb-8dfe-c4b0c42b987c",
        project_id : null,
        parent_id : "9d59aff2-ce0d-45a0-959d-c59d89316d82",
        code : "21100",
        name : "Gaji Upah",
        position : "kredit",
        category : "neraca"
    },
    {
        id : "34f1c08f-9dcf-4855-b56e-31eb107eaaca",
        project_id : null,
        parent_id : "1e46a9c0-efec-4208-9e06-cee7557fce6b",
        code : "22000",
        name : "Kewajiban Jangka Panjang",
        position : "kredit",
        category : "neraca"
    },
    {
        id : "58c4404a-9555-4358-927b-ab8885de491c",
        project_id : null,
        parent_id : "34f1c08f-9dcf-4855-b56e-31eb107eaaca",
        code : "20000",
        name : "Hutang Bank",
        position : "kredit",
        category : "neraca"
    },
    {
        id : "c41faa62-9aa7-4c2c-a3fe-edb0e0e4e6dc",
        project_id : null,
        parent_id : null,
        code : "30000",
        name : "MODAL",
        position : "kredit",
        category : "neraca"
    },
    {
        id : "d5ffd085-04e3-4c13-bfff-a703da6b0339",
        project_id : null,
        parent_id : "c41faa62-9aa7-4c2c-a3fe-edb0e0e4e6dc",
        code : "31000",
        name : "Modal Saham Perusahaan",
        position : "kredit",
        category : "neraca"
    },
    {
        id : "04e0f151-0811-426f-9f26-337349e81c04",
        project_id : null,
        parent_id : "c41faa62-9aa7-4c2c-a3fe-edb0e0e4e6dc",
        code : "32000",
        name : "Saldo Laba",
        position : "kredit",
        category : "neraca"
    },
    {
        id : "df6f2fda-2763-4a48-aa0f-5cf9af5bf9a5",
        project_id : null,
        parent_id : null,
        code : "40000",
        name : "PENDAPATAN",
        position : "kredit",
        category : "LABA RUGI"
    },
    {
        id : "2882b86c-3f6d-4426-a528-e50f1b6094f0",
        project_id : null,
        parent_id : "df6f2fda-2763-4a48-aa0f-5cf9af5bf9a5",
        code : "41000",
        name : "Pendapatan Booking Fee Diterima",
        position : "kredit",
        category : "LABA RUGI"
    },
    {
        id : "1df2a990-6ecc-48e2-b78b-d62c517d53b2",
        project_id : null,
        parent_id : "df6f2fda-2763-4a48-aa0f-5cf9af5bf9a5",
        code : "42000",
        name : "Pendapatan Down Payment Rumah",
        position : "kredit",
        category : "LABA RUGI"
    },
    {
        id : "f53c7921-6923-44fc-b8be-68a83454b904",
        project_id : null,
        parent_id : "df6f2fda-2763-4a48-aa0f-5cf9af5bf9a5",
        code : "43000",
        name : "Pendapatan Angsuran Rumah",
        position : "kredit",
        category : "LABA RUGI"
    },
    {
        id : "715f7ff4-5d7d-428c-8f53-cba06f680ccf",
        project_id : null,
        parent_id : null,
        code : "50000",
        name : "BEBAN",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "0100a7fd-a938-4998-a629-18012fe70dc2",
        project_id : null,
        parent_id : "715f7ff4-5d7d-428c-8f53-cba06f680ccf",
        code : "51000",
        name : "BIAYA PEROLEHAN LAHAN",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "2964f4bf-b79b-48e5-a55f-9a4b07dd0985",
        project_id : null,
        parent_id : "0100a7fd-a938-4998-a629-18012fe70dc2",
        code : "51100",
        name : "Biaya Pembelian Lahan",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "1693370a-5227-47a6-9d01-204fc98fba9a",
        project_id : null,
        parent_id : "0100a7fd-a938-4998-a629-18012fe70dc2",
        code : "51200",
        name : "Biaya Legal Pembelian Lahan",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "050daa62-2186-4426-b486-b2f996c730dd",
        project_id : null,
        parent_id : "0100a7fd-a938-4998-a629-18012fe70dc2",
        code : "51300",
        name : "Sertifikasi Lahan",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "9e3705ce-da67-45d7-8f12-156467a11a13",
        project_id : null,
        parent_id : "0100a7fd-a938-4998-a629-18012fe70dc2",
        code : "51400",
        name : "Pajak- Pajak Perolehan Lahan",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "7f271b2d-3852-4830-94c2-2784f4768134",
        project_id : null,
        parent_id : "715f7ff4-5d7d-428c-8f53-cba06f680ccf",
        code : "52000",
        name : "Biaya Perijinan & Perencanaan",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "ba6b0c94-1225-489e-ac4f-5e155fe5b99b",
        project_id : null,
        parent_id : "715f7ff4-5d7d-428c-8f53-cba06f680ccf",
        code : "53000",
        name : "BIAYA PEMATANGAN LAHAN",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "f26c5ad9-4742-4bda-a996-f263e51c0bc9",
        project_id : null,
        parent_id : "ba6b0c94-1225-489e-ac4f-5e155fe5b99b",
        code : "53100",
        name : "Biaya Infrastruktur",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "4bbfad5f-4895-4451-8d0c-850d974194c7",
        project_id : null,
        parent_id : "ba6b0c94-1225-489e-ac4f-5e155fe5b99b",
        code : "53200",
        name : "Biaya Utilitas",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "b166969e-e727-4d13-b6d6-d50aedd7bc62",
        project_id : null,
        parent_id : "ba6b0c94-1225-489e-ac4f-5e155fe5b99b",
        code : "53300",
        name : "Biaya Fasilitas Sosial / Umum",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "3deab134-b067-4c74-b6d9-96f852131434",
        project_id : null,
        parent_id : "ba6b0c94-1225-489e-ac4f-5e155fe5b99b",
        code : "53400",
        name : "Pemeliharaan dan Pembinaan Lingkungan",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "9b9272ac-84b9-47b2-bd51-5eb632f9c912",
        project_id : null,
        parent_id : "715f7ff4-5d7d-428c-8f53-cba06f680ccf",
        code : "54000",
        name : "BIAYA DIBAYAR DIMUKA (OHC)",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "a9b6d132-2092-4a06-b775-1f92e77c7e8c",
        project_id : null,
        parent_id : "9b9272ac-84b9-47b2-bd51-5eb632f9c912",
        code : "54100",
        name : "Biaya Persiapan Kantor dan Inventaris",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "19224980-ac1a-4d3a-8fde-ffdd05673a84",
        project_id : null,
        parent_id : "9b9272ac-84b9-47b2-bd51-5eb632f9c912",
        code : "54200",
        name : "Biaya Operasional Kantor",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "faa39e1f-22ad-42e0-9201-e370874d03ed",
        project_id : null,
        parent_id : "9b9272ac-84b9-47b2-bd51-5eb632f9c912",
        code : "54300",
        name : "Biaya Gaji Karyawan",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "107876ec-fe5d-4296-9efd-73a0b2cb68a8",
        project_id : null,
        parent_id : "9b9272ac-84b9-47b2-bd51-5eb632f9c912",
        code : "54400",
        name : "Biaya Promosi",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "7f8e9270-e202-4ae5-b9d0-b962e4b6b6ed",
        project_id : null,
        parent_id : "9b9272ac-84b9-47b2-bd51-5eb632f9c912",
        code : "54500",
        name : "Biaya Kesejahteraan",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "c3aa7196-1451-4877-a96f-de1f11b4de43",
        project_id : null,
        parent_id : "715f7ff4-5d7d-428c-8f53-cba06f680ccf",
        code : "55000",
        name : "Biaya Rumah",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "44637788-48d3-4d13-b2ec-04f84c37522d",
        project_id : null,
        parent_id : "715f7ff4-5d7d-428c-8f53-cba06f680ccf",
        code : "56100",
        name : "Biaya -",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "5f72739c-e834-4459-a182-132c1e435755",
        project_id : null,
        parent_id : "715f7ff4-5d7d-428c-8f53-cba06f680ccf",
        code : "56200",
        name : "Biaya Pajak - Pajak Penjualan kavling atau unit rumah",
        position : "debit",
        category : "LABA RUGI"
    },
    {
        id : "f18ed9df-307a-47f3-879b-f0aa9198cc32",
        project_id : null,
        parent_id : "715f7ff4-5d7d-428c-8f53-cba06f680ccf",
        code : "57100",
        name : "Biaya Lain - lain",
        position : "debit",
        category : "LABA RUGI"
    },
]
