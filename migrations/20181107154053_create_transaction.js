exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('transactions', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('accounting_code_id').references('accounting_codes.id').onDelete('restrict');
            table.uuid('transaction_type_id').references('transaction_types.id').onDelete('restrict');
            table.uuid('user_creator_id').references('users.id').onDelete('restrict');
            table.date('date');
            table.decimal('amount',14,2);
            table.string('invoice_number');
            table.text('description');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('transactions');
};