import BaseModel from "./base_model";
import {Model} from "objection";
import {Users} from './users';
import {Projects} from './projects';

export class UserProjects extends BaseModel {
	user_id : string;
	project_id : string;
    id : string;
	user ?: Users;
	project ?: Projects;

    static get tableName() {
        return 'user_projects';
    }

    static get relationMappings() {
        return {
        	user : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.user_id`,
        			to : `${Users.tableName}.id`
        		}
        	},
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	}
        }
    }
}