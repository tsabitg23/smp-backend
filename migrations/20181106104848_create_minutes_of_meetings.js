exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('minutes_of_meetings', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('user_id').references('users.id').onDelete('restrict');
            table.datetime('meeting_date');
            table.text('description');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('minutes_of_meetings');
};