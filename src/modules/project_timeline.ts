import {ProjectTimelines} from '../models/project_timelines';
import {generateData} from '../helpers/paging';
import * as moment from 'moment';
export class ProjectTimeline {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		const data = await generateData(
				ProjectTimelines.query().eager('[employee]').whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);

		ctx.body = data.data.map(prop=>{
			const start = moment(prop.date);
			const end = moment(prop.end_date);
			return {
				...prop,
				duration : end.diff(start, 'days')
			}
		})
	}

	static async getDetail(ctx){

		ctx.body = await ProjectTimelines.query().eager('[employee]').findById(ctx.params.id);
	}

	static async post(ctx){
		ctx.body = await ProjectTimelines.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
	}


	static async put(ctx){
		ctx.body = await ProjectTimelines.query().where('id',ctx.params.id).update(ctx.request.body)
	}


	static async del(ctx){
		ctx.body = await ProjectTimelines.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}