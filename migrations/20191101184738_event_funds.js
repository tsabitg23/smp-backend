exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('event_funds', (table) => {
            table.uuid('id').primary();
            table.uuid('event_id').references('events.id').onDelete('restrict');
            table.string('name');
            table.integer('amount');
            table.text('desc');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('event_funds');
};