import {UserProfile} from '../models/user_profile';

export class Profile{
	
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await UserProfile.query().findOne('user_id',ctx.state.user.user_id);
	}

	static async put(ctx){
		ctx.body = await UserProfile.query().where('user_id',ctx.state.user.user_id).update(ctx.request.body)
	}
}