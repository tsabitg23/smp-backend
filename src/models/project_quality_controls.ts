import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';

export class ProjectQualityControls extends BaseModel {
	project_id : string;
	name : string;
	date : string;
	type : string;
	impact : string;
	description : string;
	project ?: Projects;
	
    static get tableName() {
        return 'project_quality_controls';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	}
        }
    }
}