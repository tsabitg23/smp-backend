import {UnitAkad as UnitAkadModel} from '../models/unit_akad';
import {generateData} from '../helpers/paging';
import {VerificationQuestions} from '../models/verification_questions';
import {UnitAkadVerification} from '../models/unit_akad_verification';
import {generateHtml} from '../helpers/template';
import {sendMail} from '../helpers/email';
import {appConfig} from '../../config/app';
import {constant} from '../../config/const';
import {get} from 'lodash';
import * as uuid from 'uuid';
import {transaction} from 'objection';

let getValue = (type,is_favorable,value)=>{
	if(type === 'five_option'){
		if(is_favorable){
			if(value === 'ss'){
				return 5
			}
			else if(value === 's'){
				return 4
			}
			else if(value === 'n'){
				return 3
			}
			else if(value === 'ts'){
				return 2
			}
			else if(value === 'sts'){
				return 1
			}
			else {
				return null
			}
		}
		else {
			if(value === 'ss'){
				return 1
			}
			else if(value === 's'){
				return 2
			}
			else if(value === 'n'){
				return 3
			}
			else if(value === 'ts'){
				return 4
			}
			else if(value === 'sts'){
				return 5
			}
			else {
				return null
			}
		}
	}
	else if(type === 'true_false'){
		if(is_favorable){
			if(value === 'benar'){
				return 1
			}
			else if(value ==='salah'){
				return 0
			}
			else {
				return null
			}
		}
		else{
			if(value === 'benar'){
				return 0
			}
			else if(value === 'salah'){
				return 1
			}
			else {
				return null
			}
		}
	}
}


export class UnitAkad{
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				UnitAkadModel.query().eager('[unit,agent]')
					.leftJoinRelation('unit')
					.where('unit.project_id',ctx.state.user.selected_project)
					.where('unit.deleted_at',null)
					.orWhere('unit_akad.project_id', ctx.state.user.selected_project)
					.whereNull('unit_akad.deleted_at'),
				ctx
			);
	}

	static async getNewData(ctx){
		let questionData = await VerificationQuestions.query()
				.where('deleted_at',null)
				.orderBy('type', 'asc')
  				.orderBy('order', 'asc');

			let verifData = {
				name : '',
				unit_akad_id : ctx.params.id,
				ktp_number : '',
				gender : '',
				city : '',
				occupation : '',
				test_result : questionData.map(it=>{
					return {
						...it,
						...{point : null}
					}
				}),
				scores : []
			}

			ctx.body = {
				verification : verifData,
				test_result  : verifData.test_result.length
			}
	}

	static async getDetail(ctx){
		let res = await UnitAkadModel.query().eager('[unit.project,agent]').findOne({
			id : ctx.params.id,
			deleted_at : null
		});

		if(!res){
			throw new Error('Verification Data Not Found');
		}

		let resVerif = await UnitAkadVerification.query().findOne({
			unit_akad_id : ctx.params.id
		});

		let verifData:any;
		if(resVerif){
			verifData = resVerif;
			verifData.test_result = JSON.parse(verifData.test_result);
			// let questionIds = verifData.test_result.map(it=>it.verification_question_id);
			// let questionData = await VerificationQuestions.query()
			// 	.findByIds(questionIds)
			// 	.where('deleted_at',null)
			// 	.orderBy('type', 'asc')
  	// 			.orderBy('order', 'asc');

			// verifData.test_result = verifData.test_result.map(it=>{
			// 	let question = questionData.find(prop=>prop.id === it.verification_question_id) || {};

			// 	return {
			// 		...it,
			// 		...question
			// 	}
			// });
			verifData.scores = [
				{
					type : 'five_option',
					total_score : 0,
					data : []
				},
				{
					type : 'true_false',
					total_score : 0,
					data : []
				}
			];
			verifData.test_result.map(it=>{
				let nilai = (it.point) ? (+it.point) : 0
				let index = verifData.scores.findIndex(prop=>prop.type === it.type);
				if(index > -1){
					verifData.scores[index].total_score+=nilai;
					let categoryIndex = verifData.scores[index].data.findIndex(prop=>prop.category === it.category);
					if(categoryIndex > -1){
						verifData.scores[index].data[categoryIndex].value += nilai;
					}
					else{
						verifData.scores[index].data.push({value : nilai,category : it.category})
					}
				}
			})

			verifData.scores.map(score=>{
				if(score.type === 'five_option' && score.total_score > 161){
					score.category = 'Tinggi';
					score.desc = "Direkomendasikan";
				}
				else if(score.type === 'five_option' && score.total_score <= 161 && score.total_score >= 125){
					score.category = 'Sedang';
					score.desc = "Perlu Pembinaan/Edukasi";
				}
				else if(score.type === 'five_option' && score.total_score < 125){
					score.category = 'Rendah';
					score.desc = "Tidak Direkomendasikan";
				}
				else if(score.type === 'true_false' && score.total_score > 4){
					score.category = 'Pengetahuan Bagus';
					score.desc = "Direkomendasikan";
				}
				else if(score.type === 'true_false' && score.total_score < 4){
					score.category = 'Pengetahuan Rendah';
					score.desc = "Tidak Direkomendasikan";
				}
				else{
					score.category = 'N/A';
					score.desc = "Unknown";
				}

				score.data.map(cat=>{
					if(score.type === 'five_option'){
						if(cat.category === 'hutang' && cat.value > 43){
							cat.desc = "Tinggi"
						}
						else if(cat.category === 'hutang' && cat.value <= 43 && cat.value >=33){
							cat.desc = "Sedang"
						}
						else if(cat.category === 'hutang' && cat.value < 33){
							cat.desc = "Rendah"
						}
						else if(cat.category === 'riba' && cat.value > 28){
							cat.desc = "Tinggi"
						}
						else if(cat.category === 'riba' && cat.value <= 28 && cat.value >=20){
							cat.desc = "Sedang"
						}
						else if(cat.category === 'riba' && cat.value < 20){
							cat.desc = "Rendah"
						}
						else if(cat.category === 'penegakan_syariat_islam' && cat.value > 33){
							cat.desc = "Tinggi"
						}
						else if(cat.category === 'penegakan_syariat_islam' && cat.value <= 33 && cat.value >=26){
							cat.desc = "Sedang"
						}
						else if(cat.category === 'penegakan_syariat_islam' && cat.value < 26){
							cat.desc = "Rendah"
						}
						else if(cat.category === 'komitmen_dan_tanggung_jawab_dalam_jual_beli' && cat.value > 58){
							cat.desc = "Tinggi"
						}
						else if(cat.category === 'komitmen_dan_tanggung_jawab_dalam_jual_beli' && cat.value <= 58 && cat.value >=43){
							cat.desc = "Sedang"
						}
						else if(cat.category === 'komitmen_dan_tanggung_jawab_dalam_jual_beli' && cat.value < 43){
							cat.desc = "Rendah"
						}
						else{
							cat.desc = "N/A"
						}
					}
					else if(score.type === 'true_false'){
						if(cat.value >= 4){
							cat.desc = 'Pengetahuan Bagus - Direkomendasikan'
						}
						else {
							cat.desc = 'Pengetahuan Kurang - Tidak direkomendasikan'
						}
					}
					return cat;
				})
				return score;
			})
			// verifData.test_result.map(it=>{
			// 	let nilai = (it.point) ? (+it.point) : 0
			// 	let index = verifData.scores.findIndex(prop=>prop.category === it.category);
			// 	if(index > -1){
			// 		verifData.scores[index].point += nilai
			// 	}
			// 	else{
			// 		verifData.scores.push({point : nilai,category : it.category});
			// 	}
			// });
		}
		else{
			let questionData = await VerificationQuestions.query()
				.where('deleted_at',null)
				.orderBy('type', 'asc')
  				.orderBy('order', 'asc');

			verifData = {
				name : '',
				unit_akad_id : ctx.params.id,
				ktp_number : '',
				gender : '',
				city : '',
				occupation : '',
				test_result : questionData.map(it=>{
					return {
						...it,
						...{point : null}
					}
				}),
				scores : []
			}
		}

		ctx.body = {
			...res,
			verification : verifData,
			test_result  : verifData.test_result.length
		}
	}

	static async sendVerification(ctx){

		let html = await generateHtml('text_and_link', {
            target: appConfig.appUrl + `verification_user/${ctx.params.id}`,
            text : 'Please click the button below to go to verification page',
            link_label : 'Verify'
        });

        await sendMail({
            subject: "Verification for customer - SMP",
            text: "Click link below to verify data",
            html: html,
            to: ctx.request.body.email
        });

    	ctx.body = {
    		message : 'success'
    	}
	}

	static async updateEmail(ctx){
  		let data = await UnitAkadModel.query().update({
  			email_to : ctx.request.body.email_to
  		}).where('id',ctx.params.id)


    	ctx.body = {
    		message : 'success'
    	}
	}

	static async saveNewVerification(ctx){
		const {body} = ctx.request;
		const akadId = uuid.v4();
		const akadData = {
			booking_date : body.booking_date,
			verification_date : body.verification_date,
			akad_date: body.akad_date,
			customer : body.name,
			status: null,
			unit_id : body.unit_id,
			agent_id : body.agent_id,
		}
		await transaction(UnitAkadModel.knex(), async (trx) => {

				await UnitAkadModel.query()
					.insert({
						id: akadId,
						project_id : ctx.state.user.selected_project,
						...akadData
					});


				// let questionData = await VerificationQuestions.query()
				// 	.where('deleted_at',null)
				// 	.orderBy('type', 'asc')
				// 	.orderBy('order', 'asc');

				// const defaultTestResult = questionData.map(it=>{
				// 	return {
				// 		...it,
				// 		...{point : null}
				// 	}
				// })

				// let reqData = {
				// 	test_result : defaultTestResult
				// };
				// let testResult = "";
				// if(body.test_result){
				// 	let newData = body.test_result;
				// 	reqData.test_result.map(it=>{
				// 		let newVal = get(newData,it.type+'-'+it.order,false);
				// 		if(newVal){
				// 			it.point = getValue(it.type,it.is_favorable,newVal)
				// 		}
				// 		return it;
				// 	})
				// 	testResult = JSON.stringify(reqData.test_result);
				// }

				// await UnitAkadVerification.query().insert({
				// 	unit_akad_id : akadId,
				// 	name : body.name,
				// 	gender : body.gender,
				// 	city : body.city,
				// 	occupation : body.occupation,
				// 	ktp_number : body.ktp_number,
				// 	test_result : testResult
				// })

				return body;
		})

		ctx.body = {
			message: 'Success'
		}
	}

	static async updateNormal(ctx){
		ctx.body = await UnitAkadModel.query().update(ctx.request.body).where('id', ctx.params.id);
	}

	static async updateVerification(ctx){
		let data = await UnitAkadVerification.query().findOne({unit_akad_id : ctx.params.id});

		if(!data){
			let questionData = await VerificationQuestions.query()
				.where('deleted_at',null)
				.orderBy('type', 'asc')
  				.orderBy('order', 'asc');

			await UnitAkadVerification.query().insert({
				unit_akad_id : ctx.params.id,
				...ctx.request.body,
				test_result : JSON.stringify(questionData.map(it=>{
					return {
						...it,
						...{point : null}
					}
				}))
			})
		}
		else{
			let reqData = ctx.request.body;
			if(reqData.test_result){
				let newData = reqData.test_result;
				reqData.test_result = JSON.parse(data.test_result);
				reqData.test_result.map(it=>{
					let newVal = get(newData,it.type+'-'+it.order,false);
					if(newVal){
						it.point = getValue(it.type,it.is_favorable,newVal)
					}
					return it;
				})
				reqData.test_result = JSON.stringify(reqData.test_result);
			}
			await UnitAkadVerification.query()
				.update(reqData)
				.where('unit_akad_id',ctx.params.id)
		}

		ctx.body = {
			message : 'success'
		};

	}
}
