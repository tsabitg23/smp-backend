import BaseModel from "./base_model";
import {Model} from "objection";

export class Permissions extends BaseModel {
	name : string;
	key : string;
	
    static get tableName() {
        return 'permissions';
    }
}