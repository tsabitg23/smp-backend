
exports.up = function(knex, Promise) {
    return knex.schema.table("unit_akad", table => {
        table.string('payment_method');
        table.string('downpayment');
        table.string('tenor');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("unit_akad", table => {
        table.dropColumn('payment_method');
        table.dropColumn('downpayment');
        table.dropColumn('tenor');
    })
};
