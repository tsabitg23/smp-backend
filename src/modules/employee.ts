import {Employees} from '../models/employees';
import {generateData} from '../helpers/paging';
import {GuestBook} from "../models/guest_book";
import * as moment from "moment";
import {snakeCase, startCase} from "lodash";
import {Projects} from "../models/projects";
import * as path from "path";
import {Agencies} from "../models/agencies";
import {Agents} from "../models/agents";
let xl = require('excel4node');
const excelToJson = require('convert-excel-to-json');

export class Employee{
	static async get(ctx){
		const {body,query={}} = ctx.request;
		console.log(ctx.state.user.selected_project);
		ctx.body = await generateData(
				Employees.query().whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){

		ctx.body = await Employees.query().findById(ctx.params.id);
	}

	static async post(ctx){
		ctx.body = await Employees.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
	}

	static async put(ctx){
		ctx.body = await Employees.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await Employees.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}

	static async exportData(ctx){
		const data = await Employees.query().where('project_id', ctx.state.user.selected_project).whereNull('deleted_at');
		let wb = new xl.Workbook();
		let ws = wb.addWorksheet('Sheet 1');
		let style = wb.createStyle({
			font: {
				color: '#000000',
				size: 11,
			},
			numberFormat: '$#,##0.00; ($#,##0.00); -',
		});
		const header = ['No', 'Nama', 'NIK', 'Jabatan', 'Divisi', 'Atasan Langsung', 'Tanggal Bergabung']
		header.map((prop, index)=>{
			ws.cell(1, (index+1))
				.string(header[index])
				.style(style);
		});

		data.map((prop,index)=>{
			ws.cell(index+2, 1)
				.string(`${index+1}`)
				.style(style)

			ws.cell(index+2, 2)
				.string(prop.name)
				.style(style)

			ws.cell(index+2, 3)
				.string(prop.nik)
				.style(style)

			ws.cell(index+2, 4)
				.string(prop.position)
				.style(style)

			ws.cell(index+2, 5)
				.string(prop.division)
				.style(style)

			ws.cell(index+2, 6)
				.string(prop.superior)
				.style(style)

			ws.cell(index+2, 7)
				.string(prop.join_date ? moment(prop.join_date).format("YYYY-MM-DD HH:mm:ss") : "")
				.style(style)
		})

		const projectData = await Projects.query().findOne('id', ctx.state.user.selected_project);
		const filename = snakeCase(projectData.name)+'_karyawan_'+moment().format('YYYY-MM-DD')+".xlsx";
		const fileDir = path.join('assets',filename)
		await new Promise((resolve, reject)=>{
			wb.write(fileDir, (err, stat)=>{
				if(err){
					reject()
				} else {
					resolve(stat)
				}
			});
		}).catch(err=>{
			throw err
		})
		ctx.body = {
			file : filename
		}
	}

	static async importData(ctx){
		const {request: {body, files},state : {user}} = ctx;
		try{
			const data = excelToJson({
				sourceFile: files.file.path
			});

			// console.log(data,'~~~~~~~~')
			const formattedData = [];
			Object.keys(data).map(prop=>{
				data[prop].slice(1).map(it=>{
					if(Object.keys(it).length >1 ){
						formattedData.push({
							project_id : user.selected_project,
							name : it.A,
							nik : it.B,
							position : it.C,
							division : it.D,
							superior : it.E,
							join_date : it.F
						})
					}
				})
			});
			if(formattedData.length === 0){
				throw new Error('data empty');
			}

			await Employees.query().insert(formattedData);
			ctx.body = {
				message : 'success'
			}
		}
		catch(er){
			throw er
		}

	}
}
