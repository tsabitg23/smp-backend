import {Units} from '../models/units';
import {UnitAkad} from '../models/unit_akad';
import {UnitProgress} from '../models/unit_progress';
import {generateData} from '../helpers/paging';
import {transaction} from 'objection';
import * as moment from 'moment';
import {omit, snakeCase} from 'lodash';
import { appConfig } from '../../config/app';
import { Projects } from '../models/projects';
import * as path from 'path';
var xl = require('excel4node');
import * as send from 'koa-send';

export class Unit {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				Units.query().whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){

		ctx.body = await Units.query().eager('[akad(notDeleted,orderByVerif).agent,progress(notDeleted)]',{
			notDeleted : (builder)=>{
				builder.whereNull('deleted_at');
			},
			orderByVerif : (builder)=>{
				builder.orderBy('booking_date','DESC')
			}
		}).findById(ctx.params.id);
	}

	static async getUnitBookedByPeriod(ctx){
		const {query} = ctx.request;
		const q= Units.query().eager('akad').joinRelation('akad').where('units.project_id',ctx.state.user.selected_project).whereNotNull('akad.booking_date');

		// console.log(query);
		//
		q.where('akad.created_at', '>=', moment(query.period_start).startOf('days').format('YYYY-MM-DD'))
			.where('akad.created_at', '<=', moment(query.period_end).endOf('days').format('YYYY-MM-DD'));

		const data:any = await q;
		let akadData = [];
		data.forEach(unit=>{
			akadData = akadData.concat(unit.akad)
		})
		ctx.body = akadData;
	}

	static async post(ctx){
		ctx.body = await Units.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project,status : 'created'},ctx.request.body));
	}

	static async postAkad(ctx){
		await transaction(UnitAkad.knex(), async(trx)=>{
			if(ctx.request.body.akad_date){
				await Units.query(trx).update({
					status : 'akad',
					akad_by_agent_id : ctx.request.body.agent_id
				}).where('id', ctx.params.id)
			}
			await UnitAkad.query(trx)
			.insert(Object.assign({unit_id : ctx.params.id},ctx.request.body));

			return ctx.request;
		});

		ctx.body = {
			message: 'success'
		}
	}

	static async postProgress(ctx){
		ctx.body = await UnitProgress.query()
			.insert(Object.assign({unit_id : ctx.params.id},ctx.request.body));
	}

	static async put(ctx){
		const data = await Units.query().findOne('id', ctx.params.id);
		const reqData = ctx.request.body;
		if(data.status === 'akad'){
			reqData.status = data.status;
		}
		ctx.body = await Units.query().where('id',ctx.params.id).update(reqData)
	}

	static async putAkad(ctx){
		await transaction(UnitAkad.knex(), async(trx)=>{
			if(ctx.request.body.akad_date){
				await Units.query(trx).update({
					status : 'akad',
					akad_by_agent_id : ctx.request.body.agent_id
				}).where('id', ctx.params.id)
			}
			await UnitAkad.query(trx).where('id',ctx.params.akad_id).update(ctx.request.body)
			return ctx.request;
		});

		ctx.body = {
			message: 'success'
		}
	}

	static async putProgress(ctx){
		ctx.body = await UnitProgress.query().where('id',ctx.params.progress_id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await Units.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}

	static async delAkad(ctx){
		ctx.body = await UnitAkad.query().where('id',ctx.params.akad_id).update({
			deleted_at : new Date().toISOString()
		})
	}

	static async delProgress(ctx){
		ctx.body = await UnitProgress.query().where('id',ctx.params.progress_id).update({
			deleted_at : new Date().toISOString()
		})
	}

	static async getUnitAkadByAgentId(ctx){
		ctx.body = await Units.query().where({
			'akad_by_agent_id': ctx.params.id,
			status : 'akad',
			commission_status : false
		});
	}

	static async getChart(ctx){
		const {query: {agency_id, agent_id, start_date, end_date, numberOfMonth}} = ctx.request;
		const countMount = +numberOfMonth;
		let bookingQuery = `select count(a.id) as booking from units a `+
		`left join unit_akad ua on ua.unit_id = a.id `+
		`where a.project_id ='${ctx.state.user.selected_project}' `+
		`and EXTRACT(MONTH FROM ua.created_at) = dataMonth.monthNumber `+
		`and ua.booking_date is not null`;

		let verificationQuery = `select count(a.id) as verification from units a `+
		`left join unit_akad ua on ua.unit_id = a.id `+
		`where a.project_id ='${ctx.state.user.selected_project}' `+
		`and EXTRACT(MONTH FROM ua.created_at) = dataMonth.monthNumber `+
		`and ua.verification_date is not null`;

		let akadQuery = `select count(a.id) as akad from units a `+
		`left join unit_akad ua on ua.unit_id = a.id `+
		`where a.project_id ='${ctx.state.user.selected_project}' `+
		`and EXTRACT(MONTH FROM ua.created_at) = dataMonth.monthNumber `+
		`and ua.akad_date is not null`;

		let availableQuery = `select (count(id)) as available from units where project_id = '${ctx.state.user.selected_project}' and EXTRACT(MONTH FROM created_at) <= dataUnit.monthNumber`;

		const chartOne = await Units.knex().raw(`select *,
			(${availableQuery})
		from 
			(select dataMonth.monthNumber,
				(${bookingQuery}),
				(${verificationQuery}),
				(${akadQuery})
			from
				(select extract(month from current_date + interval '1 month' - interval '1 month' * a ) as monthNumber from generate_series(1,${countMount},1) AS s(a)) as dataMonth) as dataUnit`);

		let formattedData = chartOne.rows.map(prop=>{
			return {
				...omit(prop,['monthnumber']),
				name : moment(prop.monthnumber,'M').format('MMMM'),
			}
		});
		ctx.body = formattedData;
	}

	static async exportData(ctx){
		const data = await Units.query().where('project_id', ctx.state.user.selected_project).whereNull('deleted_at');
		var wb = new xl.Workbook();
		var ws = wb.addWorksheet('Sheet 1');
		var style = wb.createStyle({
			font: {
			  color: '#000000',
			  size: 11,
			},
			numberFormat: '$#,##0.00; ($#,##0.00); -',
		  });
		const header = ['No', 'Nomor kavling', 'Luas Tanah', 'Luas Bangunan', 'Status' , 'Status Komisi', 'tanggal data dibuat']
		header.map((prop, index)=>{
			ws.cell(1, (index+1))
			.string(header[index])
			.style(style);
		});

		const getStatus = (stat)=>{
			if(stat == 'sold'){
				return 'terjual'
			} else if(stat == 'booking_fee_paid'){
				return 'booking'
			} else {
				return stat
			}
		}

		data.map((prop,index)=>{
			ws.cell(index+2, 1)
			.string(`${index+1}`)
			.style(style)

			ws.cell(index+2, 2)
			.string(prop.kavling_number)
			.style(style)

			ws.cell(index+2, 3)
			.string(prop.land_area)
			.style(style)

			ws.cell(index+2, 4)
			.string(prop.building_area)
			.style(style)

			ws.cell(index+2, 5)
			.string(getStatus(prop.status))
			.style(style)

			ws.cell(index+2, 6)
			.string(prop.commission_status ? 'Cair' : "Belum Cair")
			.style(style)

			ws.cell(index+2, 7)
			.string(moment(prop.created_at).format('YYYY-MM-DD HH:mm:ss'))
			.style(style)
		})

		const projectData = await Projects.query().findOne('id', ctx.state.user.selected_project);
		const filename = snakeCase(projectData.name)+'_unit_'+moment().format('YYYY-MM-DD')+".xlsx";
		const fileDir = path.join('assets',filename)
		await new Promise((resolve, reject)=>{
			wb.write(fileDir, (err, stat)=>{
				if(err){
					reject()
				} else {
					resolve(stat)
				}
			});
		}).catch(err=>{
			throw err
		})

		ctx.body = {
			file : filename
		}

	}
}
