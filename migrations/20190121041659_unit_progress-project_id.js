
exports.up = function(knex, Promise) {
    return knex.schema.table("unit_progress", table => {
        table.uuid('project_id').references('projects.id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("unit_progress", table => {
        table.dropColumn('project_id');
    })
};
