exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('units', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.string('kavling_number');
            table.decimal('land_area',14,2);
            table.decimal('building_area',14,2);
            table.string('status');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('units');
};