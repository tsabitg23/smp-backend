import BaseModel from "./base_model";
import {Model} from "objection";
import {Entities} from './entities';

export class Projects extends BaseModel {
	entity_id : string;
	name : string;
	status : string;
    location : string;
    
	entity ?: Entities;
    static get tableName() {
        return 'projects';
    }

    static get relationMappings() {
        return {
        	entity : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Entities,
        		join : {
        			from : `${this.tableName}.entity_id`,
        			to : `${Entities.tableName}.id`
        		}
        	}
        }
    }
}