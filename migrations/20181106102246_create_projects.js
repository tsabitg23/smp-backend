exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('projects', (table) => {
            table.uuid('id').primary();
            table.uuid('entity_id').references('entities.id').onDelete('restrict');
            table.string('name')
            table.string('status')
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('projects');
};