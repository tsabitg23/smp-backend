import BaseModel from "./base_model";
import {Model} from "objection";

export class AccountingCodes extends BaseModel {
    project_id: string;
    code : string;
    name : string;
    type : string;


    static get tableName() {
        return 'accounting_codes';
    }
}