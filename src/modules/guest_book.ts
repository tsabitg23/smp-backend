import {GuestBook} from '../models/guest_book';
import {generateData} from '../helpers/paging';
import * as moment from 'moment';
import {startCase, snakeCase} from 'lodash';
import * as fs from 'fs-extra-promise';
import { Projects } from '../models/projects';
import * as path from 'path';
const excelToJson = require('convert-excel-to-json');
var xl = require('excel4node');

export class GuestBooks{
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				GuestBook.query().whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getChart(ctx){
		const {body,query} = ctx.request;
		const numberOfMonth = +query.numberOfMonth || 4;
		const dataGuest = await GuestBook.query()
			.whereNull('deleted_at')
			.where('project_id',ctx.state.user.selected_project)
			.where('created_at', '>=', moment().subtract(numberOfMonth-1,'months').startOf('month').format('YYYY-MM-DD'))
			.where('created_at', '<', moment().endOf('month').format('YYYY-MM-DD'));

		const dataDate:any = new Array(numberOfMonth).fill(0).map((prop,index)=>{
			return {
				name : moment().subtract(index,'months').format('MMMM'),
				total: 0,
				walk_in : 0,
				social_media : 0,
				agent : 0,
				event : 0,
				lain_lain : 0
			}
		})
		const nowMonth = moment().format('M')
		dataGuest.map((prop)=>{
			const date = moment(prop.created_at);
			const index = +(nowMonth) - +(date.format('M'));
			if(!dataDate[index]){
				dataDate[index] = {
					name : date.format('MMMM'),
					total : 0,
					walk_in : 0,
					social_media : 0,
					agent : 0,
					event : 0,
					lain_lain : 0
				};
			}
			dataDate[index].total = dataDate[index].total + 1;
			dataDate[index][prop.info_source] = (dataDate[index][prop.info_source] || 0) + 1;
		})
		ctx.body = dataDate;
	}

	static async getDetail(ctx){

		ctx.body = await GuestBook.query().findById(ctx.params.id);
	}

	static async post(ctx){
		ctx.body = await GuestBook.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
	}

	static async put(ctx){
		ctx.body = await GuestBook.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await GuestBook.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}

	static async importData(ctx){
		const {request: {body, files},state : {user}} = ctx;
		try{
			const data = excelToJson({
				sourceFile: files.file.path
			});
			// console.log(data,'~~~~~~~~')
			const formattedData = [];
			Object.keys(data).map(prop=>{
				data[prop].slice(1).map(it=>{
					if(Object.keys(it).length >1 ){
						formattedData.push({
							name : it.B,
							project_id : user.selected_project,
							phone : it.C,
							address : it.D,
							survei_date : it.E,
							created_at : it.A,
							info_source : it.F
						})
					}
				})
			});
			if(formattedData.length === 0){
				throw new Error('data empty');
			}

			await GuestBook.query().insert(formattedData);
			ctx.body = {
				message : 'success'
			}
		}
		catch(er){
			throw er
		}

	}


	static async exportData(ctx){
		const data = await GuestBook.query().where('project_id', ctx.state.user.selected_project).whereNull('deleted_at');
		let wb = new xl.Workbook();
		let ws = wb.addWorksheet('Sheet 1');
		let style = wb.createStyle({
			font: {
			  color: '#000000',
			  size: 11,
			},
			numberFormat: '$#,##0.00; ($#,##0.00); -',
		  });
		const header = ['No', 'Nama', 'No telepon', 'Alamat', 'Tanggal Survey', 'Sumber Info', 'Tanggal data dibuat']
		header.map((prop, index)=>{
			ws.cell(1, (index+1))
			.string(header[index])
			.style(style);
		});

		const getStatus = (stat)=>{
			if(stat == 'sold'){
				return 'terjual'
			} else if(stat == 'booking_fee_paid'){
				return 'booking'
			} else {
				return stat
			}
		}

		data.map((prop,index)=>{
			ws.cell(index+2, 1)
			.string(`${index+1}`)
			.style(style)

			ws.cell(index+2, 2)
			.string(prop.name)
			.style(style)

			ws.cell(index+2, 3)
			.string(prop.phone)
			.style(style)

			ws.cell(index+2, 4)
			.string(prop.address)
			.style(style)

			ws.cell(index+2, 5)
			.string(prop.survei_date ? moment(prop.survei_date).format("YYYY-MM-DD HH:mm:ss") : "")
			.style(style)

			ws.cell(index+2, 6)
			.string(startCase(prop.info_source))
			.style(style)

			ws.cell(index+2, 7)
			.string(moment(prop.created_at).format('YYYY-MM-DD HH:mm:ss'))
			.style(style)
		})

		const projectData = await Projects.query().findOne('id', ctx.state.user.selected_project);
		const filename = snakeCase(projectData.name)+'_buku_tamu_'+moment().format('YYYY-MM-DD')+".xlsx";
		const fileDir = path.join('assets',filename)
		await new Promise((resolve, reject)=>{
			wb.write(fileDir, (err, stat)=>{
				if(err){
					reject()
				} else {
					resolve(stat)
				}
			});
		}).catch(err=>{
			throw err
		})

		ctx.body = {
			file : filename
		}
	}


}
