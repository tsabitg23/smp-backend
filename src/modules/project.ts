import {Projects} from '../models/projects';
import {generateData} from '../helpers/paging';
import {constant} from '../../config/const';
import {UserProjects} from '../models/user_projects';
import {transaction} from 'objection';

export class Project {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		let data = {};
		if(ctx.state.user.role_id === constant.ROLES.ADMIN){
			data = await generateData(
					Projects.query().whereNull('deleted_at'),
					ctx,
					{entity_id : ctx.state.user.entity_id}
				);
		}
		else {
			let userProject = await UserProjects.query().where('user_id',ctx.state.user.user_id);
			data = await generateData(
					Projects.query().whereIn('id',userProject.map(it=>it.project_id)).whereNull('deleted_at'),
					ctx,
					{entity_id : ctx.state.user.entity_id}
				);	
		}

		ctx.body = data;
	}

	static async getDetail(ctx){

		ctx.body = await Projects.query().findById(ctx.params.id);	
	}

	static async post(ctx){
		ctx.body = await Projects.query()
			.insert(Object.assign({entity_id : ctx.state.user.entity_id},ctx.request.body));
	}

	static async put(ctx){
		ctx.body = await Projects.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		await transaction(Projects.knex(),async trx=>{
			await Projects.query(trx).where('id',ctx.params.id).update({
				deleted_at : new Date().toISOString()
			})

			await UserProjects.query(trx).update({deleted_at : new Date().toISOString()}).where('project_id',ctx.params.id);
			
			ctx.body = {
				message : 'success'
			}
		})
	}
}