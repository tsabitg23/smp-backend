import {ProjectReports} from '../models/project_reports';
import {generateData} from '../helpers/paging';
import {Users} from '../models/users';
import {UserProjects} from '../models/user_projects';
import {TaskHelper} from '../helpers/task';
import {transaction} from 'objection';
import * as uuid from 'uuid';
import {constant} from '../../config/const';

export class ProjectReport {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				ProjectReports.query().eager('[task]').whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){

		ctx.body = await ProjectReports.query().eager('[task]').findById(ctx.params.id);
	}

	static async post(ctx){
		await transaction(ProjectReports.knex(),async trx=>{
			let Id = uuid.v4();
			let userTarget:any = await UserProjects.query().select('user_id as id').innerJoinRelation('project')
			.innerJoinRelation('user').where({
				['project.id'] : ctx.state.user.selected_project,
				['user.deleted_at'] : null,
				['user.role_id'] : constant.ROLES.MR
			});
			
			let taskData = await TaskHelper.createTask('project_report',ctx.state.user.selected_project,{
					project_report_id : Id
				},
				ctx.state.user.user_id,
				userTarget.map(it=>it.id)
			);

			await ProjectReports.query()
				.insert(
					Object.assign({
						project_id : ctx.state.user.selected_project,
						id:Id,
						user_id : ctx.state.user.user_id,
						task_id : taskData.task_id
					},
					ctx.request.body)
				);

			ctx.body = {
				message : 'Success'
			}
		});
	}


	static async put(ctx){
		await transaction(ProjectReports.knex(),async trx=>{
			let data = await ProjectReports.query().eager('[task]').findOne('id',ctx.params.id);
			
			if(data.task.status === 'created' && !!ctx.request.body.alternate_recommendation){
				let userTarget:any = await UserProjects.query().select('user_id as id').innerJoinRelation('project')
					.innerJoinRelation('user').where({
						['project.id'] : ctx.state.user.selected_project,
						['user.deleted_at'] : null,
						['user.role_id'] : constant.ROLES.SUPERVISOR
					});
				await TaskHelper.update(
					data.task_id,
					'answered',
					userTarget.map(it=>it.id),
					'project_report',
					{
						project_report_id : ctx.params.id
					},
					ctx.state.user.selected_project
				)
			}

			ctx.body = {
				message : 'Success'
			}
		});
		ctx.body = await ProjectReports.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async reject(ctx){
		let data = await ProjectReports.query().eager('[task]').findOne('id',ctx.params.id);
		let userTarget:any = await UserProjects.query().select('user_id as id').innerJoinRelation('project')
					.innerJoinRelation('user').where({
						['project.id'] : ctx.state.user.selected_project,
						['user.deleted_at'] : null,
						['user.role_id'] : constant.ROLES.SUPERVISOR
					});

		await TaskHelper.update(
			data.task_id,
			'rejected',
			userTarget.map(it=>it.id),
			'project_report',
			{
				project_report_id : ctx.params.id
			},
			ctx.state.user.selected_project
		)

		ctx.body = {
			message : "OK"
		}
	}


	static async del(ctx){
		ctx.body = await ProjectReports.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}