import BaseModel from "./base_model";
import {Model} from "objection";
import {Agents} from './agents';

export class Commission extends BaseModel {
	agent_id : string;
	project_id : string;
	unit_commission_approved : string;
	unit_commission_pending : string;
	total_commission_approved : number;
	total_commission_pending : number;
	
    static get tableName() {
        return 'commission';
    }

    static get relationMappings() {
        return {
            agent : {
                relation : Model.BelongsToOneRelation,
                modelClass : Agents,
                join : {
                    from : `${this.tableName}.agent_id`,
                    to : `${Agents.tableName}.id`
                }
            }
        }
    }

}