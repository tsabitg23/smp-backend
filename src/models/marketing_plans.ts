import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {Employees} from './employees';

export class MarketingPlans extends BaseModel {
	project_id : string;
	employee_id : string;
	type : string;
	status : string;
	date : string;
	manager_approval_status : boolean;
	manager_desc : string;
	place : string;
	cost : number;

	project ?: Projects;
	employee ?: Employees;

    static get tableName() {
        return 'marketing_plans';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
        	employee : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Employees,
        		join : {
        			from : `${this.tableName}.employee_id`,
        			to : `${Employees.tableName}.id`
        		}
        	}
        }
    }
}