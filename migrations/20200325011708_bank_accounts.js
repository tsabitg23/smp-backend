exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('bank_accounts', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.string('bank_name');
            table.string('account_number');
            table.string('on_behalf');
            table.decimal('balance',14,2);
            // table.string('status');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('bank_accounts');
};
