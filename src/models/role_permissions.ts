import BaseModel from "./base_model";
import {Model} from "objection";
import {Roles} from './roles';
import {Permissions} from './permissions';

export class RolePermissions extends BaseModel {
	role_id : string;
	permission_id : string;
	create : boolean;
	update : boolean;
	delete : boolean;
	read : boolean;

	role ?: Roles;
	permission ?: Permissions;

    static get tableName() {
        return 'role_permissions';
    }

    static get relationMappings() {
        return {
        	role : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Roles,
        		join : {
        			from : `${this.tableName}.role_id`,
        			to : `${Roles.tableName}.id`
        		}
        	},
        	permission : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Permissions,
        		join : {
        			from : `${this.tableName}.permission_id`,
        			to : `${Permissions.tableName}.id`
        		}
        	}
        }
    }
}