import {Users} from '../models/users';
import {UserPermissions} from '../models/user_permissions';
import {generateData} from '../helpers/paging';
import {omit} from 'lodash';
import * as uuid from 'uuid';
import {transaction} from 'objection';
import * as crypto from 'crypto';
import {UserProjects} from '../models/user_projects';

export class User {
	static async get(ctx){
		const {body,query={}} = ctx.request;
		
		ctx.body = await generateData(
				Users.query().select(['id','entity_id','role_id','email','created_at','status']).eager('[role]').whereNull('deleted_at'),
				ctx,
				{entity_id : ctx.state.user.entity_id}
			);
	}

	static async getDetail(ctx){

		ctx.body = await Users.query().select(['id','entity_id','role_id','email','created_at','status']).eager('[role,permissions,projects]').findById(ctx.params.id);	
	}

	static async post(ctx){
		const {body} = ctx.request;
		const id = uuid.v4()

		let isEmailAlreadyExist = await Users.query().findOne({
			email : body.email,
			deleted_at : null
		});

		if(isEmailAlreadyExist){
			ctx.status = 400;
            ctx.body = {
                message: 'Email is already taken'
            };
            return;
		}

		let permissions_data = body.permissions.map(it=>{
			it.user_id = id
			return it;
		})

		let projects_data = body.projects.map(it=>{
			return {
				user_id : id,
				project_id : it.value
			}
		})

		let userData =omit(Object.assign({id : id,entity_id : ctx.state.user.entity_id,status : 'registered'},body),['permissions','projects']);
		userData.salt = uuid.v4().replace(/-/g, '');
		userData.password = crypto.pbkdf2Sync(userData.password.toString(), userData.salt, 50, 100, 'sha512').toString('hex')
		
		await transaction(Users.knex(), async(trx)=>{
			await Users.query(trx)
				.insert(userData);
			
			await UserPermissions.query(trx)
				.insert(permissions_data);	
		
			await UserProjects.query(trx)
				.insert(projects_data);
		});

		ctx.body = {
			message : 'success'
		}
	}

	static async postPermission(ctx){
		let isExist = await UserPermissions.query().findOne({
			user_id : ctx.params.id,
			permission_id : ctx.request.body.permission_id
		});

		if(isExist){
			throw new Error('Permission already added');
		}
		ctx.body = await UserPermissions.query().insert(Object.assign({
			user_id : ctx.params.id
		},ctx.request.body));
	}

	static async delPermission(ctx){
		ctx.body = await UserPermissions.query().del().where('id',ctx.params.permission_id);
	}

	static async put(ctx){
		let cleanedData = omit(ctx.request.body,['permissions','projects']);
		let permissions_data = ctx.request.body.permissions.map(it=>{
			it.user_id = ctx.params.id
			return it;
		})

		let projects_data = ctx.request.body.projects.map(it=>{
			return {
				user_id : ctx.params.id,
				project_id : it.value || it.project_id
			}
		})
		
		await transaction(Users.knex(), async(trx)=>{
			await Users.query(trx).where('id',ctx.params.id).update(cleanedData);
			await UserPermissions.query(trx).where('user_id',ctx.params.id).del();
			await UserPermissions.query(trx)
				.insert(permissions_data);
			await UserProjects.query(trx).where('user_id',ctx.params.id).del();
			await UserProjects.query(trx)
				.insert(projects_data);
		})

		ctx.body = {
			message : 'success'
		}
	}

	static async del(ctx){
		await Users.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})

		await UserPermissions.query().where('user_id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})

		await UserProjects.query().where('user_id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})

		ctx.body = {
			message : 'success'
		}
	}
}