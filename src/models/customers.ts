import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {CustomerUnits} from './customer_units';
import {UnitAkad} from "./unit_akad";

interface PhoneData {
	phone : string;
	home_phone : string;
	whatsapp_phone : string;
}

interface CompanyData {
	occupation : string;
	company_name : string;
	company_address : string;
	company_field : string;
	company_phone_number : string;
	engagement_length : string;
}

interface ContactableData {
	contact_name : string;
	contact_relation : string;
	contact_home_phone : string;
	contact_phone : string;
	contact_whatsapp : string;
	contact_email : string;
	contact_address_type : string;
	contact_address : string;
	contact_mail_address : string;
}

interface FinancialData {
	patner_monthly_income : number;
	monthly_expense : number;
	current_installment : string;
	installment_paid : number;
	installment_total : number;
	dependents_number : number;
}

export class Customers extends BaseModel {
	project_id : string;
	name : string;
	email : string;
	gender : string;
	dob : string;
	id_card_type : string;
	id_card_number : string;
	marital_status : string;
	religion : string;
	last_degree : string;
	home_address : string;
	home_status : string;
	phones_data : string;
	company_data : string;
	contactable_data : string;
	financial_data : string;
	birthplace : string;
	project ?:Projects;
	units ?: CustomerUnits[];

    static get tableName() {
        return 'customers';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
        	units : {
        		relation : Model.HasManyRelation,
        		modelClass : CustomerUnits,
        		join : {
        			from : `${this.tableName}.id`,
        			to : `${CustomerUnits.tableName}.customer_id`
        		}
        	},
			akad : {
				relation : Model.HasManyRelation,
				modelClass : UnitAkad,
				join : {
					from : `${this.tableName}.id`,
					to : `${UnitAkad.tableName}.customer_id`
				}
			}
        }
    }
}
