
exports.up = function(knex, Promise) {
    return knex.schema.alterTable("unit_progress", table => {
        table.integer('percentage').alter();
    })
};

exports.down = function(knex, Promise) {
	return knex.schema.alterTable("unit_progress", table => {
        table.decimal('percentage',14,2).alter();
    })  
};
