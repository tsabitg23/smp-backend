export const notifData = {
	project_jobs : {
		title : "Permintaan Kebutuhan",
		desc : "Permintaan baru untuk kebutuhan project"
	},
	project_jobs_approved : {
		title : "Permintaan Kebutuhan diterima",
		desc : "Permintaan telah diterima oleh manager"
	},
	project_jobs_rejected : {
		title : "Permintaan Kebutuhan ditolak",
		desc : "Permintaan telah ditolak oleh manager"
	},
	project_report : {
		title : "Laporan baru",
		desc : "Laporan baru masuk dari pengawas lapangan"
	},
	project_report_answered : {
		title : "Laporan sudah dicheck",
		desc : "Laporan telah dicheck dan dibalas oleh pengawas lapangan"
	},
	project_payment_request : {
		title : "Permintaan Pembiayaan",
		desc : "Permintaan pembiayaan project baru"
	},
	project_payment_request_approved : {
		title : "Permintaan pembiayaan diterima",
		desc : "Permintaan telah diterima oleh manager"
	},
	project_payment_request_rejected : {
		title : "Permintaan pembiayaan ditolak",
		desc : "Permintaan telah ditolak oleh manager"
	},
}