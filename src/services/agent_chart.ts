import * as moment from 'moment';
import { UnitAkad } from '../models/unit_akad';

export const getChartOne = async (selectedProject, numberOfMonth, agency_id='',agent_id='') => {
    const countMount = +numberOfMonth;
    let bookingQuery = `select count(a.id) as booking from unit_akad a ` +
        `left join agents ag on ag.id = a.agent_id `+
        `left join agencies b on b.id = ag.agency_id `+
        `inner join units un on un.id = a.unit_id `+
        `where un.project_id='${selectedProject}' `+
        `and EXTRACT(MONTH FROM a.created_at) = dataMonth.monthNumber and `+
        `a.booking_date is not null`;

    let akadQuery = `select count(a.id) as akad from unit_akad a `+
        `left join agents ag on ag.id = a.agent_id `+
        `left join agencies b on b.id = ag.agency_id `+
        `inner join units un on un.id = a.unit_id `+
        `where un.project_id='${selectedProject}' `+
        `and EXTRACT(MONTH FROM a.created_at) = dataMonth.monthNumber `+
        `and a.akad_date is not null`;

    if(agency_id){
        bookingQuery+= ` and ag.agency_id = '${agency_id}'`;
        akadQuery+= ` and ag.agency_id = '${agency_id}'`;
    }

    if(agent_id){
        bookingQuery+=`  and a.agent_id = '${agent_id}'`;
        akadQuery+=`  and a.agent_id = '${agent_id}'`;
    }
    const chartOne = await UnitAkad.knex().raw(`select dataMonth.monthNumber,
        (${bookingQuery}),
        (${akadQuery})
    from 
        (select extract(month from current_date + interval '1 month' - interval '1 month' * a ) as monthNumber from generate_series(1,${countMount},1) AS s(a)) as dataMonth;`);
    let formattedData = chartOne.rows.map(prop=>{
        return {
            name : moment(prop.monthnumber,'M').format('MMMM'),
            booking : prop.booking,
            akad : prop.akad
        }
    });
    return formattedData;
}