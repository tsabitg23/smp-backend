import BaseModel from "./base_model";

export class CustomerAnalystAccount extends BaseModel {
    project_id : string;
    customer_analyst_id : string;
    bank_name : string;
    month : string;
    start_balance : string;
    credit : string;
    debit : string;
    end_balance : string;


    static get tableName() {
        return 'customer_analyst_account';
    }

}
