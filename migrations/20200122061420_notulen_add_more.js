
exports.up = function(knex, Promise) {
    return knex.schema.table("minutes_of_meetings", table => {
        table.string('place');
        table.string('time');
        table.text('participant');
        table.text('problems');
        table.text('results');
        table.boolean('status');
        table.string('desc');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("minutes_of_meetings", table => {
        table.dropColumn('place');
        table.dropColumn('time');
        table.dropColumn('participant');
        table.dropColumn('problems');
        table.dropColumn('results');
        table.dropColumn('status');
        table.dropColumn('desc');
    })
};
