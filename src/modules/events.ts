import {Events} from '../models/events';
import {CustomerUnits} from '../models/customer_units';
import {generateData} from '../helpers/paging';
import {transaction} from 'objection';
import {omit} from 'lodash';
import * as uuid from 'uuid';
import { EventFunds } from '../models/event_funds';
import { EventCrew } from '../models/event_crew';

export class EventsController {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				Events.query().whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){

		let data = await Events.query().eager('[crew, funds]').findById(ctx.params.id);
		ctx.body = data;

	}

	static async post(ctx){
		const {body} = ctx.request;
		await transaction(Events.knex(),async (trx)=>{
			let eventId = uuid.v4();
			const total = body.funds.reduce((total, data) => total+(+data.amount),0);
			await Events.query(trx)
				.insert({
					project_id : ctx.state.user.selected_project,
					name : body.name,
					date: body.date,
					id: eventId,
					place : body.place,
					target : body.target,
					achievement : body.achievement,
					evaluation : body.evaluation,
					status : 'created',
					total
				});

			await EventFunds.query(trx)
				.insert(body.funds.map(prop=>{
					return {
						...prop,
						event_id : eventId
					}
				}));

			await EventCrew.query(trx)
				.insert(body.crew.map(prop=>{
					return {
						...prop,
						event_id : eventId
					}
				}))

			ctx.body = {
				message : 'Success'
			}
		})
	}

	static async put(ctx){
		const {body} = ctx.request;
		await transaction(Events.knex(),async (trx)=>{

			await Events.query(trx)
				.update({
					name : body.name,
					date: body.date,
					place : body.place,
					desc: body.desc,
					target : body.target,
					achievement : body.achievement,
					evaluation : body.evaluation,
					status : body.status,
					total : body.funds.reduce((total, data) => (+total)+(+data.amount),0)
				}).where('id', ctx.params.id);

			if(body.funds){
				await EventFunds.query(trx).del().where('event_id', ctx.params.id);
				await EventFunds.query(trx)
					.insert(body.funds.map(prop=>{
						return {
							...prop,
							event_id : ctx.params.id
						}
					}));
			}
			if(body.crew){
				await EventCrew.query(trx).del().where('event_id', ctx.params.id);
				await EventCrew.query(trx)
					.insert(body.crew.map(prop=>{
						return {
							...prop,
							event_id : ctx.params.id
						}
					}))
			}


			ctx.body = {
				message : 'Success'
			}
		})
	}

	static async del(ctx){
		await transaction(Events.knex(),async (trx)=>{
			await EventFunds.query(trx).del().where('event_id', ctx.params.id);
			await EventCrew.query(trx).del().where('event_id', ctx.params.id);

			await Events.query(trx).where('id',ctx.params.id).update({
				deleted_at : new Date().toISOString()
			})

			ctx.body = {
				message : 'success'
			}
		})
	}

	static async approvalChange(ctx){
		const {body} = ctx.request;
		ctx.body = await Events.query().update({
			status: body.status,
			desc: body.desc
		}).where('id', ctx.params.id)
	}
}
