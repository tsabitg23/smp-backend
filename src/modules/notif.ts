import {NotifHelper} from '../helpers/notification';
import {NotificationUsers} from '../models/notification_users';
import {UserProjects} from '../models/user_projects';
import {generateData} from '../helpers/paging';

export class Notification {
	static async test(ctx){
		await NotifHelper.send('test',JSON.stringify({data : 1}),['35ec10d9-7157-403d-a4e8-46074dc13c11']);
		ctx.body = {
			message : 'success'
		}

		// ctx.body = await UserProjects.query().select('user_id as id').innerJoinRelation('project')
		// .innerJoinRelation('user').where({
		// 	['project.id'] : ctx.state.user.selected_project,
		// 	['user.deleted_at'] : null
		// });
	}

	static async getAll(ctx){
		let additionalQ = ctx.request.query.read_at ? {'notification_users.read_at' : null} : {};
		ctx.body = await generateData(
			NotificationUsers.query().joinRelation('notification').eager('[notification]').where({
				['notification_users.to_user_id'] : ctx.state.user.user_id,
				['notification.project_id'] : ctx.state.user.selected_project,
				['notification.deleted_at'] : null,
				...additionalQ
			}).orderBy('created_at','desc')
			,ctx);
	}

	static async read(ctx){
		ctx.body = await NotificationUsers.query().update({
			read_at : new Date().toISOString()
		}).where({
			to_user_id : ctx.state.user.user_id,
			notification_id : ctx.params.notification_id
		})
	}
}