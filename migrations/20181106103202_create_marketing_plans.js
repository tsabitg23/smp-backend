exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('marketing_plans', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id');
            table.uuid('user_id').references('users.id');
            table.string('type');
            table.string('status');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('marketing_plans');
};