module.exports = [
	{
		id : '24fb37ee-e1c2-4403-b122-176870ec9c1f',
		name : 'Jika saya baru mendapat uang, saya akan menggunakannya untuk melunasi hutang terlebih dahulu',
		category : 'hutang',
		type : 'five_option',
		is_favorable : true,
		order : 1
	},
	{
		id : '3511db3c-6dc9-41a3-96df-d29974f35a5e',
		name : 'Saya bersegera melunasi hutang',
		category : 'hutang',
		type : 'five_option',
		is_favorable : true,
		order : 2
	},
	{
		id : '9d2933a2-8606-4149-9504-c912c9606dd9',
		name : 'Saya mudah untuk berhutang, namun sulit melunasinya',
		category : 'hutang',
		type : 'five_option',
		is_favorable : false,
		order : 3
	},
	{
		id : '2c27f573-16ef-47e2-8b27-0bcf4c94edd9',
		name : 'Saya tertarik pada layanan tabungan di bank  dengan bunga besar',
		category : 'riba',
		type : 'five_option',
		is_favorable : false,
		order : 4
	},
	{
		id : 'addb4d86-a1a5-4465-8472-5b38a8e3669c',
		name : 'Saya menyarankan orang untuk hutang bank saja daripada harus saya yang meminjamkan uang padanya',
		category : 'riba',
		type : 'five_option',
		is_favorable : false,
		order : 5
	},
	{
		id : 'd09d3fbe-9560-4dc0-bc83-1abf6955db7c',
		name : 'Menurut saya syariat Islam terlalu rumit untuk diterapkan dalam kehidupan sehari-hari',
		category : 'penegakan_syariat_islam',
		type : 'five_option',
		is_favorable : false,
		order : 6
	},
	{
		id : '36e7a1c2-749a-4b59-9c7f-bdfe67ba31ce',
		name : 'Jika melanggar syariat Islam hidup saya diliputi rasa berdosa',
		category : 'penegakan_syariat_islam',
		type : 'five_option',
		is_favorable : true,
		order : 7
	},
	{
		id : '198cc894-8c64-46b5-aead-c7d4a8308410',
		name : 'Syariat Islam bersifat wajib dilaksanakan, saya kadang merasa terpaksa',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : false,
		order : 8
	},
	{
		id : 'e0d5864a-75a2-44f5-91d6-da0da5604ced',
		name : 'Saya memiliki catatan pembayaran barang yang belum lunas supaya saya tidak lupa',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : true,
		order : 9
	},
	{
		id : '21fa8cf6-71a6-45cf-ba44-4346e8e4256f',
		name : 'Saya akan berkomitmen melunasi pembayaran setelah mengumpulkan cukup informasi tentang suatu barang yang dibeli',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : true,
		order : 10
	},
	{
		id : 'ae60061a-d003-4d4b-b05b-12e89283a9a2',
		name : 'Cara musyawarah dapat ditempuh jika saya mengalami masalah dalam bertransaksi dengan penjual',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : true,
		order : 11
	},
	{
		id : '7beff2a1-463e-40e7-95b9-973dfe0cbf00',
		name : 'Saya memberikan informasi atau dokumen pribadi yang valid sebagai bukti bahwa saya sungguh-sungguh akan melunasi angsuran',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : true,
		order : 12
	},
	{
		id : '67e90951-35e9-43d1-a192-9735d15a9299',
		name : 'Saya akan bertanggungjawab penuh atas angsuran barang, dari awal sampai lunas.',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : true,
		order : 13
	},
	{
		id : '0a94d61b-b164-4b14-8b77-f6c8b02f8bad',
		name : 'Saya akan menjalankan kewajiban sebagai pembeli terlebih dahulu, setelah itu hak saya akan terpenuhi',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : true,
		order : 14
	},
	{
		id : 'bb5844ec-afc5-4410-9cb5-5add6fe01533',
		name : 'Saya menerapkan syariat Islam dalam kehidupan dengan ikhlas',
		category : 'penegakan_syariat_islam',
		type : 'five_option',
		is_favorable : true,
		order : 15
	},
	{
		id : '95c15b6d-294e-4a84-9f7d-13ec8c65c810',
		name : 'Apabila melakukan hal yang tidak diperbolehkan dalam syariat Islam saya segera bertaubat',
		category : 'penegakan_syariat_islam',
		type : 'five_option',
		is_favorable : true,
		order : 16
	},
	{
		id : '7f361821-0965-4ec0-81e9-1734b76736d3',
		name : 'Saya tidak berani melanggar syariat Islam karena takut Allah tidak meridhoi kehidupan saya',
		category : 'penegakan_syariat_islam',
		type : 'five_option',
		is_favorable : true,
		order : 17
	},
	{
		id : 'e545903a-8512-427c-9daf-a5e2c871b413',
		name : 'Saya memanfaatkan jasa bank yang dirancang modern, praktis, dan memiliki suku bunga tinggi',
		category : 'riba',
		type : 'five_option',
		is_favorable : false,
		order : 18
	},
	{
		id : '0efd8b41-b06e-4c39-a1b8-4d8659743052',
		name : 'Saya meminjamkan uang kepada orang lain yang membutuhkan tanpa mencari keuntungan uang tambahan',
		category : 'riba',
		type : 'five_option',
		is_favorable : true,
		order : 19
	},
	{
		id : 'b777465c-26d7-4a67-a999-79d92f3016bf',
		name : 'Sebagai seorang muslim saya wajib sadar terhadap keharaman riba',
		category : 'riba',
		type : 'five_option',
		is_favorable : true,
		order : 20
	},
	{
		id : 'ecd8b07d-da7e-45e8-896f-038cb1bc5043',
		name : 'Apabila ingin berhutang, maka saya bertekad dalam hati yang jujur untuk segera melunasi hutang tersebut',
		category : 'hutang',
		type : 'five_option',
		is_favorable : true,
		order : 21
	},
	{
		id : '563a5dca-2f17-450a-861e-11933d1eb604',
		name : 'Saya menjadi orang yang dzalim jika meminjam uang pada orang lain dan menunda pelunasannya',
		category : 'hutang',
		type : 'five_option',
		is_favorable : true,
		order : 22
	},
	{
		id : '5aba4669-6dc3-42f0-9b8e-cb8a3da23246',
		name : 'Saya mampu menahan diri untuk tidak membeli barang lain saat masih punya hutang',
		category : 'hutang',
		type : 'five_option',
		is_favorable : true,
		order : 23
	},
	{
		id : 'b07f0fbf-4b3b-4d47-b5a7-ff14e16c56e7',
		name : 'Saya merasa bersalah pada diri sendiri jika memiliki banyak hutang',
		category : 'hutang',
		type : 'five_option',
		is_favorable : true,
		order : 24
	},
	{
		id : '3d137954-733d-4ac0-8e78-17cbb7fdc1f1',
		name : 'Saya akan nekad kabur seandainya terlilit hutang yang tidak mungkin saya lunasi',
		category : 'hutang',
		type : 'five_option',
		is_favorable : false,
		order : 25
	},
	{
		id : '9c1942cc-4587-481b-91bd-f989a88af925',
		name : 'Islam adalah agama yang sempurna dan saya berusaha menjauhi larangan Allah agar dapat menjadi hamba yang taat',
		category : 'penegakan_syariat_islam',
		type : 'five_option',
		is_favorable : true,
		order : 26
	},
	{
		id : '4c3b490d-7694-4897-b01b-8d29fcabafbf',
		name : 'Hak saya sebagai pembeli akan dipenuhi oleh penjual ketika saya sudah melaksanakan kewajiban saya kepadanya',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : true,
		order : 27
	},
	{
		id : '7afee43f-b262-46f5-8469-076566b6d5b8',
		name : 'Apabila ada masalah di kemudian hari yang berkaitan dengan transaksi jual beli, saya bersedia untuk menyelesaikan sampai tuntas',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : true,
		order : 28
	},
	{
		id : '4202be5a-7a81-422f-83f1-2ade7ed3fc1f',
		name : 'Sebagai pembeli saya akan membayar barang angsuran tidak lebih dari waktu yang disepakati',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : true,
		order : 29
	},
	{
		id : 'd043a572-4c2d-4177-8022-3b44c98e0eee',
		name : 'Saya bersedia terbuka dan jujur membicarakan masalah pembiayaan dengan penjual',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : true,
		order : 30
	},
	{
		id : '692e8d67-df14-44bc-a4f6-1240a868c480',
		name : 'Saya bersedia dipantau/ monitoring apabila pihak penjual menemukan permasalahan saya dalam melunasi barang',
		category : 'komitmen_dan_tanggung_jawab_dalam_jual_beli',
		type : 'five_option',
		is_favorable : true,
		order : 31
	},
	{
		id : '8f02cad0-dd35-4122-b1c5-118d44b3877b',
		name : 'Saya merasa hidup lebih terarah dan tenang dengan menerapkan syariat Islam',
		category : 'penegakan_syariat_islam',
		type : 'five_option',
		is_favorable : true,
		order : 32
	},
	{
		id : 'c45f9c01-6e6f-443f-b948-61fc12310df2',
		name : 'Allah mengharamkan riba dan saya taat pada ketentuan tersebut',
		category : 'riba',
		type : 'five_option',
		is_favorable : true,
		order : 33
	},
	{
		id : 'dc7d183e-6676-4887-9658-dd1421516c38',
		name : 'Saya memiliki tabungan supaya bila ada hutang yang ternyata lupa belum dibayar, bisa segera saya lunasi',
		category : 'hutang',
		type : 'five_option',
		is_favorable : true,
		order : 34
	},
	{
		id : 'bf127dd6-9680-409a-b2b9-808836db1653',
		name : 'Hutang tidak harus dilunasi jika pihak yang memberi pinjaman saja sudah lupa',
		category : 'true_false',
		type : 'true_false',
		is_favorable : false,
		order : 1
	},
	{
		id : '2075a73c-682e-4cb2-a244-5a76edbcc02b',
		name : 'Terdapat tata cara di Al Quran dan hadits yang diatur oleh Islam dalam melakukan transaksi hutang',
		category : 'true_false',
		type : 'true_false',
		is_favorable : true,
		order : 2
	},
	{
		id : '4741a33f-eaf1-4086-bdd3-e7c2b9435c4d',
		name : 'Penambahan yang ditarik dari hutang adalah salah satu bentuk riba',
		category : 'true_false',
		type : 'true_false',
		is_favorable : true,
		order : 3
	},
	{
		id : 'c58f2ca0-866e-4641-b656-485e46d2d048',
		name : 'Adanya denda karena keterlambatan pelunasan hutang adalah bentuk praktik riba',
		category : 'true_false',
		type : 'true_false',
		is_favorable : true,
		order : 4
	},
	{
		id : '0e56514b-bb4d-46d7-a0d0-67c7c541dd3b',
		name : 'Islam telah mengatur syarat dan rukun yang harus dipenuhi dalam transaksi jual beli',
		category : 'true_false',
		type : 'true_false',
		is_favorable : true,
		order : 5
	},
	{
		id : '50ed453c-5652-42e2-89e5-c43db6634fc4',
		name : 'Segala sisi kehidupan manusia berpedoman kepada Al Quran dan hadits',
		category : 'true_false',
		type : 'true_false',
		is_favorable : true,
		order : 6
	},
	
]