exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('hrd_reports', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('employee_id').references('employees.id').onDelete('restrict');
            table.date('period_start');
            table.date('period_end');
            table.integer('total_customer_booking');
            table.integer('total_customer_akad');
            table.integer('total_customer_interviewed');
            table.integer('total_customer_interviewed_passed');
            table.integer('total_customer_interviewed_failed');
            table.integer('total_customer_pending_interview');
            table.integer('total_customer_pending_akad');
            table.boolean('manager_approval_status');
            table.string('manager_desc');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('hrd_reports');
};
