import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {Users} from './users';
import {AccountingCodes} from './accounting_codes';
import {TransactionTypes} from './transaction_types';

export class Transactions extends BaseModel {
	project_id : string;
	accounting_code_id : string;
	transaction_type_id : string;
	user_creator_id : string;
	date : string;
	amount : number;
	invoice_number : string;
	description : string;
	
	project ?: Projects;
	accounting_code ?: AccountingCodes;
	transaction_type ?: TransactionTypes;
	user_creator ?: Users;

    static get tableName() {
        return 'transactions';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
        	user_creator : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.user_creator_id`,
        			to : `${Users.tableName}.id`
        		}
        	},
        	accounting_code : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : AccountingCodes,
        		join : {
        			from : `${this.tableName}.accounting_code_id`,
        			to : `${AccountingCodes.tableName}.id`
        		}
        	},
        	transaction_type : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : TransactionTypes,
        		join : {
        			from : `${this.tableName}.transaction_type_id`,
        			to : `${TransactionTypes.tableName}.id`
        		}
        	},
        }
    }
}