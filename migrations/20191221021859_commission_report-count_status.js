
exports.up = function(knex, Promise) {
    return knex.schema.table("commission_report", table => {
        table.boolean('distribution_status').defaultTo(false);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("commission_report", table => {
        table.dropColumn('distribution_status');
    })
};
