import * as path from 'path';

export const appConfig = {
    upload_dir : path.join(__dirname, "/../uploads"),
    port : process.env.PORT || '2121',
    secret : '6e8a2402-9887-4964-9653-638e527e20c2',
    captchaSecret : '6LcXNIUUAAAAAPYSX4J7lkVIwiPGB-NGslT3cd_c',
    appUrl : process.env.appUrl || 'http://localhost:6768/',
    ignored_column : ['search','search_by','show_all','grouped','page','per_page','limit','order_by','read_at','start_date','end_date','start_key','end_key'],
    email : {
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // use SSL
        name : "RumahDPS",
        auth: {
            user: 'developer.smp.mgo@gmail.com',
            pass: 'SMPProject@123'
        }
    },
};