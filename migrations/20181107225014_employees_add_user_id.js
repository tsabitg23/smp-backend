
exports.up = function(knex, Promise) {
    return knex.schema.table("employees", table => {
        table.uuid('user_id').references('users.id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("employees", table => {
        table.dropColumn('user_id');
    })
};
