import BaseModel from "./base_model";
import {Model} from "objection";
import {Notifications} from './notifications';
import {Users} from './users';

export class NotificationUsers extends BaseModel {
	notification_id : string;
	to_user_id : string;
	read_at : string;

	notification ?: Notifications;
	to_user ?: Users;

    static get tableName() {
        return 'notification_users';
    }

    static get relationMappings() {
        return {
        	notification : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Notifications,
        		join : {
        			from : `${this.tableName}.notification_id`,
        			to : `${Notifications.tableName}.id`
        		}
        	},
        	to_user : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.user_id`,
        			to : `${Users.tableName}.id`
        		}
        	}
        }
    }
}