import * as admin from "firebase-admin";

const serviceAccount = require("../../config/firebase-key.json");

admin.initializeApp({
    projectId: 'smp-project-1579f',
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://smp-project-1579f.firebaseio.com"
});

export function firebaseAdmin() {
  return admin;
}