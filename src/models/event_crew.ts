import BaseModel from "./base_model";

export class EventCrew extends BaseModel {
	name : string;
	event_id : string;
	job : string;
	desc : string;
	
    static get tableName() {
        return 'event_crew';
    }

}