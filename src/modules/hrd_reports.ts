import {generateData} from "../helpers/paging";
import {CustomerAnalyst} from "../models/customer_analyst";
import {transaction} from "objection";
import {CustomerAnalystAccount} from "../models/customer_analyst_account";
import * as uuid from 'uuid';
import {omit} from 'lodash';
import {HrdReports} from "../models/hrd_reports";
export class HrdReportModule {
    static async get(ctx) {
        const {body, query = {}} = ctx.request;

        ctx.body = await generateData(
            HrdReports.query().eager('[employee]').whereNull('deleted_at'),
            ctx,
            {project_id: ctx.state.user.selected_project}
        );
    }

    static async getDetail(ctx) {

        ctx.body = await HrdReports.query().eager('[employee]').findById(ctx.params.id);
    }

    static async post(ctx) {
        const data = omit(ctx.request.body, ['accounts']);
        const id = uuid.v4();
        await transaction(HrdReports.knex(), async trx => {
            await HrdReports.query(trx).insert({
                ...data,
                project_id : ctx.state.user.selected_project,
                id
            });
            return id;
        }).catch(err=>{
            throw err;
        })

        ctx.body = {
            message:'success'
        }
    }

    static async put(ctx) {
        const {body} = ctx.request;
        await transaction(HrdReports.knex(),async (trx)=>{
            const data = omit(body, ['accounts']);
            await HrdReports.query(trx)
                .update(data).where('id', ctx.params.id);

            ctx.body = {
                message : 'Success'
            }
        })
    }

    static async del(ctx) {
        ctx.body = await HrdReports.query().where('id', ctx.params.id).update({
            deleted_at: new Date().toISOString()
        })
    }

    static async approvalChange(ctx){
        const {body} = ctx.request;
        ctx.body = await HrdReports.query().update({
            manager_approval_status: body.status,
            manager_desc: body.desc
        }).where('id', ctx.params.id)
    }

    static async getChart(ctx){
		const {query: {agency_id, agent_id, start_date, end_date, numberOfMonth=4}} = ctx.request;
		const countMonth = +numberOfMonth;
		let bookingQuery = `select sum(a.total_customer_booking) as total_booking from hrd_reports a `+
		`where a.project_id ='${ctx.state.user.selected_project}' `+
		`and EXTRACT('week' FROM a.created_at) = dataTanggal.weekval `+
		`and EXTRACT(YEAR FROM a.created_at) = dataTanggal.yearVal `+
		`and a.deleted_at is null`;

        let interviewQuery = `select sum(a.total_customer_interviewed) as total_interview from hrd_reports a `+
		`where a.project_id ='${ctx.state.user.selected_project}' `+
		`and EXTRACT('week' FROM a.created_at) = dataTanggal.weekval `+
		`and EXTRACT(YEAR FROM a.created_at) = dataTanggal.yearVal `+
        `and a.deleted_at is null`;

        let akadQuery = `select sum(a.total_customer_akad) as total_akad from hrd_reports a `+
		`where a.project_id ='${ctx.state.user.selected_project}' `+
		`and EXTRACT('week' FROM a.created_at) = dataTanggal.weekval `+
		`and EXTRACT(YEAR FROM a.created_at) = dataTanggal.yearVal `+
		`and a.deleted_at is null`;

        const chartOne = await HrdReports.knex().raw(`
        select dataTanggal.*,`+
				`(${bookingQuery}), `+
				`(${interviewQuery}), `+
				`(${akadQuery}) `+
			`from
				(select extract('week' from current_date + interval '1 week' - interval '1 week' * a ) as weekval, EXTRACT(YEAR from current_date + interval '1 week' - interval '1 week' * a ) as yearVal from generate_series(1,${countMonth},1) AS s(a)) as dataTanggal`);

		let formattedData = chartOne.rows.map(prop=>{
            Object.keys(prop).forEach(key=>{
                prop[key] = prop[key] ? prop[key] : 0
            })
			return {
                name : "Week "+prop.weekval+", "+prop.yearval,
                ...omit(prop,['weekval','yearval'])
            };
		});
		ctx.body = formattedData.reverse();
	}
}
