exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('unit_akad_verification', (table) => {
            table.uuid('id').primary();
            table.uuid('unit_akad_id').references('unit_akad.id').onDelete('restrict');
            table.string('name');
            table.string('gender');
            table.string('city');
            table.string('occupation');
            table.string('ktp_number');
            table.text('test_result');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('unit_akad_verification');
};