exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('commission', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('agent_id').references('agents.id').onDelete('restrict');
            table.string('unit_commission_approved');
            table.string('unit_commission_pending');
            table.decimal('total_commission_approved',14,2);
            table.decimal('total_commission_pending',14,2);
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('commission');
};