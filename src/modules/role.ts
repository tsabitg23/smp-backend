import {Roles} from '../models/roles';
import {RolePermissions} from '../models/role_permissions';
import {generateData} from '../helpers/paging';

export class Role {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				Roles.query().eager('[permissions.permission]').whereNull('deleted_at'),
				ctx,
			);
	}

	static async getDetail(ctx){

		ctx.body = await Roles.query().findById(ctx.params.id);	
	}

	static async post(ctx){
		ctx.body = await Roles.query().insert(ctx.request.body);
	}

	static async postPermission(ctx){
		let isExist = await RolePermissions.query().findOne({
			role_id : ctx.params.id,
			permission_id : ctx.request.body.permission_id
		});

		if(isExist){
			throw new Error('Permission already added');
		}
		
		ctx.body = await RolePermissions.query().insert(Object.assign({
			role_id : ctx.params.id
		},ctx.request.body));
	}

	static async delPermission(ctx){
		ctx.body = await RolePermissions.query().del().where('id',ctx.params.permission_id);
	}

	static async put(ctx){
		ctx.body = await Roles.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await Roles.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}