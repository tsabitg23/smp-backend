import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {Users} from './users';
import {TransactionTypes} from './transaction_types';

export class Cashflows extends BaseModel {
	project_id : string;
	transaction_type_id : string;
	user_creator_id : string;
	type : string;
	month : string;
	amount : number;
    
	project ?: Projects;
	transaction_type ?: TransactionTypes;
	user ?: Users;

    static get tableName() {
        return 'cashflows';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
        	user : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.user_creator_id`,
        			to : `${Users.tableName}.id`
        		}
        	},
        	transaction_type : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : TransactionTypes,
        		join : {
        			from : `${this.tableName}.transaction_type_id`,
        			to : `${TransactionTypes.tableName}.id`
        		}
        	}
        }
    }
}