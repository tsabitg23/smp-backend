exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('verification_questions', (table) => {
            table.uuid('id').primary();
            table.text('name');
            table.string('category');
            table.string('type');
            table.boolean('is_favorable');
            table.integer('order');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('verification_questions');
};