import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {Employees} from './employees';

export class ProjectTimelines extends BaseModel {
	project_id : string;
	employee_id : string;
	name : string;
	date : string;
    status : string;
    end_date : string;

	project ?: Projects;
	employee ?: Employees;

    static get tableName() {
        return 'project_timelines';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
        	employee : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Employees,
        		join : {
        			from : `${this.tableName}.employee_id`,
        			to : `${Employees.tableName}.id`
        		}
        	}
        }
    }
}