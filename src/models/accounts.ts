import BaseModel from "./base_model";
import {Model} from "objection";
import {Agencies} from "./agencies";

export class Accounts extends BaseModel {
    parent_id : string;

    static get tableName() {
        return 'accounts';
    }

    static get relationMappings() {
        return {
            agency : {
                relation : Model.BelongsToOneRelation,
                modelClass : Agencies,
                join : {
                    from : `${this.tableName}.agency_id`,
                    to : `${Agencies.tableName}.id`
                }
            }
        }
    }


}
