import {MarketingPlans} from '../models/marketing_plans';
import {generateData} from '../helpers/paging';

export class MarketingPlan{
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				MarketingPlans.query().eager('[employee]').whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){
		ctx.body = await MarketingPlans.query().eager('[employee]').findById(ctx.params.id);	
	}

	static async post(ctx){
		ctx.body = await MarketingPlans.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
	}

	static async put(ctx){
		ctx.body = await MarketingPlans.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await MarketingPlans.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}