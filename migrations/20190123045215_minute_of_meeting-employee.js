
exports.up = function(knex, Promise) {
    return knex.schema.table("minutes_of_meetings", table => {
        table.uuid('employee_id').references('employees.id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("minutes_of_meetings", table => {
        table.dropColumn('employee_id');
    })
};
