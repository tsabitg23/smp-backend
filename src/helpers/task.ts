import {Notifications} from '../models/notifications';
import {NotificationUsers} from '../models/notification_users';
import {Tasks} from '../models/tasks';
import {TaskUsers} from '../models/task_users';
import {transaction} from 'objection';
import {UserFirebaseTokens} from '../models/user_firebase_tokens'
import * as uuid from 'uuid';
import {firebaseAdmin} from "./firebase";
import {NotifHelper} from './notification';

export class TaskHelper {
	static async createTask(type,project_id,data,creator_id,users_ids){
		return transaction(Tasks.knex(),async trx=>{
			let taskId = uuid.v4();
			let notifId = uuid.v4();
			let notifData = NotifHelper.createData(type);

			await Tasks.query(trx).insert({
				id : taskId,
				creator_user_id : creator_id,
				status : 'created',
				type : type
			});

			await Notifications.query(trx).insert({
				id: notifId,
				title : notifData.title,
				description : notifData.desc,
				type : type,
				data : JSON.stringify(data),
				project_id : project_id
			})

			// let taskUserData = users_ids.map(userId=>{
			// 	return {
			// 		user_id : userId,
			// 		task_id  : taskId,
			// 		status : 'created'
			// 	}
			// })
			// if(taskUserData.length > 0){
			// 	await TaskUsers.query(trx).insert(taskUserData);
			// }

			let notifUserData = users_ids.map(userId=>{
				return {
					to_user_id : userId,
					notification_id  : notifId,
					read_at : null
				}
			});


			if(notifUserData.length >0){
				await NotificationUsers.query(trx).insert(notifUserData);
				
				//send notif
				NotifHelper.send(type,JSON.stringify(data),users_ids,notifId,project_id).then(()=>{
					console.log('complete send notif')
				});
			}



			return {
				task_id : taskId,
				notification_id : notifId
			}
		})
	}

	static async update(task_id,status,user_ids=[],type="",data={},selected_project){
		return transaction(Tasks.knex(),async trx=>{
			if(user_ids && type){
				let notif_type = type+`_${status}`
				let notifData = NotifHelper.createData(notif_type);
				let notifId = uuid.v4();

				let notifUserData = user_ids.map(userId=>{
					return {
						to_user_id : userId,
						notification_id  : notifId,
						read_at : null
					}
				});

				Notifications.query(trx).insert({
					id : notifId,
					title : notifData.title,
					description : notifData.desc,
					type : notif_type,
					data : JSON.stringify(data),
					project_id : selected_project
				}).then(res=>{
					// console.log('complete 1')
				})

				NotificationUsers.query(trx).insert(notifUserData).then(res=>{
					// console.log('complete 1')
				});

				NotifHelper.send(notif_type,JSON.stringify(data),user_ids,notifId,selected_project).then(()=>{
					// console.log('complete send notif')
				});


			}

			await Tasks.query(trx).update({status}).where('id',task_id);
		})
	}
}