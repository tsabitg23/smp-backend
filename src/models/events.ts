import BaseModel from "./base_model";
import {Model} from "objection";
import { EventFunds } from "./event_funds";
import { EventCrew } from "./event_crew";

export class Events extends BaseModel {
	name : string;
	date : string;
	place : string;
	status : string;
	desc : string;
	total : number;
	target : string;
	achievement : string;
	evaluation : string;
	project_id : string;
	
    static get tableName() {
        return 'events';
	}
	
	static get relationMappings() {
        return {
        	funds : {
        		relation : Model.HasManyRelation,
        		modelClass : EventFunds,
        		join : {
        			from : `${this.tableName}.id`,
        			to : `${EventFunds.tableName}.event_id`
        		}
        	},
        	crew : {
        		relation : Model.HasManyRelation,
        		modelClass : EventCrew,
        		join : {
        			from : `${this.tableName}.id`,
        			to : `${EventCrew.tableName}.event_id`
        		}
        	}
        }
    }

}