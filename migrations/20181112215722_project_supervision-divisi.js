
exports.up = function(knex, Promise) {
    return knex.schema.table("project_supervision", table => {
    	table.dropForeign('role_id');
        table.dropColumn('role_id');
        table.string('division');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("project_supervision", table => {
        table.uuid('role_id').references('roles.id').onDelete('restrict');
        table.dropColumn('division');
    })
};
