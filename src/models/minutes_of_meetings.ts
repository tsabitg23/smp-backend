import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {Users} from './users';
import {Employees} from './employees';

export class MinutesOfMeetings extends BaseModel {
	project_id : string;
	user_id : string;
	meeting_date : string;
	description : string;
	place : string;
	time : string;
	participant : string;
	problems : string;
	results : string;
	status : boolean;
	desc : string;

	project ?: Projects;
	employee ?: Employees;

    static get tableName() {
        return 'minutes_of_meetings';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
            employee : {
                relation : Model.BelongsToOneRelation,
                modelClass : Employees,
                join : {
                    from : `${this.tableName}.employee_id`,
                    to : `${Employees.tableName}.id`
                }
            },
        	user : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.user_id`,
        			to : `${Users.tableName}.id`
        		}
        	}
        }
    }
}