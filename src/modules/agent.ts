import {Agents} from '../models/agents';
import {generateData} from '../helpers/paging';
import * as moment from 'moment';
import { UnitAkad } from '../models/unit_akad';
import { Agencies } from '../models/agencies';
import {snakeCase} from 'lodash';
import { getChartOne } from '../services/agent_chart';
const excelToJson = require('convert-excel-to-json');

export class Agent {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(Agents.query().eager('[agency]')
			// .leftJoinRelation('agency')
			// .where('agency.deleted_at',null)
			// .orWhere('agency.id',null)
			.andWhere('agents.project_id',ctx.state.user.selected_project)
			.whereNull('agents.deleted_at')
			,ctx);
	}

	static async getDetail(ctx){

		ctx.body = await Agents.query().eager('[agency]').findById(ctx.params.id);	
	}

	static async post(ctx){
		let isUsed = await Agents.query().findOne({email : ctx.request.body.email});
		if(isUsed){
			ctx.status = 400;
			ctx.body = {
				message : "Email is already used"
			}
			return;
		}
		ctx.body = await Agents.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
	}

	static async put(ctx){
		ctx.body = await Agents.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await Agents.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}


	static async getChart(ctx){
		const {query: {agency_id, agent_id, start_date, end_date, numberOfMonth}} = ctx.request;
		const countMount = +numberOfMonth || 4;

		const chart_one = await getChartOne(ctx.state.user.selected_project, countMount, agency_id, agent_id);

		ctx.body = {
			chart_one
		};
	}

	static async getChartTwo(ctx){
		const {query: {agency_id, agent_id, start_date, end_date, numberOfMonth}} = ctx.request;
		const countMount = +numberOfMonth || 4;
		const agency = await Agencies.query().where('project_id', ctx.state.user.selected_project).whereNull('deleted_at');
		const chart_two = await Promise.all(agency.map(async prop=>{
			const data = await getChartOne(ctx.state.user.selected_project, countMount, prop.id, '');
			return data.map(it=>{
				return {
					name : it.name,
					[`${snakeCase(prop.name)}-akad`] : it.akad,
					[`${snakeCase(prop.name)}-booking`] : it.booking,
				}
			});

		}));

		const formattedData = [];

		chart_two.map(agency => {
			agency.map((prop, index)=>{
				if(!formattedData[index]){
					formattedData.push(prop)
				} 
				else{
					Object.keys(prop).filter(key=> key !== "name").map(bar => {
						formattedData[index][bar] = prop[bar]
					})
				}
			})
		})

		ctx.body = {
			chart_two : formattedData
		};
	}

	static async importData(ctx){
		const {request: {body, files},state : {user}} = ctx;
		try{
			const data = excelToJson({
				sourceFile: files.file.path
			});

			const agencyData = await Agencies.query().where('project_id', user.selected_project); 
			// console.log(data,'~~~~~~~~')
			const formattedData = [];
			Object.keys(data).map(prop=>{
				data[prop].slice(1).map(it=>{
					if(Object.keys(it).length >1 ){
						formattedData.push({
							project_id : user.selected_project,
							name : it.A,
							phone : it.F,
							address : it.B,
							email : it.G,
							join_date : it.E,
							agency_name : it.C,
							unit_sold : it.D,
						})
					}
				})
			});
			if(formattedData.length === 0){
				throw new Error('data empty');
			}

			formattedData.map(prop=>{
				const agency = agencyData.find(it=>it.name.toLowerCase() == prop.agency_name);
				if(agency){
					prop.agency_id = agency.name;
				} else {
					prop.agency_id = null;
				}

				delete prop.agency_name;
				return prop;
			})

			await Agents.query().insert(formattedData);
			ctx.body = {
				message : 'success'
			}
		}
		catch(er){
			throw er
		}
		
	}
}