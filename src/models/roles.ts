import BaseModel from "./base_model";
import {Model} from "objection";
import {RolePermissions} from './role_permissions'
export class Roles extends BaseModel {
	name : string;
	key : string;
	parent_id : string;

	permissions ?:RolePermissions;

    static get tableName() {
        return 'roles';
    }

    static get relationMappings() {
        return {
        	permissions : {
        		relation : Model.HasManyRelation,
        		modelClass : RolePermissions,
        		join : {
        			from : `${this.tableName}.id`,
        			to : `${RolePermissions.tableName}.role_id`
        		}
        	}
        }
    }
}