import {AssessmentAspects} from '../models/assessment_aspects';
import {generateData} from '../helpers/paging';
import {sortBy,omit} from 'lodash'
import {appConfig} from '../../config/app';

export class AssessmentAspect {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		let data = await AssessmentAspects.query().whereNull('deleted_at').where(omit(query,appConfig.ignored_column));
	
		let groupedData = [];
		data.filter(it=>it.is_header).map(it=>{
			it.children = [];
			groupedData.push(it)
			return it;
		})
		data.filter(it=>!it.is_header && it.parent_id).map(it=>{
			let index = groupedData.findIndex(group=> group.id === it.parent_id);
			if(index > -1){ 
				groupedData[index].children.push(it)
			}
		});

		ctx.body = sortBy(groupedData,['order']);
	}

	static async getDetail(ctx){

		ctx.body = await AssessmentAspects.query().findById(ctx.params.id);	
	}

	static async post(ctx){
		ctx.body = await AssessmentAspects.query().insert(ctx.request.body);
	}

	static async put(ctx){
		ctx.body = await AssessmentAspects.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await AssessmentAspects.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}