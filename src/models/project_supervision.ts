import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {Roles} from './roles';

export class ProjectSupervision extends BaseModel {
	project_id : string;
	role_id : string;
	name : string;
	result : string;
	description : string;
    
    project ?: Projects;
    role ?: Roles;

    static get tableName() {
        return 'project_supervision';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
        	role : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Roles,
        		join : {
        			from : `${this.tableName}.role_id`,
        			to : `${Roles.tableName}.id`
        		}
        	}
        }
    }
}