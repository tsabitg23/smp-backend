exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('task_users', (table) => {
            table.uuid('id').primary();
            table.uuid('user_id').references('users.id').onDelete('restrict');
            table.uuid('task_id').references('tasks.id').onDelete('restrict');
            table.string('status');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('task_users');
};