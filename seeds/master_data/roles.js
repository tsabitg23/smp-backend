const constant = require('../../config/const').constant;

module.exports = [
	{
		id : constant.ROLES.HRD,
		name : 'HRD',
		key : 'hrd',
		parent_id : null
	},
	{
		id : constant.ROLES.MR,
		name : 'Management Representative',
		key : 'mr',
		parent_id : constant.ROLES.OWNER
	},
	{
		id : constant.ROLES.SUPERVISOR,
		name : 'Pengawas Lapangan',
		key : 'supervisor',
		parent_id : constant.ROLES.MR
	},
	{
		id : constant.ROLES.ACCOUNTING,
		name : 'Accounting',
		key : 'accounting',
		parent_id : null
	},
	{
		id : constant.ROLES.MARKETING,
		name : 'Marketing',
		key : 'marketing',
		parent_id : null
	},
	{
		id : constant.ROLES.ADMIN,
		name : 'Administrator',
		key : 'admin',
		parent_id : null,
		deleted_at : new Date().toISOString()
	},
	{
		id : constant.ROLES.SUPERADMIN,
		name : 'Super Admin',
		key : 'superadmin',
		parent_id : null,
		deleted_at : new Date().toISOString()
	},
	{
		id : constant.ROLES.OWNER,
		name : 'Owner',
		key : 'owner',
		parent_id :null,
		deleted_at : new Date().toISOString()
	}
];