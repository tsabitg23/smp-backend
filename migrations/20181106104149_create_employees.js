exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('employees', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('role_id').references('roles.id').onDelete('restrict');
            table.string('name');
            table.string('nik');
            table.datetime('join_date');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('employees');
};