import {generateData} from '../helpers/paging';
import {AccountingCodes} from '../models/accounting_codes';

export class AccountingCode {
    public static async get(ctx){
        ctx.body = await generateData(
                AccountingCodes.query().whereNull('deleted_at'),
                ctx,
                {project_id : ctx.state.user.selected_project}
            );
    }

    public static async getDetail(ctx){

        ctx.body = await AccountingCodes.query().findById(ctx.params.id);
    }

    public static async post(ctx){
        ctx.body = await AccountingCodes.query()
            .insert(Object.assign({project_id : ctx.state.user.selected_project}, ctx.request.body));
    }

    public static async put(ctx){
        ctx.body = await AccountingCodes.query().where('id', ctx.params.id).update(ctx.request.body)
    }

    public static async del(ctx){
        ctx.body = await AccountingCodes.query().where('id', ctx.params.id).update({
            deleted_at : new Date().toISOString()
        });
    }
}