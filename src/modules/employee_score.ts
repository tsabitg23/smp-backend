import {EmployeeScores} from '../models/employee_scores';
import {AssessmentAspects} from '../models/assessment_aspects';
import {generateData, filterRange} from '../helpers/paging';
import {sortBy,omit, flatten} from 'lodash'
import * as path from 'path';
var xl = require('excel4node');
import {snakeCase} from 'lodash';
import * as send from 'koa-send';
import * as moment from 'moment';
import { Projects } from '../models/projects';

export class EmployeeScore{
	static async get(ctx){
		const {body,query={}} = ctx.request;
		ctx.body = await generateData(
				EmployeeScores.query().eager('[employee]').whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){
		const data:any = await EmployeeScores.query().eager('[employee]').findById(ctx.params.id);
		const questionList:any = await AssessmentAspects.query().whereNull('deleted_at');

		let questionGrouped = [];
		questionList.filter(it=>it.is_header).map(it=>{
			questionGrouped.push({
				...it,
				children : [],
				score : '',
				number : it.order
			})
			return it;
		})
		questionList.filter(it=>!it.is_header && it.parent_id).map(it=>{
			let index = questionGrouped.findIndex(group=> group.id === it.parent_id);
			if(index > -1){
				if(questionGrouped[index].children.length === 0){
					questionGrouped[index].children.push({
						...omit(questionGrouped[index],['children'])
					})
				}
				questionGrouped[index].children.push({
					...it,
					score: data.score_list[it.id],
					number: questionGrouped[index].number+'.'+it.order
				})
				// questionGrouped.splice((index + (it.order -1)), 0, {
				// 	...it,
				// 	score: data.score_list[it.id],

				// })
			}
		});
		questionGrouped = sortBy(questionGrouped, ['order'])
		data.question_list = flatten(questionGrouped.map(it=>it.children));
		ctx.body = data;
	}

	static async post(ctx){
		ctx.body = await EmployeeScores.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
	}

	static async put(ctx){
		ctx.body = await EmployeeScores.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await EmployeeScores.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}

	static async exportData(ctx){
		const q = EmployeeScores.query().eager('[employee]').where('project_id', ctx.state.user.selected_project).whereNull('deleted_at');
		const data = await filterRange(q, ctx.request);
		var wb = new xl.Workbook();
		var ws = wb.addWorksheet('Sheet 1');
		var style = wb.createStyle({
			font: {
			  color: '#000000',
			  size: 11,
			},
			numberFormat: '$#,##0.00; ($#,##0.00); -',
		  });
		const header = ['No', 'Nama Pegawai','periode mulai penilaian', 'periode selesai','total skor','catatan karyawan','catatan hrd','catatan atasan','tanggal data dibuat']
		header.map((prop, index)=>{
			ws.cell(1, (index+1))
			.string(header[index])
			.style(style);
		});

		data.map((prop,index)=>{
			const start_periode = prop.period_start ? moment(prop.period_start).format('YYYY-MM-DD HH:mm:ss') : '-';
			const end_periode = prop.period_end ? moment(prop.period_end).format('YYYY-MM-DD HH:mm:ss') : '-';

			ws.cell(index+2, 1)
			.string(`${index+1}`)
			.style(style)

			ws.cell(index+2, 2)
			.string(prop.employee.name)
			.style(style)

			ws.cell(index+2, 3)
			.string(start_periode)
			.style(style)

			ws.cell(index+2, 4)
			.string(end_periode)
			.style(style)

			ws.cell(index+2, 5)
			.string(`${prop.total_score}`)
			.style(style)

			ws.cell(index+2, 6)
			.string(prop.employee_note)
			.style(style)

			ws.cell(index+2, 7)
			.string(prop.hrd_note)
			.style(style)

			ws.cell(index+2, 8)
			.string(prop.boss_note)
			.style(style)

			ws.cell(index+2, 9)
			.string(moment(prop.created_at).format('YYYY-MM-DD HH:mm:ss'))
			.style(style)
		})

		const projectData = await Projects.query().findOne('id', ctx.state.user.selected_project);
		const filename = snakeCase(projectData.name)+'_penilaian_karyawan_'+moment().format('YYYY-MM-DD')+".xlsx";
		const fileDir = path.join('assets',filename)
		await new Promise((resolve, reject)=>{
			wb.write(fileDir, (err, stat)=>{
				if(err){
					reject()
				} else {
					resolve(stat)
				}
			});
		}).catch(err=>{
			throw err
		})

		ctx.body = {
			file : filename
		}

	}
}
