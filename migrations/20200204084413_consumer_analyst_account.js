exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('customer_analyst_account', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('customer_analyst_id').references('customer_analyst.id').onDelete('restrict');
            table.string('bank_name');
            table.string('month');
            table.decimal('start_balance',14,2);
            table.decimal('credit',14,2);
            table.decimal('debit',14,2);
            table.decimal('end_balance',14,2);
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('customer_analyst_account');
};
