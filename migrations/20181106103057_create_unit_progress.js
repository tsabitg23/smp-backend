exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('unit_progress', (table) => {
            table.uuid('id').primary();
            table.uuid('unit_id').references('units.id');
            table.decimal('percentage',3,2);
            table.text('description');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('unit_progress');
};