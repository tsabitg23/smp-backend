
exports.up = function(knex, Promise) {
    return knex.schema.table("units", table => {
        table.uuid('akad_by_agent_id').references('agents.id').onDelete('restrict');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("units", table => {
        table.dropColumn('akad_by_agent_id');
    })
};
