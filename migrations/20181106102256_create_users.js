exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('users', (table) => {
            table.uuid('id').primary();
            table.uuid('entity_id').references('entities.id').onDelete('restrict');
            table.uuid('role_id').references('roles.id').onDelete('restrict');
            table.string('email').unique();
            table.string('nik');
            table.string('password');
            table.string('salt');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('users');
};