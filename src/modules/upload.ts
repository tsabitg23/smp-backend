import * as csv from 'csvtojson';
import * as fs from 'fs-extra-promise';
import {get,intersection} from 'lodash';
import {Units} from '../models/units';
import * as path from 'path';
import * as mime from 'mime';
import * as send from 'koa-send';

export class Upload{
	static async upload(ctx){
		const {request: {body, files},state : {user}} = ctx;

        let csvData = await csv().fromFile(files.file.path);

        let formatedData = csvData.map(it=>{
        	let neededKey =['no_kavling','luas_tanah_m2','luas_bangunan_m2','sudah_terjual','sudah_booking']
        	if(intersection(neededKey,Object.keys(it)).length !== 5){
	            throw new Error('Format error, please use the correct template')
        	}
        	let status = 'available';
        	let isBooked = get(it,'sudah_booking',false);
        	let isSold = get(it,'sudah_terjual',false);

        	if(isSold){
        		status = 'sold';
        	}
        	else if(isBooked){
        		status = 'booking_fee_paid'
        	}

        	return {
        		kavling_number : get(it,'no_kavling','-'),
        		land_area : get(it,'luas_tanah_m2','-'),
        		building_area : get(it,'luas_bangunan_m2','-'),
        		status : status,
        		project_id : user.selected_project
        	}
        })
        await Units.query().insert(formatedData);
		ctx.body = {
			message : 'success'
		}
	}


        static async download(ctx){
				let file = path.join('assets',ctx.params.filename);
				// let file = path.join('assets',ctx.request.body.filename);
                // const data = await fs.readFileSync(file);
				// ctx.body = data.toString('base64');
				
                await send(ctx,file)
        }
}