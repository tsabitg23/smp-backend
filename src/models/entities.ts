import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';

export class Entities extends BaseModel {
	name : string;
	phone : string;
	address : string;
	
	projects ?: Projects[]	
    static get tableName() {
        return 'entities';
    }

    static get relationMappings() {
        return {
        	projects : {
        		relation : Model.HasManyRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.id`,
        			to : `${Projects.tableName}.entity_id`
        		}
        	}
        }
    }
}