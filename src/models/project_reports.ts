import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {Users} from './users';
import {Tasks} from './tasks';

export class ProjectReports extends BaseModel {
	project_id : string;
	date : string;
	user_id : string;
	task_id : string;
	name : string;
	type : string;
	recommendation : string;
	alternate_recommendation : string;

	project ?: Projects;
	user ?: Users;
	task ?: Tasks;

    static get tableName() {
        return 'project_reports';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
        	user : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.user_id`,
        			to : `${Users.tableName}.id`
        		}
        	},
        	task : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Tasks,
        		join : {
        			from : `${this.tableName}.task_id`,
        			to : `${Tasks.tableName}.id`
        		}
        	}
        }
    }
}