import {generateData} from "../helpers/paging";
import {CustomerAnalyst} from "../models/customer_analyst";
import {transaction} from "objection";
import {CustomerAnalystAccount} from "../models/customer_analyst_account";
import * as uuid from 'uuid';
import {omit} from 'lodash';
export class CustomerAnalystModule {
    static async get(ctx) {
        const {body, query = {}} = ctx.request;

        ctx.body = await generateData(
            CustomerAnalyst.query().eager('[customer, unit]').whereNull('deleted_at'),
            ctx,
            {project_id: ctx.state.user.selected_project}
        );
    }

    static async getDetail(ctx) {

        ctx.body = await CustomerAnalyst.query().eager('[customer, unit, accounts]').findById(ctx.params.id);
    }

    static async post(ctx) {
        const data = omit(ctx.request.body, ['accounts']);
        const id = uuid.v4();
        await transaction(CustomerAnalyst.knex(), async trx => {
            await CustomerAnalyst.query(trx).insert({
                ...data,
                project_id : ctx.state.user.selected_project,
                id
            });
            await CustomerAnalystAccount.query(trx).insert(ctx.request.body.accounts.map(prop=>{
                return {
                    ...prop,
                    project_id : ctx.state.user.selected_project,
                    customer_analyst_id : id
                }
            }));

            return id;
        }).catch(err=>{
            throw err;
        })

        ctx.body = {
            message:'success'
        }
    }

    static async put(ctx) {
        const {body} = ctx.request;
        await transaction(CustomerAnalyst.knex(),async (trx)=>{
            const data = omit(body, ['accounts']);
            await CustomerAnalyst.query(trx)
                .update(data).where('id', ctx.params.id);
            if(body.accounts){
                await CustomerAnalystAccount.query(trx).del().where('customer_analyst_id', ctx.params.id);
                await CustomerAnalystAccount.query(trx)
                    .insert(body.accounts.map(prop=>{
                        return {
                            ...prop,
                            project_id : ctx.state.user.selected_project,
                            customer_analyst_id : ctx.params.id
                        }
                    }));
            }


            ctx.body = {
                message : 'Success'
            }
        })
    }

    static async del(ctx) {
        ctx.body = await CustomerAnalyst.query().where('id', ctx.params.id).update({
            deleted_at: new Date().toISOString()
        })
    }

    static async approvalChange(ctx){
        const {body} = ctx.request;
        ctx.body = await CustomerAnalyst.query().update({
            manager_approve_status: body.status,
            manager_desc: body.desc
        }).where('id', ctx.params.id)
    }
}
