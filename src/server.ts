require('dotenv').config();
import {appConfig} from "../config/app";
import * as Koa from "koa";
import * as cors from "@koa/cors";
import * as morgan from "koa-morgan";
import * as compress from "koa-compress";
import * as Router from "koa-router";
import * as koaBody from "koa-body";
import {routes} from "./route_list";

export default async function startServer(){

    const app = new Koa();
    const router = new Router();

    app.use(cors({
        exposeHeaders : 'Content-disposition'
    }));
    app.use(koaBody({
        formidable:{uploadDir: './uploads'},
        multipart:true
    }));

    app.use(morgan(':method :url :status [:date[iso]] - :response-time ms'));
    let options = {
        filter: function (content_type) { return /text/i.test(content_type) },
        threshold: 2048,
        flush: require('zlib').Z_SYNC_FLUSH
    };
    app.use(compress(options));
    app.use(async (ctx, next) => {
        const start = Date.now();
        await next();
        const ms = Date.now() - start;
        ctx.set('X-Response-Time', `${ms}ms`);
    });

    app.use(async (ctx, next) => {
        try {
            await next();
        } catch (err) {
            ctx.status = err.status || 500;
            ctx.body = err.message;
            ctx.app.emit('error', err, ctx);
        }
    });

    routes.map(it=>{
       let middleware = it.middleware || [];
       router[it.method](it.route,...middleware,it.execute);
    });

    app.use(router.routes());
    app.use(router.allowedMethods());
    console.log('Application running on PORT',appConfig.port);
    app.listen(appConfig.port);
}