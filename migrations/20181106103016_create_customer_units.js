exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('customer_units', (table) => {
            table.uuid('id').primary();
            table.uuid('unit_id').references('units.id').onDelete('restrict');
            table.uuid('customer_id').references('customers.id');
            table.uuid('payment_type_id').references('payment_types.id');
            table.decimal('downpayment',14,2);
            table.integer('installment_per_month');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('customer_units');
};