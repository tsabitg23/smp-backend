import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {Employees} from './employees';

export class EmployeeScores extends BaseModel {
	project_id : string;
	employee_id : string;
	period_start : string;
	period_end : string;
	total_score : string;
	score_list : string;
	employee_note : string;
	hrd_note : string;
	boss_note : string;

	project ?: Projects;
	employee ?: Employees;

    static get tableName() {
        return 'employee_scores';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
        	employee : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Employees,
        		join : {
        			from : `${this.tableName}.employee_id`,
        			to : `${Employees.tableName}.id`
        		}
        	}
        }
    }
}