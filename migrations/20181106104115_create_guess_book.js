exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('guest_book', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.string('name');
            table.string('phone');
            table.text('address');
            table.datetime('survei_date');
            table.text('info_source');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('guest_book');
};