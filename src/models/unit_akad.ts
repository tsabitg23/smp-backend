import BaseModel from "./base_model";
import {Model} from "objection";
import {Units} from './units';
import {Customers} from './customers';
import {Agents} from './agents';

export class UnitAkad extends BaseModel {
	unit_id ?: string;
	customer_id ?: string;
	agent_id ?: string;
	booking_date : string;
	verification_date : string;
	akad_date : string;
	status : string;
    customer : string;
	email_to : string;
	
	payment_method : string;
	downpayment : string;
	tenor : string;
	project_id?:string;

	unit ?: Units;
	customer_detail ?: Customers;
	agent ?: Agents;

    static get tableName() {
        return 'unit_akad';
    }

    static get relationMappings() {
        return {
        	agent : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Agents,
        		join : {
        			from : `${this.tableName}.agent_id`,
        			to : `${Agents.tableName}.id`
        		}
        	},
        	unit : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Units,
        		join : {
        			from : `${this.tableName}.unit_id`,
        			to : `${Units.tableName}.id`
        		}
        	},
        	customer_detail : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Customers,
        		join : {
        			from : `${this.tableName}.customer_id`,
        			to : `${Customers.tableName}.id`
        		}
        	}
        }
    }
}