import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';

export class GuestBook extends BaseModel {
	project_id : string;
	name : string;
	phone : string;
	address : string;
	survei_date : string;
	info_source : string;

	project ?: Projects;

    static get tableName() {
        return 'guest_book';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	}
        }
    }
}