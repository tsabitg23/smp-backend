
exports.up = function(knex, Promise) {
    return knex.schema.table("transaction_types", table => {
        table.string('type');
        table.boolean('is_header');
        table.uuid('parent_id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("transaction_types", table => {
        table.dropColumn('type');
        table.dropColumn('is_header');
        table.dropColumn('parent_id');
    })
};
