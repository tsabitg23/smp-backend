
exports.up = function(knex, Promise) {
    return knex.schema.table("units", table => {
        table.boolean('commission_status').defaultTo(false);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("units", table => {
        table.dropColumn('commission_status');
    })
};
