import BaseModel from "./base_model";
import {Model} from "objection";
import {Users} from './users';

export class UserProfile extends BaseModel {
	user_id : string;
	name : string;
	photo : string;
	phone : string;
	address : string;
	bio : string;

	user ?: Users;
    static get tableName() {
        return 'user_profile';
    }

    static get relationMappings() {
        return {
        	user : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.user_id`,
        			to : `${Users.tableName}.id`
        		}
        	}
        }
    }
}