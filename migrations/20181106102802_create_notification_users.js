exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('notification_users', (table) => {
            table.uuid('id').primary();
            table.uuid('notification_id').references('notifications.id').onDelete('restrict');
            table.uuid('to_user_id').references('users.id').onDelete('restrict');
            table.datetime('read_at');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('notification_users');
};