import {Projects} from '../models/projects';
import {Users} from '../models/users';
import {Agents} from '../models/agents';
import {Units} from '../models/units';
import {UserPermissions} from '../models/user_permissions';
import {MarketingPlans} from '../models/marketing_plans';
import {GuestBook} from '../models/guest_book';
import {Employees} from '../models/employees';
import {UnitAkadVerification} from '../models/unit_akad_verification';
import {UnitAkad} from '../models/unit_akad';
import * as moment from 'moment';
import { ProjectQualityControls } from '../models/project_quality_controls';
import { EmployeeScores } from '../models/employee_scores';
import { ProjectSupervision } from '../models/project_supervision';
import { ProjectReports } from '../models/project_reports';
import { ProjectJobs } from '../models/project_jobs';
import { ProjectPaymentRequests } from '../models/project_payment_requests';
import { MinutesOfMeetings } from '../models/minutes_of_meetings';
import { ProjectTimelines } from '../models/project_timelines';
import { UnitProgress } from '../models/unit_progress';
import { Events } from '../models/events';
import { Commission } from '../models/commission';
import {HrdReports} from "../models/hrd_reports";
import {CustomerAnalyst} from "../models/customer_analyst";
import {BankAccounts} from "../models/bank_accounts";
import {Accounts} from "../models/accounts";

export class Dashboard {
	static async get(ctx){
		let dashboard:any = [
			{
				permission_key : 'projects',
				icon : 'work',
				title : "Projects",
				subtitle : "Total projects",
				subtitle_icon : 'assessment',
				value : Projects.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'users',
				icon : 'people',
				title : "Users",
				subtitle : "Total users",
				subtitle_icon : 'assessment',
				value : Users.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'units',
				icon : 'home',
				title : "Units",
				subtitle : "Unit sales status",
				subtitle_icon : 'attach_money',
				value : Units.query().select(['status as name']).count('id as value').where({
					deleted_at : null,
					project_id : ctx.state.user.selected_project
				}).groupBy('status'),
				data : {},
				type : "pie"
			},
			{
				permission_key : 'agents',
				icon : 'person',
				title : "Agents",
				subtitle : "Total agents",
				subtitle_icon : 'assessment',
				value : Agents.query().count('id').where({
					deleted_at : null,
					project_id : ctx.state.user.selected_project
				}),
				data : {},
				type : "count"
			},
			{
				permission_key : 'events',
				icon : 'event',
				title : "Events",
				subtitle : "Total event",
				subtitle_icon : 'assessment',
				value : Events.query().count('id').where({
					deleted_at : null,
					project_id : ctx.state.user.selected_project
				}),
				data : {},
				type : "count"
			},
			{
				permission_key : 'komisi',
				icon : 'attach_money',
				title : "Komisi",
				subtitle : "Jumlah agent penerima",
				subtitle_icon : 'assessment',
				value : Commission.query().count('id').where({
					deleted_at : null,
					project_id : ctx.state.user.selected_project
				}),
				data : {},
				type : "count"
			},
			{
				permission_key : 'guest_book',
				icon : 'book',
				title : "Guest Book",
				subtitle : "Guest entry this month",
				subtitle_icon : 'calendar_today',
				value : GuestBook.query().count('id').whereRaw('extract(month from survei_date) = ?',[moment().format('MM')]).andWhere({
					deleted_at : null,
					project_id : ctx.state.user.selected_project
				}),
				data : {},
				type : "count"
			},
			{
				permission_key : 'marketing_plans',
				icon : 'description',
				title : "Marketing Plan",
				subtitle : "Total plan",
				subtitle_icon : 'assessment',
				value : MarketingPlans.query().count('id').where({
					deleted_at : null,
					project_id : ctx.state.user.selected_project
				}),
				data : {},
				type : "count"
			},
			{
				permission_key : 'employees',
				icon : 'people',
				title : "Employees",
				subtitle : "Total employees",
				subtitle_icon : 'assessment',
				value : Employees.query().count('id').where({
					deleted_at : null,
					project_id : ctx.state.user.selected_project
				}),
				data : {},
				type : "count"
			},
			{
				permission_key : 'customer_verification',
				icon : 'assignment_turned_in',
				title : "Verif Done",
				subtitle : "Verification done this month",
				subtitle_icon : 'assessment',
				value : UnitAkadVerification.query().joinRelation('unit_akad.[unit]').count('unit_akad_verification.id')
					.where('unit_akad_verification.deleted_at',null)
					.where('unit_akad:unit.project_id',ctx.state.user.selected_project)
					.whereRaw('extract(month from unit_akad_verification.created_at) = ?',[moment().format('MM')])
				,
				data : {},
				type : "count"
			},
			{
				permission_key : 'customer_verification',
				icon : 'assignment',
				title : "Verif Total",
				subtitle : "All user verification This month",
				subtitle_icon : 'assessment',
				value : UnitAkad.query().joinRelation('unit').count('unit_akad.id')
					.where('unit_akad.deleted_at',null)
					.where('unit.project_id',ctx.state.user.selected_project)
					.whereRaw('extract(month from unit_akad.created_at) = ?',[moment().format('MM')])
				,
				data : {},
				type : "count"
			},
			{
				permission_key : 'quality_control',
				icon : 'search',
				title : "Temuan",
				subtitle : "Total temuan",
				subtitle_icon : 'assessment',
				value : ProjectQualityControls.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'employee_score',
				icon : 'content_paste',
				title : "Penilaian",
				subtitle : "Total penilaian",
				subtitle_icon : 'assessment',
				value : EmployeeScores.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'project_supervision',
				icon : 'remove_red_eye',
				title : "Pengawasan",
				subtitle : "Total data pengawasan",
				subtitle_icon : 'assessment',
				value : ProjectSupervision.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'recommend_project_report',
				icon : 'feedback',
				title : "Rekomendasi",
				subtitle : "Total data rekomendasi",
				subtitle_icon : 'assessment',
				value : ProjectReports.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'approve_project_jobs',
				icon : 'note_add',
				title : "Kebutuhan Project",
				subtitle : "Total data kebutuhan project",
				subtitle_icon : 'assessment',
				value : ProjectJobs.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'approve_project_payment_request',
				icon : 'attach_money',
				title : "Pembiayaan",
				subtitle : "Total permintaan pembiayaan",
				subtitle_icon : 'assessment',
				value : ProjectPaymentRequests.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'minutes_of_meetings',
				icon : 'class',
				title : "Notulen",
				subtitle : "Total data notulen",
				subtitle_icon : 'assessment',
				value : MinutesOfMeetings.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'timeline_project',
				icon : 'timeline',
				title : "Timeline",
				subtitle : "Total data timeline project",
				subtitle_icon : 'assessment',
				value : ProjectTimelines.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'unit_progress',
				icon : 'trending_up',
				title : "Unit Progress",
				subtitle : "Total data timeline project",
				subtitle_icon : 'assessment',
				value : UnitProgress.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'project_plannings',
				icon : 'description',
				title : "Pengawasan",
				subtitle : "Total data pengawasan",
				subtitle_icon : 'assessment',
				value : ProjectSupervision.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'project_jobs',
				icon : 'note_add',
				title : "Kebutuhan Project",
				subtitle : "Total data kebutuhan project",
				subtitle_icon : 'assessment',
				value : ProjectJobs.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'project_payment_request',
				icon : 'attach_money',
				title : "Pembiayaan Project",
				subtitle : "Total data permintaan pembiayaan",
				subtitle_icon : 'assessment',
				value : ProjectPaymentRequests.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'project_quality_control',
				icon : 'check_box',
				title : "Quality Control",
				subtitle : "Total data quality control",
				subtitle_icon : 'assessment',
				value : ProjectQualityControls.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'project_report',
				icon : 'report',
				title : "Report",
				subtitle : "Total data laporan",
				subtitle_icon : 'assessment',
				value : ProjectReports.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'report_mingguan',
				icon : 'report',
				title : "Laporan HRD",
				subtitle : "Laporan Mingguan",
				subtitle_icon : 'assessment',
				value : HrdReports.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'customer_analyst',
				icon : 'class',
				title : "Analisa Customer",
				subtitle : "Laporan Analisa customer",
				subtitle_icon : 'assessment',
				value : CustomerAnalyst.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'rekening_bank',
				icon : 'note_add',
				title : "Rekening",
				subtitle : "Jumlah Rekening Bank",
				subtitle_icon : 'assessment',
				value : BankAccounts.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
			{
				permission_key : 'akun',
				icon : 'note_add',
				title : "Akun",
				subtitle : "Jumlah akun",
				subtitle_icon : 'assessment',
				value : Accounts.query().count('id').whereNull('deleted_at'),
				data : {},
				type : "count"
			},
		]

		let userPermissions = await UserPermissions.query().eager('permission').where({
			user_id : ctx.state.user.user_id,
			read : true,
			deleted_at : null
		});

		let data = dashboard.filter(prop=>userPermissions.findIndex(it=>it.permission.key === prop.permission_key) > -1);

		let newData = data.map(async (dash,index)=>{

			if(dash.value && dash.type === "count"){
				let value = await dash.value
				dash.value = value[0].count;
			}
			else if(dash.value && dash.type === 'pie'){
				let value = await dash.value
				dash.value = value;
			}
			return dash
		})

		await Promise.all(newData);

		ctx.body = data;
	}
}
