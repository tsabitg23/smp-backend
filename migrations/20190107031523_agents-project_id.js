
exports.up = function(knex, Promise) {
    return knex.schema.table("agents", table => {
        table.uuid('project_id').references('projects.id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("agents", table => {
        table.dropColumn('project_id');
    })
};
