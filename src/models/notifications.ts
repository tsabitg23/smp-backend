import BaseModel from "./base_model";
import {Model} from "objection";
import {Users} from './users';

export class Notifications extends BaseModel {
	creator_user_id : string;
	title : string;
	description : string;
	type : string;
	data : string;
	creator_user ?:Users;
    project_id : string;
	
    static get tableName() {
        return 'notifications';
    }

    static get relationMappings() {
        return {
        	creator_user : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.creator_user_id`,
        			to : `${Users.tableName}.id`
        		}
        	}
        }
    }
}