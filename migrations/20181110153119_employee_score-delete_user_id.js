
exports.up = function(knex, Promise) {
    return knex.schema.table("employee_scores", table => {
    	table.dropForeign('user_id');
        table.dropColumn('user_id');
        table.dropColumn('phone');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("employee_scores", table => {
        table.uuid('user_id').references('users.id').onDelete('restrict');
        table.string('phone');
    })
};
