import {ProjectPlannings} from '../models/project_plannings';
import {generateData} from '../helpers/paging';

export class ProjectPlanning {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				ProjectPlannings.query().whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){

		ctx.body = await ProjectPlannings.query().findById(ctx.params.id);
	}

	static async post(ctx){
		ctx.body = await ProjectPlannings.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project,user_id : ctx.state.user.user_id},ctx.request.body));
	}


	static async put(ctx){
		ctx.body = await ProjectPlannings.query().where('id',ctx.params.id).update(ctx.request.body)
	}


	static async del(ctx){
		ctx.body = await ProjectPlannings.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}