import {appConfig} from '../../config/app';
const fetch = require('node-fetch');
import * as qs from 'querystring';

const header = {
	"Content-Type": "application/json"
}

let secret = appConfig.captchaSecret;

export async function verifyCaptcha(token){
	let data = qs.stringify({
		secret : appConfig.captchaSecret,
		response : token
    })

	let response = await fetch('https://www.google.com/recaptcha/api/siteverify', {
        method: 'POST',
        body: data,
        headers: {
			"Content-Type": "application/x-www-form-urlencoded"
		}
    }).then(res => {
    	if (res.status >= 400) {
			throw new Error("Bad response from server");
		}
        return res.json()
    }).then(result=>{
    	console.log(result,'result captcha');
    	return result;
    }).catch(err=>{
    	console.log(err,'error captcha')
    });
    return response;
}
