import {MinutesOfMeetings} from '../models/minutes_of_meetings';
import {generateData} from '../helpers/paging';

export class MinuteOfMeeting {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				MinutesOfMeetings.query().eager('[employee]').whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){

		ctx.body = await MinutesOfMeetings.query().eager('[employee]').findById(ctx.params.id);
	}

	static async post(ctx){
		ctx.body = await MinutesOfMeetings.query()
			.insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
	}

	static async put(ctx){
		ctx.body = await MinutesOfMeetings.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await MinutesOfMeetings.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}