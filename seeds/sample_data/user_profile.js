const constant = require('../../config/const').constant;

module.exports = [
    {
        id : '69559685-44cb-4c9d-a2f1-b1622e4c70df',
        user_id : constant.USER.SUPERADMIN,
        name : "Superadmin SMP"
    },
    {
        id : '2ecf3e27-4a8a-46f2-b1b8-354cdb7aaa60',
        user_id : constant.USER.ADMIN,
        name : 'Administrator'
    }
];