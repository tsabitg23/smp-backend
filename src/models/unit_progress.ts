import BaseModel from "./base_model";
import {Model} from "objection";
import {Units} from './units';

export class UnitProgress extends BaseModel {
	unit_id : string;
	percentage : number;
	description : string;
    project_id : string;

	unit ? : Units;
    static get tableName() {
        return 'unit_progress';
    }

    static get relationMappings() {
        return {
        	unit : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Units,
        		join : {
        			from : `${this.tableName}.unit_id`,
        			to : `${Units.tableName}.id`
        		}
        	}
        }
    }
}