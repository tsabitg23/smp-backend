import {TransactionTypes} from '../models/transaction_types';
import {generateData} from '../helpers/paging';

export class TransactionType {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		// let data = await TransactionTypes.query().whereNull('deleted_at').where(omit(query,['grouped']));
		let data = await generateData(
				TransactionTypes.query().whereNull('deleted_at'),
				ctx
			);
		if(!query.grouped){
			ctx.body = data
		}else{
			let groupedData = [];
			data.data.filter(it=>it.is_header).map(it=>{
				it.children = [];
				groupedData.push(it)
				return it;
			})
			data.data.filter(it=>!it.is_header).map(it=>{
				if(it.parent_id){
					let index = groupedData.findIndex(group=> group.id === it.parent_id);
					if(index > -1){ 
						groupedData[index].children.push(it)
					}
				}else{
					groupedData.push(it)
				}
			});

			ctx.body = groupedData;
		}
	}

	static async getDetail(ctx){

		ctx.body = await TransactionTypes.query().findById(ctx.params.id);	
	}

	static async post(ctx){
		ctx.body = await TransactionTypes.query().insert(ctx.request.body);
	}

	static async put(ctx){
		ctx.body = await TransactionTypes.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		ctx.body = await TransactionTypes.query().where('id',ctx.params.id).update({
			deleted_at : new Date().toISOString()
		})
	}
}