
exports.up = function(knex, Promise) {
    return knex.schema.table("project_timelines", table => {
        table.dropColumn('activity_name');
        table.date('date');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("project_timelines", table => {
        table.string('activity_name');
        table.dropColumn('date');
    })
};
