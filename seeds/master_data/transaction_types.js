const constant = require('../../config/const').constant;

module.exports = [
    {
        id : constant.TRANSACTION_TYPES.INCOME,
        name : 'Pemasukan',
        type : 'transaction',
        parent_id : null,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.EXPENSE,
        name : 'Pengeluaran',
        type : 'transaction',
        parent_id : null,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.EQUITY,
        name : 'Modal Perusahaan',
        type : 'cash_in',
        parent_id : null,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.DOWNPAYMENT,
        name : 'Downpayment',
        type : 'cash_in',
        parent_id : null,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.INCOME_FROM_INSTALLMENT,
        name : 'Pendapatan dari Angsuran',
        type : 'cash_in',
        parent_id : null,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.LAND_ACQUISITION_COST,
        name : 'Biaya Perelohan Lahan',
        type : 'cash_out',
        parent_id : null,
        is_header : true
    },
    {
        id : constant.TRANSACTION_TYPES.LAND_COST,
        name : 'Pembelian Lahan',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.LAND_ACQUISITION_COST,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.LAND_COST_LEGAL,
        name : 'Legal Pembelian Tanah',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.LAND_ACQUISITION_COST,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.LAND_CERTIFICATION,
        name : 'Sertifikat Tanah',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.LAND_ACQUISITION_COST,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.LAND_TAX,
        name : 'Pajak Tanah',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.LAND_ACQUISITION_COST,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.LINENCE_FEE,
        name : 'Biaya Perizinan',
        type : 'cash_out',
        parent_id : null,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.LAND_PROJECT_FEE,
        name : 'Biaya Pematangan Lahan',
        type : 'cash_out',
        parent_id : null,
        is_header : true
    },
    {
        id : constant.TRANSACTION_TYPES.INFRASTRUCTURE,
        name : 'Infrastruktur',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.LAND_PROJECT_FEE,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.UTILITY,
        name : 'Utilitas',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.LAND_PROJECT_FEE,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.PUBLIC_FACILITY,
        name : 'Fasilitas Sosial / Umum',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.LAND_PROJECT_FEE,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.ENVIRONTMENT_MAINTENANCE,
        name : 'Pemeliharaan dan Pembinaan Lingkungan',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.LAND_PROJECT_FEE,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.OVERHEAD_FEE,
        name : 'Biaya Overheade',
        type : 'cash_out',
        parent_id : null,
        is_header : true
    },
    {
        id : constant.TRANSACTION_TYPES.OFFICE_PREPARATION,
        name : 'Persiapan Kantor dan Inventaris',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.OVERHEAD_FEE,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.OFFICE_OPERATIONAL,
        name : 'Operasional Kantor',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.OVERHEAD_FEE,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.SALARY,
        name : 'Gaji Karyawan',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.OVERHEAD_FEE,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.PROMOTION,
        name : 'Promosi',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.OVERHEAD_FEE,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.PROSPERITY,
        name : 'Kesejahteraan',
        type : 'cash_out',
        parent_id : constant.TRANSACTION_TYPES.OVERHEAD_FEE,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.CONSTRUCTION_FEE,
        name : 'Biaya Konstruksi Rumah',
        type : 'cash_out',
        parent_id : null,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.LOADING_UNIT_FEE,
        name : 'Biaya Pembebanan Unit / Rumah',
        type : 'cash_out',
        parent_id : null,
        is_header : false
    },
    {
        id : constant.TRANSACTION_TYPES.PPN_TAX,
        name : 'Biaya PPH & PPN',
        type : 'cash_out',
        parent_id : null,
        is_header : false
    }

];