module.exports = [
    {
        id : 'b791867d-5948-49e1-88a0-1945ee224587',
        project_id : null,
        order : 1,
        name : 'Sikap',
        is_header : true,
        parent_id : null
    },
    {
        id : 'e5d28a83-f093-411e-bf54-f80ee6a5b22f',
        project_id : null,
        order : 1,
        name : 'Mengutamakan Pelayanan',
        is_header : false,
        parent_id : 'b791867d-5948-49e1-88a0-1945ee224587'
    },
    {
        id : '179a5d4a-c0cf-437b-b623-c1b40d50a280',
        project_id : null,
        order : 2,
        name : 'Kesetiaan terhadap perusahaan',
        is_header : false,
        parent_id : 'b791867d-5948-49e1-88a0-1945ee224587'
    },
    {
        id : '3764e8f3-ad4c-4ecf-8f47-2869e3504ad9',
        project_id : null,
        order : 3,
        name : 'Minat bekerja',
        is_header : false,
        parent_id : 'b791867d-5948-49e1-88a0-1945ee224587'
    },
    {
        id : '4656b780-ee93-446b-a6e6-37ed571b9542',
        project_id : null,
        order : 4,
        name : 'Minat belajar',
        is_header : false,
        parent_id : 'b791867d-5948-49e1-88a0-1945ee224587'
    },
    {
        id : '104db6c5-40e2-4418-92aa-2d8b54c74cd2',
        project_id : null,
        order : 5,
        name : 'Penerimaan terhadap pengawasan',
        is_header : false,
        parent_id : 'b791867d-5948-49e1-88a0-1945ee224587'
    },
    {
        id : '2f69e473-1cbb-4997-a5d0-4b09ca39b66a',
        project_id : null,
        order : 6,
        name : 'Kepemimpinan',
        is_header : false,
        parent_id : 'b791867d-5948-49e1-88a0-1945ee224587'
    },
    {
        id : 'dfe1eefe-15a0-4b4e-8c4b-b28701949e22',
        project_id : null,
        order : 7,
        name : 'Kemampuan bekerja dibawah tekanan',
        is_header : false,
        parent_id : 'b791867d-5948-49e1-88a0-1945ee224587'
    },
    {
        id : '57a3df76-0d72-4b6f-be38-9d91dc9daadc',
        project_id : null,
        order : 2,
        name : 'Tanggung Jawab',
        is_header : true,
        parent_id : null
    },
    {
        id : '696d4e04-b3e6-4765-9d15-baa4520a6b27',
        project_id : null,
        order : 1,
        name : 'Kehadiran tepat waktu',
        is_header : false,
        parent_id : '57a3df76-0d72-4b6f-be38-9d91dc9daadc'
    },{
        id : 'ad5c6a64-1674-4a29-971d-89c584252d37',
        project_id : null,
        order : 2,
        name : 'Kegiatan waktu menyelesaikan pekerjaan',
        is_header : false,
        parent_id : '57a3df76-0d72-4b6f-be38-9d91dc9daadc'
    },
    {
        id : '4257e452-a138-4529-8605-462c602178d2',
        project_id : null,
        order : 3,
        name : 'Inisiatif',
        is_header : false,
        parent_id : '57a3df76-0d72-4b6f-be38-9d91dc9daadc'
    },{
        id : '8b403e1f-dbee-4368-9ac1-08d7efbf1aab',
        project_id : null,
        order : 4,
        name : 'Penerimaan terhadap tugas tambahan',
        is_header : false,
        parent_id : '57a3df76-0d72-4b6f-be38-9d91dc9daadc'
    },{
        id : '01efcab8-dfd9-4a56-b99c-1700069ef051',
        project_id : null,
        order : 3,
        name : 'Kompetensi',
        is_header : true,
        parent_id : null
    },
    {
        id : '05483bce-9c7f-4797-8ea0-eb75345d57b3',
        project_id : null,
        order : 1,
        name : 'Kreativitas',
        is_header : false,
        parent_id : '01efcab8-dfd9-4a56-b99c-1700069ef051'
    },{
        id : 'e2837a86-bf80-4fa5-8c41-eec22d49d22c',
        project_id : null,
        order : 2,
        name : 'Produktivitas',
        is_header : false,
        parent_id : '01efcab8-dfd9-4a56-b99c-1700069ef051'
    },{
        id : 'fd8dc6e4-0f63-4872-a6d5-7767600456fd',
        project_id : null,
        order : 3,
        name : 'Kemampuan dalam bekerja',
        is_header : false,
        parent_id : '01efcab8-dfd9-4a56-b99c-1700069ef051'
    },{
        id : '9a60b224-792b-4dc5-a94a-90dcff7eaa5f',
        project_id : null,
        order : 4,
        name : 'Pengetahuan tentang pekerjaan',
        is_header : false,
        parent_id : '01efcab8-dfd9-4a56-b99c-1700069ef051'
    },{
        id : 'b7b530fe-0332-4539-82d7-4aef5b00e8bf',
        project_id : null,
        order : 5,
        name : 'Ketepatan mengambil keputusan',
        is_header : false,
        parent_id : '01efcab8-dfd9-4a56-b99c-1700069ef051'
    },{
        id : 'a18e32b1-7a46-45ab-a153-fd4897fe55c4',
        project_id : null,
        order : 4,
        name : 'Perencanaan',
        is_header : true,
        parent_id : null
    },{
        id : '8bf05813-2a19-4b5b-a87b-a0886ce9b43d',
        project_id : null,
        order : 1,
        name : 'Kemampuan menetapkan sasaran & tujuan spesifik serta membuat rencana kerja yang efektif untuk mencapainya',
        is_header : false,
        parent_id : 'a18e32b1-7a46-45ab-a153-fd4897fe55c4'
    },{
        id : '6b6ce3a4-6c2a-4043-8f6c-ecf33ab5a770',
        project_id : null,
        order : 5,
        name : 'Pengorganisasian',
        is_header : true,
        parent_id : null
    },{
        id : 'b4ab1fc6-e6a5-4e2e-9377-c536de45d2e3',
        project_id : null,
        order : 1,
        name : 'Kemampuan mengatur pekerjaan dan sistem kerja dengan baik',
        is_header : false,
        parent_id : '6b6ce3a4-6c2a-4043-8f6c-ecf33ab5a770'
    },
    {
        id : 'ecb3e1bd-ee74-4ece-8857-fc20f1bdc9ec',
        project_id : null,
        order : 6,
        name : 'Pengarahan',
        is_header : true,
        parent_id : null
    },{
        id : 'e19460df-a2b0-4e9a-8fe3-d2f85197b464',
        project_id : null,
        order : 1,
        name : 'Kemampuan mengarahkan, memotivasi dan menasehati bawahan',
        is_header : false,
        parent_id : 'ecb3e1bd-ee74-4ece-8857-fc20f1bdc9ec'
    },{
        id : '8c5b055e-1020-4a66-b953-b7d0d93355e4',
        project_id : null,
        order : 7,
        name : 'Pemecahan Masalah',
        is_header : true,
        parent_id : null
    },
    {
        id : '9ee5b542-3062-48a3-9992-17b218d2b133',
        project_id : null,
        order : 1,
        name : 'Kemampuan mengidentifikasi masalah, mengumpulkan dan menganalisa data, mengembangkan & memilih alternatif penyelesaian',
        is_header : false,
        parent_id : '8c5b055e-1020-4a66-b953-b7d0d93355e4'
    },{
        id : 'c0edeaba-634d-4f2e-940e-d338818d0de1',
        project_id : null,
        order : 8,
        name : 'Kemampuan interpersonal',
        is_header : true,
        parent_id : null
    },{
        id : '7ef5069d-67b6-4a38-ae58-f1d0edbb17f0',
        project_id : null,
        order : 1,
        name : 'Kemanapun membina hubungan baik dengan seluiruh pelanggan dan mengerti kebutuhan mereka',
        is_header : false,
        parent_id : 'c0edeaba-634d-4f2e-940e-d338818d0de1'
    },{
        id : '18c2323d-521d-4c45-bd1a-1e9a4810d182',
        project_id : null,
        order : 2,
        name : 'Kemampuan bekerjasama dengan rekan dan memberikan teladan yang baik kepada bawahan',
        is_header : false,
        parent_id : 'c0edeaba-634d-4f2e-940e-d338818d0de1'
    },{
        id : '6c8be1aa-ec4a-4569-aa9d-bc013617fbb5',
        project_id : null,
        order : 9,
        name : 'Kemampuan Berkomunikasi',
        is_header : true,
        parent_id : null
    },{
        id : '25170528-d73a-4cf5-b670-8e78a9d5da6e',
        project_id : null,
        order : 1,
        name : 'Kemampuan menulis memo, laporan berkualitas baik',
        is_header : false,
        parent_id : '6c8be1aa-ec4a-4569-aa9d-bc013617fbb5'
    },{
        id : 'd677f464-5323-4a54-95fd-aa935c834f20',
        project_id : null,
        order : 2,
        name : 'Kemampuan berbicara jelas, singkat dan padat',
        is_header : false,
        parent_id : '6c8be1aa-ec4a-4569-aa9d-bc013617fbb5'
    }
];