export async function insertIfNotExist(tableName, datas, knex, updateExistingData=false) {
    let idLists = datas.map(it => it.id);
    let existingDatas = await knex(tableName).whereIn('id', idLists);

    let newDatas = datas.filter(dataRow => {
        let row = existingDatas.find(it => it.id === dataRow.id);
        if (!row) {
            return true;
        }
        return false;
    });


    if(updateExistingData) {
        let existingDataUpdate = datas.filter(dataRow => {
            let row = existingDatas.find(it => it.id === dataRow.id);
            if (row) {
                return true;
            }
            return false;
        });

        await Promise.all(existingDataUpdate.map(async (existingData) => {
            return await knex(tableName).update(existingData).where({
                id: existingData.id
            });
        }))
    }

    await knex(tableName).insert(newDatas);
}