
exports.up = function(knex, Promise) {
    return knex.schema.table("marketing_plans", table => {
        table.boolean('manager_approval_status');
        table.string('manager_desc');    
        table.string('place');
        table.decimal('cost',14,2);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("marketing_plans", table => {
        table.dropColumn('manager_approval_status');
        table.dropColumn('manager_desc');
        table.dropColumn('place');
        table.dropColumn('cost');
    })
};
