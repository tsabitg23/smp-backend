const insertIfNotExist = require('../src/helpers/seed_insert').insertIfNotExist;
const roles = require('./master_data/roles');
const permissions = require('./master_data/permissions');
const role_permissions = require('./master_data/role_permissions');
const payment_types = require('./master_data/payment_types');
const transaction_types = require('./master_data/transaction_types');
const assessment_aspects = require('./master_data/assessment_aspects');
const verification_questions = require('./master_data/verification_questions');
const accounts = require('./master_data/accounts');

exports.seed = async function(knex, Promise) {
  await insertIfNotExist('roles',roles,knex,true);
  await insertIfNotExist('permissions',permissions,knex,true);
  await insertIfNotExist('role_permissions',role_permissions,knex,true);
  await insertIfNotExist('payment_types',payment_types,knex,true);
  await insertIfNotExist('transaction_types',transaction_types,knex,true);
  await insertIfNotExist('assessment_aspects',assessment_aspects,knex,true);
  await insertIfNotExist('verification_questions',verification_questions,knex,true);
  await insertIfNotExist('accounts',accounts,knex,true);
};
