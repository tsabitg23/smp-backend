import {generateData} from '../helpers/paging';
import {BankAccounts} from "../models/bank_accounts";

export class BankAccountModule{
    static async get(ctx){
        const {body,query={}} = ctx.request;

        ctx.body = await generateData(
            BankAccounts.query().whereNull('deleted_at'),
            ctx,
            {project_id : ctx.state.user.selected_project}
        );
    }

    static async getDetail(ctx){

        ctx.body = await BankAccounts.query().findById(ctx.params.id);
    }

    static async post(ctx){
        ctx.body = await BankAccounts.query()
            .insert(Object.assign({project_id : ctx.state.user.selected_project},ctx.request.body));
    }

    static async put(ctx){
        ctx.body = await BankAccounts.query().where('id',ctx.params.id).update(ctx.request.body)
    }

    static async del(ctx){
        ctx.body = await BankAccounts.query().where('id',ctx.params.id).update({
            deleted_at : new Date().toISOString()
        })
    }


}
