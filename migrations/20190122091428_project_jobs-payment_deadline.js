
exports.up = function(knex, Promise) {
    return knex.schema.table("project_jobs", table => {
        table.renameColumn('payment_deadline','request_date');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("project_jobs", table => {
        table.renameColumn('request_date','payment_deadline');
    })
};
