const constant = require('../../config/const').constant;

module.exports = [
    {
        id : constant.ENTITY.MAIN,
        name : 'Project SMP',
        phone : '0895370886626',
        address : 'Jl. Wijaya 6 Perumnas 1 Bekasi'
    }
];