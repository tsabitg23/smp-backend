import BaseModel from "./base_model";
import {Model} from "objection";
import {Agents} from './agents';
import { Units } from "./units";

export class CommissionRequest extends BaseModel {
    agent_id : string;
    code : string;
	project_id : string;
    unit_id : string;
    unit ?: Units;
	amount : number;
	desc : string;
	marketing_approval_status : boolean;
    marketing_desc : string;
    finance_approval_status : boolean;
    finance_desc : string;
    manager_approval_status : boolean;
    manager_desc : string;
    counted_status : boolean;
    distribution_status : boolean;
    report_status : boolean;
	
    static get tableName() {
        return 'commission_request';
    }

    static get relationMappings() {
        return {
            agent : {
                relation : Model.BelongsToOneRelation,
                modelClass : Agents,
                join : {
                    from : `${this.tableName}.agent_id`,
                    to : `${Agents.tableName}.id`
                }
            },
            unit : {
                relation : Model.BelongsToOneRelation,
                modelClass : Units,
                join : {
                    from : `${this.tableName}.unit_id`,
                    to : `${Units.tableName}.id`
                }
            }
        }
    }

}