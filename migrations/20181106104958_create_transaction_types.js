exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('transaction_types', (table) => {
            table.uuid('id').primary();
            table.string('name');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('transaction_types');
};