import BaseModel from "./base_model";
import {Model} from "objection";
import {Users} from './users';

export class Tasks extends BaseModel {
	creator_user_id : string;
	status : string;
	type : string;

	creator_user ?: Users;
    static get tableName() {
        return 'tasks';
    }

    static get relationMappings() {
        return {
        	creator_user : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.creator_user_id`,
        			to : `${Users.tableName}.id`
        		}
        	}
        }
    }
}