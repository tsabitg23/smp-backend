
exports.up = function(knex, Promise) {
    return knex.schema.table("project_plannings", table => {
        table.uuid('user_id').references('users.id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("project_plannings", table => {
        table.dropColumn('user_id');
    })
};
