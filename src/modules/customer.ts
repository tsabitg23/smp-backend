import {Customers} from '../models/customers';
import {CustomerUnits} from '../models/customer_units';
import {generateData} from '../helpers/paging';
import {transaction} from 'objection';
import {omit} from 'lodash';
import * as uuid from 'uuid';
import {Units} from "../models/units";

export class Customer {
	static async get(ctx){
		const {body,query={}} = ctx.request;

		ctx.body = await generateData(
				Customers.query().whereNull('deleted_at'),
				ctx,
				{project_id : ctx.state.user.selected_project}
			);
	}

	static async getDetail(ctx){

		let data = await Customers.query().eager('[units(notDeleted).[unit,payment_type], akad.[unit, agent]]',{
			notDeleted : (builder)=>{
				builder.whereNull('deleted_at');
			}
		}).findById(ctx.params.id);
		ctx.body = data;

	}

	static async getCustomerUnit(ctx){
		const data = await CustomerUnits.query().eager('[unit]').where({
			customer_id: ctx.params.customer_id,
		});

		ctx.body = data.map(prop=>prop.unit);
	}

	static async post(ctx){
		await transaction(Customers.knex(),async (trx)=>{
			let customer_id = uuid.v4();
			let customer_data = Object.assign({id: customer_id,project_id : ctx.state.user.selected_project},omit(ctx.request.body,['customer_units']));
			let customer_unit_data = Object.assign({customer_id : customer_id},ctx.request.body.customer_units);
			await Customers.query(trx)
				.insert(customer_data);

			await CustomerUnits.query(trx)
				.insert(customer_unit_data);

			ctx.body = {
				message : 'Success',
				id: customer_id
			}
		})
	}

	static async put(ctx){
		ctx.body = await Customers.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async del(ctx){
		await transaction(Customers.knex(),async (trx)=>{
			await Customers.query(trx).where('id',ctx.params.id).update({
				deleted_at : new Date().toISOString()
			})

			await CustomerUnits.query(trx).where('customer_id',ctx.params.id).update({
				deleted_at : new Date().toISOString()
			})

			ctx.body = {
				message : 'success'
			}
		})
	}

	static async postUnit(ctx){
		await transaction(Customers.knex(),async (trx)=>{
			let customer_unit_data = Object.assign({customer_id : ctx.params.customer_id},ctx.request.body);
			await CustomerUnits.query(trx)
				.insert(customer_unit_data);

			ctx.body = {
				message : 'Success'
			}
		})
	}

	static async putUnit(ctx){
		ctx.body = await CustomerUnits.query().where('id',ctx.params.id).update(ctx.request.body)
	}

	static async delUnit(ctx){
		await transaction(Customers.knex(),async (trx)=>{
			await CustomerUnits.query(trx).where('id',ctx.params.id).update({
				deleted_at : new Date().toISOString()
			})

			ctx.body = {
				message : 'success'
			}
		})
	}

	static async getCustomerAkad(ctx){
		const {body,query={}} = ctx.request;
		const q = Units.query().eager('[akad(newest)]',{
			newest : (builder)=>{
				builder.whereNull('deleted_at').orderBy('customer_id','created_at')
			}
		}).whereNull('units.deleted_at').where({project_id : ctx.state.user.selected_project});
		const data:any = await q;
		const formattedData:any = data.filter(prop=>prop.akad.length > 0).map(prop=>{
			const isValid = !!prop.akad[0].customer_id;
			prop.is_data_valid = isValid;
			prop.is_data_not_valid = !isValid;
			return prop
		})
		ctx.body = {
			data : formattedData,
			max : formattedData.length,
			size : formattedData.length
		};
	}
}
