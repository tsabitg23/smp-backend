exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('tasks', (table) => {
            table.uuid('id').primary();
            table.uuid('creator_user_id').references('users.id').onDelete('restrict');
            table.string('status');
            table.string('type');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('tasks');
};