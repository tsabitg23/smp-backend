exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('customer_analyst', (table) => {
            table.uuid('id').primary();
            table.uuid('project_id').references('projects.id').onDelete('restrict');
            table.uuid('customer_id').references('customers.id').onDelete('restrict');
            table.uuid('unit_id').references('units.id').onDelete('restrict');
            table.date('akad_date');
            table.boolean('recommendation');
            table.boolean('manager_approve_status');
            table.string('manager_desc');
            table.decimal('unit_price',14,2);
            table.string('payment_scheme');
            table.string('tenor');
            table.string('downpayment');
            table.string('first_downpayment');
            table.string('next_downpayment');
            table.string('next_downpayment_amount');
            table.decimal('total_price',14,2);
            table.integer('total_installment');
            table.string('mortgage_history');
            table.text('desc');
            table.date('debtor_dob');
            table.string('spouse_name');
            table.string('occupation');
            table.string('spouse_occupation');
            table.string('work_duration');
            table.string('spouse_work_duration');
            table.string('children');
            table.string('payroll_desc');
            table.string('debtor_asset');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.datetime('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('customer_analyst');
};
