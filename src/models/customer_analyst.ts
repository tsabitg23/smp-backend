import BaseModel from "./base_model";
import {Model} from "objection";
import {Agencies} from "./agencies";
import {Customers} from "./customers";
import {Units} from "./units";
import {CustomerAnalystAccount} from "./customer_analyst_account";

export class CustomerAnalyst extends BaseModel {
    project_id : string;
    customer_id : string;
    unit_id : string;
    akad_date: string;
    recommendation : boolean;
    manager_approve_status : boolean;
    manager_desc: string;
    unit_price: number;
    payment_scheme: string;
    tenor: string;
    downpayment: string;
    first_downpayment: string;
    next_downpayment: string;
    next_downpayment_amount: string;
    total_price: number;
    total_installment : number;
    mortgage_history: string;
    desc: string;
    debtor_dob: string;
    spouse_name: string;
    occupation: string;
    spouse_occupation: string;
    work_duration: string;
    spouse_work_duration: string;
    children: string;
    payroll_desc: string;
    debtor_asset: string;

    customer ?: Customers;
    unit ?: Units;
    accounts ?: CustomerAnalystAccount;


    static get tableName() {
        return 'customer_analyst';
    }

    static get relationMappings() {
        return {
            customer : {
                relation : Model.BelongsToOneRelation,
                modelClass : Customers,
                join : {
                    from : `${this.tableName}.customer_id`,
                    to : `${Customers.tableName}.id`
                }
            },
            unit : {
                relation : Model.BelongsToOneRelation,
                modelClass : Units,
                join : {
                    from : `${this.tableName}.unit_id`,
                    to : `${Units.tableName}.id`
                }
            },
            accounts : {
                relation : Model.HasManyRelation,
                modelClass : CustomerAnalystAccount,
                join : {
                    from : `${this.tableName}.id`,
                    to : `${CustomerAnalystAccount.tableName}.customer_analyst_id`
                }
            }
        }
    }

}
