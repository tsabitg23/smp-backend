import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';
import {Users} from './users';

export class AccountingPeriod extends BaseModel {
	project_id : string;
	user_id : string;
	start_balance : number;
	account_number : string;
	month : string;

	project ?: Projects;
	user ?: Users;

    static get tableName() {
        return 'accounting_period';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	},
        	user : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Users,
        		join : {
        			from : `${this.tableName}.user_id`,
        			to : `${Users.tableName}.id`
        		}
        	}
        }
    }
}