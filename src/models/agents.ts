import BaseModel from "./base_model";
import {Model} from "objection";
import {Agencies} from './agencies';

export class Agents extends BaseModel {
	agency_id : string;
	name : string;
	phone : string;
	email : string;
	address : string;
	unit_sold: number;
    join_date : string;
    project_id : string;

	agency ?: Agencies;
    static get tableName() {
        return 'agents';
    }

    static get relationMappings() {
        return {
        	agency : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Agencies,
        		join : {
        			from : `${this.tableName}.agency_id`,
        			to : `${Agencies.tableName}.id`
        		}
        	}
        }
    }

}