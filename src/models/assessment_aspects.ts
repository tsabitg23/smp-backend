import BaseModel from "./base_model";
import {Model} from "objection";
import {Projects} from './projects';

export class AssessmentAspects extends BaseModel {
	project_id : string;
	name : string;
	is_header : boolean;
	parent_id : string;
	order : number;
    children: any;

    project ?: Projects;
    static get tableName() {
        return 'assessment_aspects';
    }

    static get relationMappings() {
        return {
        	project : {
        		relation : Model.BelongsToOneRelation,
        		modelClass : Projects,
        		join : {
        			from : `${this.tableName}.project_id`,
        			to : `${Projects.tableName}.id`
        		}
        	}
        }
    }
}